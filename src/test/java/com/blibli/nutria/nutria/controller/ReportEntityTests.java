package com.blibli.nutria.nutria.controller;


import com.blibli.nutria.nutria.helper.DateHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.user.LoginUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.report.ReportResponse;
import com.blibli.nutria.nutria.model.command.response.user.LoginUserResponse;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.model.entity.Menu;
import com.blibli.nutria.nutria.model.entity.Report;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.ReportRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ReportEntityTests {

  public static final String EMAIL = "bagasimmanuel00@gmail.com";
  public static final String ADMIN_EMAIL = "admin@gmail.com";
  public static final String PASSWORD = "123456";
  public static final String FULL_NAME = "Bagas Immanuel";
  private static final String BEARER_PREFIX = "Bearer ";

  public static final String MEALPLAN_NAME_ONE = "MEALPLAN_ONE";
  public static final String MEALPLAN_DESCRIPTION_ONE = "MEALPLAN_ONE_DESCRIPTION";

  public static final String STOCK_NAME_ONE = "STOCK_ONE";
  public static final String STOCK_DESC_ONE = "STOCK_ONE_DESCRIPTION";
  public static final int STOCK_QUANTITY_ONE = 1;
  public static final String STOCK_NAME_TWO = "STOCK_ONE_UPDATED";
  public static final String STOCK_DESC_TWO = "STOCK_ONE_DESCRIPTION_UPDATED";
  public static final int STOCK_QUANTITY_TWO = 2;

  public static final String MENU_NAME_ONE = "MENU_NAME_ONE";
  public static final String MENU_DESCRIPTION_ONE = "MENU_DESCRIPTION_ONE";
  public static final int MENU_CALORIE_ONE = 450;

  public static final int MENU_INGREDIENT_QUANTITY_ONE = 5;
  public static final int MENU_INGREDIENT_QUANTITY_TWO = 4;

  public static final String MENU_NAME_TWO = "MENU_NAME_TWO";
  public static final String MENU_DESCRIPTION_TWO = "MENU_DESCRIPTION_TWO";
  public static final int MENU_CALORIE_TWO = 300;

  public static final UUID ADMIN_UUID = UUID.randomUUID();
  public static final UUID USER_UUID = UUID.randomUUID();
  public static final UUID MEALPLAN_UUID = UUID.randomUUID();
  public static final UUID SUBSCRIPTION_UUID = UUID.randomUUID();
  public static final UUID SUBSCRIPTION_UUID_TWO = UUID.randomUUID();
  public static final UUID MENU_UUID = UUID.randomUUID();
  public static final UUID MENU_UUID_TWO = UUID.randomUUID();
  public static final UUID STOCK_UUID_ONE = UUID.randomUUID();
  public static final UUID STOCK_UUID_TWO = UUID.randomUUID();
  public static final UUID REPORT_UUID = UUID.randomUUID();

  public static final SubscriptionItem.DeliveryTime LUNCH = SubscriptionItem.DeliveryTime.LUNCH;
  public static final SubscriptionItem.DeliveryTime DINNER = SubscriptionItem.DeliveryTime.DINNER;
  public static final SubscriptionItem.DeliveryStatus NOT_PROCESSED = SubscriptionItem.DeliveryStatus.NOT_PROCESSED;
  public static final String DELIVERY_LOC = "Lokasi 1";
  public static final String DELIVERY_LOC_TWO = "Lokasi 2";

  public static final int SUBSCRIPTION_COUNT = 20;
  public static final int REVENUE = 8000000;
  public static final int PURCHASE_COUNT_MENU = 5;
  public static final int PURCHASE_COUNT_MENU_TWO = 5;

  @Autowired
  private MealplanRepository mealplanRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private StockRepository stockRepository;
  @Autowired
  private MenuRepository menuRepository;
  @Autowired
  private ReportRepository reportRepository;
  @Autowired
  private SubscriptionRepository subscriptionRepository;
  @Autowired
  private WebTestClient webTestClient;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  private UUIDHelper uuidHelper;
  @Autowired
  private DateHelper dateHelper;
  @Autowired
  @Qualifier("springRedisTest")
  private ReactiveRedisTemplate<String, String> reactiveRedisTemplate;

  private UUID accessToken;
  private UUID adminAccessToken;
  private long MONDAY;
  private long TUESDAY;
  private long WEDNESDAY;
  private long THURSDAY;
  private long FRIDAY;
  private long NEXT_WEEK_MONDAY;

  @BeforeAll
  public void setup() {
    User user = User
        .builder()
        .id(USER_UUID)
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .roles(Arrays.asList("USER"))
        .fullName(FULL_NAME)
        .verified(true)
        .build();

    User admin = User
        .builder()
        .id(ADMIN_UUID)
        .email(ADMIN_EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .roles(Arrays.asList("USER", "ADMIN"))
        .fullName(FULL_NAME)
        .verified(true)
        .build();

    userRepository.save(user).subscribe();
    userRepository.save(admin).subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    accessToken = loginResponse.getData().getAccessToken();


    LoginUserRequest adminLoginRequest = LoginUserRequest.builder()
        .email(ADMIN_EMAIL)
        .password(PASSWORD)
        .build();


    BaseCommandResponse<LoginUserResponse> adminLoginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(adminLoginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    adminAccessToken = adminLoginResponse.getData().getAccessToken();

    // Mock Data

    Mealplan mealplan = Mealplan.builder()
        .id(MEALPLAN_UUID)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .imageUrl(null)
        .build();

    Stock stock_ONE = Stock.builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    Stock stock_TWO = Stock.builder()
        .id(STOCK_UUID_TWO)
        .name(STOCK_NAME_TWO)
        .description(STOCK_DESC_TWO)
        .quantity(STOCK_QUANTITY_TWO)
        .build();

    mealplanRepository.save(mealplan).subscribe();
    stockRepository.save(stock_ONE).subscribe();
    stockRepository.save(stock_TWO).subscribe();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID.toString())
        .recipes(Arrays.asList(
            Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build(),
            Ingredient.builder().stockId(STOCK_UUID_TWO.toString()).quantity(MENU_INGREDIENT_QUANTITY_TWO).build()))
        .build();

    Menu menu_two = Menu.builder()
        .id(MENU_UUID_TWO)
        .name(MENU_NAME_TWO)
        .description(MENU_DESCRIPTION_TWO)
        .calorie(MENU_CALORIE_TWO)
        .mealplanId(MEALPLAN_UUID.toString())
        .recipes(Arrays.asList(
            Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build(),
            Ingredient.builder().stockId(STOCK_UUID_TWO.toString()).quantity(MENU_INGREDIENT_QUANTITY_TWO).build()))
        .build();

    menuRepository.save(menu_one).subscribe();
    menuRepository.save(menu_two).subscribe();

    MONDAY = dateHelper.getPreviousOrSameDayOfTheWeek(DayOfWeek.MONDAY);
    TUESDAY = MONDAY + 86400;
    WEDNESDAY = MONDAY + 2 * 86400;
    THURSDAY = MONDAY + 3 * 86400;
    FRIDAY = MONDAY + 4 * 86400;
    NEXT_WEEK_MONDAY = dateHelper.getNextDayOfTheWeek(DayOfWeek.MONDAY);

    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID_TWO)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.PAID)
        .timestamp(MONDAY)
        .renewal(false)
        .build()).subscribe();

  }

  @AfterEach
  void teardown() {
    this.reportRepository.deleteAll().subscribe();
  }

  @AfterAll
  void cleanUp() {
    this.mealplanRepository.deleteAll().subscribe();
    this.userRepository.deleteAll().subscribe();
    this.stockRepository.deleteAll().subscribe();
    this.menuRepository.deleteAll().subscribe();
    this.subscriptionRepository.deleteAll().subscribe();
    reactiveRedisTemplate.delete(reactiveRedisTemplate.keys("*")).subscribe();
  }

  // Report
  // Report
  // Report
  @Test
  void getReport_success_reportExists() {

    reportRepository.save(Report.builder()
        .id(REPORT_UUID)
        .timestamp(MONDAY)
        .subscriptionCount(SUBSCRIPTION_COUNT)
        .revenue(REVENUE)
        .items(Arrays.asList(
            Report.ReportItem.builder()
                .purchaseCount(5)
                .menuId(MENU_UUID.toString()).build(),
            Report.ReportItem.builder()
                .purchaseCount(5)
                .menuId(MENU_UUID_TWO.toString()).build()
        )).build()).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<ReportResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<ReportResponse>>() {
        };

    BaseCommandResponse<ReportResponse> response = this.webTestClient.get()
        .uri("/api/admin/reports/"+ MONDAY)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(REPORT_UUID,response.getData().getId());
    assertEquals(REVENUE,response.getData().getRevenue());
    assertEquals(SUBSCRIPTION_COUNT,response.getData().getSubscriptionCount());
    assertEquals(MONDAY,response.getData().getTimestamp());
    assertEquals(MENU_UUID,response.getData().getItems().get(0).getMenu().getId());
    assertEquals(PURCHASE_COUNT_MENU,response.getData().getItems().get(0).getPurchaseCount());
    assertEquals(MENU_UUID_TWO,response.getData().getItems().get(1).getMenu().getId());
    assertEquals(PURCHASE_COUNT_MENU_TWO,response.getData().getItems().get(1).getPurchaseCount());

  }

  @Test
  void getReport_success_newReport() {

    ParameterizedTypeReference<BaseCommandResponse<ReportResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<ReportResponse>>() {
        };

    BaseCommandResponse<ReportResponse> response = this.webTestClient.get()
        .uri("/api/admin/reports/"+ MONDAY)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(200000,response.getData().getRevenue());
    assertEquals(1,response.getData().getSubscriptionCount());
    assertEquals(MONDAY,response.getData().getTimestamp());
    assertEquals(MENU_UUID,response.getData().getItems().get(0).getMenu().getId());
    assertEquals(5,response.getData().getItems().get(0).getPurchaseCount());

  }

  @Test
  void getReport_failed_invalidDateNotMonday() {

    reportRepository.save(Report.builder()
        .id(REPORT_UUID)
        .timestamp(MONDAY)
        .subscriptionCount(SUBSCRIPTION_COUNT)
        .revenue(REVENUE)
        .items(Arrays.asList(
            Report.ReportItem.builder()
                .purchaseCount(5)
                .menuId(MENU_UUID.toString()).build(),
            Report.ReportItem.builder()
                .purchaseCount(5)
                .menuId(MENU_UUID_TWO.toString()).build()
        )).build()).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<ReportResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<ReportResponse>>() {
        };

    BaseCommandResponse<ReportResponse> response = this.webTestClient.get()
        .uri("/api/admin/reports/"+ TUESDAY)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    System.out.println(response);
    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getCode());
    assertEquals(HttpStatus.BAD_REQUEST.getReasonPhrase(), response.getStatus());

  }

  @Test
  void getReport_failed_invalidDateNoSubscriptions() {

    ParameterizedTypeReference<BaseCommandResponse<ReportResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<ReportResponse>>() {
        };

    BaseCommandResponse<ReportResponse> response = this.webTestClient.get()
        .uri("/api/admin/reports/"+ NEXT_WEEK_MONDAY)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    System.out.println(response);
    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getCode());
    assertEquals(HttpStatus.BAD_REQUEST.getReasonPhrase(), response.getStatus());


  }

  @Test
  void getReport_failed_unauthorized() {

    reportRepository.save(Report.builder()
        .id(REPORT_UUID)
        .timestamp(MONDAY)
        .subscriptionCount(SUBSCRIPTION_COUNT)
        .revenue(REVENUE)
        .items(Arrays.asList(
            Report.ReportItem.builder()
                .purchaseCount(5)
                .menuId(MENU_UUID.toString()).build(),
            Report.ReportItem.builder()
                .purchaseCount(5)
                .menuId(MENU_UUID_TWO.toString()).build()
        )).build()).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<ReportResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<ReportResponse>>() {
        };

    BaseCommandResponse<ReportResponse> response = this.webTestClient.get()
        .uri("/api/admin/reports/"+ MONDAY)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

}
