package com.blibli.nutria.nutria.controller;

import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.user.LoginUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuResponse;
import com.blibli.nutria.nutria.model.command.response.user.LoginUserResponse;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.model.entity.Menu;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MenuControllerTests {

  public static final String EMAIL = "bagasimmanuel00@gmail.com";
  public static final String PASSWORD = "123456";
  public static final String FULL_NAME = "Bagas Immanuel";
  private static final String BEARER_PREFIX = "Bearer ";

  public static final String MEALPLAN_NAME_ONE = "MEALPLAN_ONE";
  public static final String MEALPLAN_DESCRIPTION_ONE = "MEALPLAN_ONE_DESCRIPTION";

  public static final String STOCK_NAME_ONE = "STOCK_ONE";
  public static final String STOCK_DESC_ONE = "STOCK_ONE_DESCRIPTION";
  public static final int STOCK_QUANTITY_ONE = 1;
  public static final String STOCK_NAME_TWO = "STOCK_ONE_UPDATED";
  public static final String STOCK_DESC_TWO = "STOCK_ONE_DESCRIPTION_UPDATED";
  public static final int STOCK_QUANTITY_TWO = 2;

  public static final String MENU_NAME_ONE = "MENU_NAME_ONE";
  public static final String MENU_DESCRIPTION_ONE = "MENU_DESCRIPTION_ONE";
  public static final int MENU_CALORIE_ONE = 450;
  public static final int MENU_INGREDIENT_QUANTITY_ONE = 5;
  public static final int MENU_INGREDIENT_QUANTITY_TWO = 4;

  public static final UUID MEALPLAN_UUID = UUID.randomUUID();
  public static final UUID MENU_UUID = UUID.randomUUID();
  public static final UUID MENU_UUID_TWO = UUID.randomUUID();
  public static final UUID STOCK_UUID_ONE = UUID.randomUUID();
  public static final  UUID STOCK_UUID_TWO = UUID.randomUUID();

  @Autowired
  private MealplanRepository mealplanRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private StockRepository stockRepository;
  @Autowired
  private MenuRepository menuRepository;
  @Autowired
  private WebTestClient webTestClient;
  @Autowired
  private UUIDHelper uuidHelper;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  @Qualifier("springRedisTest")
  private ReactiveRedisTemplate<String, String> reactiveRedisTemplate;

  private UUID accessToken;

  @BeforeAll
  public void setup() {
    User user = User
        .builder()
        .id(UUID.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .roles(Arrays.asList("USER"))
        .fullName(FULL_NAME)
        .verified(true)
        .build();

    userRepository.save(user).subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    accessToken = loginResponse.getData().getAccessToken();

    // Mock Data

    Mealplan mealplan = Mealplan.builder()
        .id(MEALPLAN_UUID)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .imageUrl(null)
        .build();

    Stock stock_ONE = Stock.builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    Stock stock_TWO = Stock.builder()
        .id(STOCK_UUID_TWO)
        .name(STOCK_NAME_TWO)
        .description(STOCK_DESC_TWO)
        .quantity(STOCK_QUANTITY_TWO)
        .build();

    mealplanRepository.save(mealplan).subscribe();
    stockRepository.save(stock_ONE).subscribe();
    stockRepository.save(stock_TWO).subscribe();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID.toString())
        .recipes(Arrays.asList(
            Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build(),
            Ingredient.builder().stockId(STOCK_UUID_TWO.toString()).quantity(MENU_INGREDIENT_QUANTITY_TWO).build()))
        .softDelete(false)
        .build();

    Menu menu_two= Menu.builder()
        .id(MENU_UUID_TWO)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID.toString())
        .recipes(Arrays.asList(
            Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build(),
            Ingredient.builder().stockId(STOCK_UUID_TWO.toString()).quantity(MENU_INGREDIENT_QUANTITY_TWO).build()))
        .softDelete(true)
        .build();
    menuRepository.save(menu_one).subscribe();
    menuRepository.save(menu_two).subscribe();
  }

  @AfterAll
  void cleanUp() {
    this.mealplanRepository.deleteAll().subscribe();
    this.userRepository.deleteAll().subscribe();
    this.stockRepository.deleteAll().subscribe();
    this.menuRepository.deleteAll().subscribe();
    reactiveRedisTemplate.delete(reactiveRedisTemplate.keys("*")).subscribe();
  }

  @Test
  void findAllWithParam_success_NameMealplanCalorie(){

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>>() {
        };

    BasePaginatedCommandResponse<MenuResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/menus")
                .queryParam("name",MENU_NAME_ONE)
                .queryParam("mealplanId",MEALPLAN_UUID.toString())
                .queryParam("calorie",MENU_CALORIE_ONE)
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().get(0).getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().get(0).getCalorie());
    assertEquals(MEALPLAN_UUID, response.getData().get(0).getMealplan().getId());
    assertEquals(MEALPLAN_NAME_ONE, response.getData().get(0).getMealplan().getName());
    assertEquals(MEALPLAN_DESCRIPTION_ONE, response.getData().get(0).getMealplan().getDescription());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(1, response.getPagination().getTotalItems());
  }

  @Test
  void findAllWithParam_success_NameMealplan(){

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>>() {
        };

    BasePaginatedCommandResponse<MenuResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/menus")
                .queryParam("name",MENU_NAME_ONE)
                .queryParam("mealplanId",MEALPLAN_UUID.toString())
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().get(0).getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().get(0).getCalorie());
    assertEquals(MEALPLAN_UUID, response.getData().get(0).getMealplan().getId());
    assertEquals(MEALPLAN_NAME_ONE, response.getData().get(0).getMealplan().getName());
    assertEquals(MEALPLAN_DESCRIPTION_ONE, response.getData().get(0).getMealplan().getDescription());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(1, response.getPagination().getTotalItems());
  }

  @Test
  void findAllWithParam_success_NameCalorie() {

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>>() {
        };

    BasePaginatedCommandResponse<MenuResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/menus")
                .queryParam("name", MENU_NAME_ONE)
                .queryParam("calorie", MENU_CALORIE_ONE)
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().get(0).getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().get(0).getCalorie());
    assertEquals(MEALPLAN_UUID, response.getData().get(0).getMealplan().getId());
    assertEquals(MEALPLAN_NAME_ONE, response.getData().get(0).getMealplan().getName());
    assertEquals(MEALPLAN_DESCRIPTION_ONE, response.getData().get(0).getMealplan().getDescription());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(1, response.getPagination().getTotalItems());
  }

  @Test
  void findAllWithParam_success_NameOnly(){

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>>() {
        };

    BasePaginatedCommandResponse<MenuResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/menus")
                .queryParam("name",MENU_NAME_ONE)
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().get(0).getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().get(0).getCalorie());
    assertEquals(MEALPLAN_UUID, response.getData().get(0).getMealplan().getId());
    assertEquals(MEALPLAN_NAME_ONE, response.getData().get(0).getMealplan().getName());
    assertEquals(MEALPLAN_DESCRIPTION_ONE, response.getData().get(0).getMealplan().getDescription());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(1, response.getPagination().getTotalItems());
  }

  @Test
  void findAllWithParam_success_MealplanCalorie(){

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>>() {
        };

    BasePaginatedCommandResponse<MenuResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/menus")
                .queryParam("mealplanId",MEALPLAN_UUID.toString())
                .queryParam("calorie",MENU_CALORIE_ONE)
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().get(0).getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().get(0).getCalorie());
    assertEquals(MEALPLAN_UUID, response.getData().get(0).getMealplan().getId());
    assertEquals(MEALPLAN_NAME_ONE, response.getData().get(0).getMealplan().getName());
    assertEquals(MEALPLAN_DESCRIPTION_ONE, response.getData().get(0).getMealplan().getDescription());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(1, response.getPagination().getTotalItems());
  }

  @Test
  void findAllWithParam_success_MealplanOnly(){

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>>() {
        };

    BasePaginatedCommandResponse<MenuResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/menus")
                .queryParam("mealplanId",MEALPLAN_UUID.toString())
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().get(0).getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().get(0).getCalorie());
    assertEquals(MEALPLAN_UUID, response.getData().get(0).getMealplan().getId());
    assertEquals(MEALPLAN_NAME_ONE, response.getData().get(0).getMealplan().getName());
    assertEquals(MEALPLAN_DESCRIPTION_ONE, response.getData().get(0).getMealplan().getDescription());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(1, response.getPagination().getTotalItems());
  }

  @Test
  void findAllWithParam_success_CalorieOnly(){

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>>() {
        };

    BasePaginatedCommandResponse<MenuResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/menus")
                .queryParam("calorie",MENU_CALORIE_ONE)
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().get(0).getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().get(0).getCalorie());
    assertEquals(MEALPLAN_UUID, response.getData().get(0).getMealplan().getId());
    assertEquals(MEALPLAN_NAME_ONE, response.getData().get(0).getMealplan().getName());
    assertEquals(MEALPLAN_DESCRIPTION_ONE, response.getData().get(0).getMealplan().getDescription());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(1, response.getPagination().getTotalItems());
  }

  @Test
  void findAllWithParam_success_noParam(){

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuResponse>>() {
        };

    BasePaginatedCommandResponse<MenuResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/menus")
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().get(0).getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().get(0).getCalorie());
    assertEquals(MEALPLAN_UUID, response.getData().get(0).getMealplan().getId());
    assertEquals(MEALPLAN_NAME_ONE, response.getData().get(0).getMealplan().getName());
    assertEquals(MEALPLAN_DESCRIPTION_ONE, response.getData().get(0).getMealplan().getDescription());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(1, response.getPagination().getTotalItems());
  }
}
