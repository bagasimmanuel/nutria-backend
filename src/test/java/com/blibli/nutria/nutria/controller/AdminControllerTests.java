package com.blibli.nutria.nutria.controller;


import com.blibli.nutria.nutria.helper.DateHelper;
import com.blibli.nutria.nutria.helper.StorageHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.mealplan.CreateMealplanRequest;
import com.blibli.nutria.nutria.model.command.request.mealplan.EditMealplanRequest;
import com.blibli.nutria.nutria.model.command.request.menu.CreateMenuRequest;
import com.blibli.nutria.nutria.model.command.request.menu.EditMenuRequest;
import com.blibli.nutria.nutria.model.command.request.stock.CreateStockRequest;
import com.blibli.nutria.nutria.model.command.request.stock.EditStockRequest;
import com.blibli.nutria.nutria.model.command.request.user.LoginUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.banner.BannerResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.CreateMealplanResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.EditMealplanResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.GetMealplanResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import com.blibli.nutria.nutria.model.command.response.stock.CheckStockResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.command.response.user.LoginUserResponse;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.model.entity.Menu;
import com.blibli.nutria.nutria.model.entity.Necessity;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.BannerRepository;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.NecessityRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AdminControllerTests {

  public static final String EMAIL = "bagasimmanuel00@gmail.com";
  public static final String PASSWORD = "123456";
  public static final String FULL_NAME = "Bagas Immanuel";
  private static final String BEARER_PREFIX = "Bearer ";

  public static final String MEALPLAN_NAME_ONE = "MEALPLAN_ONE";
  public static final String MEALPLAN_DESCRIPTION_ONE = "MEALPLAN_ONE_DESCRIPTION";
  public static final String MEALPLAN_NAME_TWO = "MEALPLAN_ONE_TWO";
  public static final String MEALPLAN_DESCRIPTION_TWO = "MEALPLAN_ONE_DESCRIPTION_TWO";

  public static final String STOCK_NAME_ONE = "STOCK_ONE";
  public static final String STOCK_DESC_ONE = "STOCK_ONE_DESCRIPTION";
  public static final int STOCK_QUANTITY_ONE = 2;
  public static final String STOCK_NAME_TWO = "STOCK_ONE_UPDATED";
  public static final String STOCK_DESC_TWO = "STOCK_ONE_DESCRIPTION_UPDATED";
  public static final int STOCK_QUANTITY_TWO = 2;

  public static final String MENU_NAME_ONE = "MENU_NAME_ONE";
  public static final String MENU_DESCRIPTION_ONE = "MENU_DESCRIPTION_ONE";
  public static final int MENU_CALORIE_ONE = 450;
  public static final int MENU_INGREDIENT_QUANTITY_ONE = 5;

  public static final String MENU_NAME_TWO = "MENU_NAME_TWO";
  public static final String MENU_DESCRIPTION_TWO = "MENU_DESCRIPTION_TWO";
  public static final int MENU_CALORIE_TWO = 600;
  public static final int MENU_INGREDIENT_QUANTITY_TWO = 3;

  public static final SubscriptionItem.DeliveryTime LUNCH = SubscriptionItem.DeliveryTime.LUNCH;
  public static final SubscriptionItem.DeliveryTime DINNER = SubscriptionItem.DeliveryTime.DINNER;
  public static final SubscriptionItem.DeliveryStatus NOT_PROCESSED = SubscriptionItem.DeliveryStatus.NOT_PROCESSED;
  public static final String DELIVERY_LOC = "Lokasi 1";

  public static final UUID USER_UUID = UUID.randomUUID();
  public static final UUID MEALPLAN_UUID_ONE = UUID.randomUUID();
  public static final UUID MEALPLAN_UUID_TWO = UUID.randomUUID();
  public static final UUID SUBSCRIPTION_UUID = UUID.randomUUID();
  public static final UUID MENU_UUID = UUID.randomUUID();
  public static final UUID STOCK_UUID_ONE = UUID.randomUUID();
  public static final UUID STOCK_UUID_TWO = UUID.randomUUID();

  @Autowired
  private MealplanRepository mealplanRepository;
  @Autowired
  private BannerRepository bannerRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private StockRepository stockRepository;
  @Autowired
  private MenuRepository menuRepository;
  @Autowired
  private SubscriptionRepository subscriptionRepository;
  @Autowired
  private NecessityRepository necessityRepository;
  @Autowired
  private WebTestClient webTestClient;
  @Autowired
  private UUIDHelper uuidHelper;
  @Autowired
  private DateHelper dateHelper;
  @Autowired
  private StorageHelper storageHelper;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  @Qualifier("springRedisTest")
  private ReactiveRedisTemplate<String, String> reactiveRedisTemplate;

  private UUID accessToken;
  private long MONDAY;
  private long TUESDAY;
  private long WEDNESDAY;
  private long THURSDAY;
  private long FRIDAY;
  private long NEXT_WEEK_MONDAY;
  private long NEXT_WEEK_TUESDAY;
  private long NEXT_WEEK_WEDNESDAY;
  private long NEXT_WEEK_THURSDAY;
  private long NEXT_WEEK_FRIDAY;

  @BeforeAll
  public void beforeAll() {
    User adminUser = User
        .builder()
        .id(USER_UUID)
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .roles(Arrays.asList("USER", "ADMIN"))
        .fullName(FULL_NAME)
        .verified(true)
        .build();

    userRepository.save(adminUser).subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    accessToken = loginResponse.getData().getAccessToken();
    MONDAY = dateHelper.getPreviousOrSameDayOfTheWeek(DayOfWeek.MONDAY);
    TUESDAY = dateHelper.getPreviousOrSameDayOfTheWeek(DayOfWeek.TUESDAY);
    WEDNESDAY = dateHelper.getPreviousOrSameDayOfTheWeek(DayOfWeek.WEDNESDAY);
    THURSDAY = dateHelper.getPreviousOrSameDayOfTheWeek(DayOfWeek.THURSDAY);
    FRIDAY = dateHelper.getPreviousOrSameDayOfTheWeek(DayOfWeek.FRIDAY);
    NEXT_WEEK_MONDAY = dateHelper.getNextDayOfTheWeek(DayOfWeek.MONDAY);
    NEXT_WEEK_TUESDAY = dateHelper.getNextDayOfTheWeek(DayOfWeek.TUESDAY);
    NEXT_WEEK_WEDNESDAY = dateHelper.getNextDayOfTheWeek(DayOfWeek.WEDNESDAY);
    NEXT_WEEK_THURSDAY = dateHelper.getNextDayOfTheWeek(DayOfWeek.THURSDAY);
    NEXT_WEEK_FRIDAY = dateHelper.getNextDayOfTheWeek(DayOfWeek.FRIDAY);

  }

  @AfterEach
  void teardown() {
    this.mealplanRepository.deleteAll().subscribe();
    this.stockRepository.deleteAll().subscribe();
    this.menuRepository.deleteAll().subscribe();
    this.subscriptionRepository.deleteAll().subscribe();
    this.bannerRepository.deleteAll().subscribe();
    this.necessityRepository.deleteAll().subscribe();
  }

  @AfterAll
  void cleanUp() {
    this.userRepository.deleteAll().subscribe();
    reactiveRedisTemplate.delete(reactiveRedisTemplate.keys("*")).subscribe();
  }

  @Test
  void createMealplan_success() {

    CreateMealplanRequest request = CreateMealplanRequest
        .builder()
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<CreateMealplanResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<CreateMealplanResponse>>() {
        };

    BaseCommandResponse<CreateMealplanResponse> response = this.webTestClient.post()
        .uri("/api/admin/mealplans")
        .body(Mono.just(request), CreateMealplanRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MEALPLAN_NAME_ONE, response.getData().getName());
    assertEquals(MEALPLAN_DESCRIPTION_ONE, response.getData().getDescription());
    assertNull(response.getData().getImageUrl());
  }

  @Test
  void createMealplan_failed_unauthorized() {
    CreateMealplanRequest request = CreateMealplanRequest
        .builder()
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<CreateMealplanResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<CreateMealplanResponse>>() {
        };

    BaseCommandResponse<CreateMealplanResponse> response = this.webTestClient.post()
        .uri("/api/admin/mealplans")
        .body(Mono.just(request), CreateMealplanRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void editMealplan_success() {
    Mealplan mealplan = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .softDelete(false)
        .build();
    mealplanRepository.save(mealplan).subscribe();

    EditMealplanRequest request = EditMealplanRequest
        .builder()
        .name(MEALPLAN_NAME_TWO)
        .description(MEALPLAN_DESCRIPTION_TWO)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<EditMealplanResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<EditMealplanResponse>>() {
        };

    BaseCommandResponse<EditMealplanResponse> response = this.webTestClient.put()
        .uri(uriBuilder ->
            uriBuilder.path("/api/admin/mealplans/" + MEALPLAN_UUID_ONE.toString())
                .build()
        )
        .body(Mono.just(request), EditMealplanRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MEALPLAN_NAME_TWO, response.getData().getName());
    assertEquals(MEALPLAN_DESCRIPTION_TWO, response.getData().getDescription());
  }

  @Test
  void editMealplan_failed_unauthorized() {
    Mealplan mealplan = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();
    mealplanRepository.save(mealplan).subscribe();

    EditMealplanRequest request = EditMealplanRequest
        .builder()
        .name(MEALPLAN_NAME_TWO)
        .description(MEALPLAN_DESCRIPTION_TWO)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<EditMealplanResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<EditMealplanResponse>>() {
        };

    BaseCommandResponse<EditMealplanResponse> response = this.webTestClient.put()
        .uri(uriBuilder ->
            uriBuilder.path("/api/admin/mealplans/" + MEALPLAN_UUID_ONE.toString())
                .build()
        )
        .body(Mono.just(request), EditMealplanRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void deleteMealplan_success() {
    Mealplan mealplan = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();
    mealplanRepository.save(mealplan).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<Void>> type =
        new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
        };

    BaseCommandResponse<Void> response = this.webTestClient.delete()
        .uri(uriBuilder ->
            uriBuilder.path("/api/admin/mealplans/" + MEALPLAN_UUID_ONE.toString())
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertNull(response.getData());

    storageHelper.deleteFile(StorageHelper.STORAGE_TYPE.MEALPLAN_IMAGE,MEALPLAN_UUID_ONE).subscribe();

  }

  @Test
  void deleteMealplan_failed_unauthorized() {
    Mealplan mealplan = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();
    mealplanRepository.save(mealplan).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<Void>> type =
        new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
        };

    BaseCommandResponse<Void> response = this.webTestClient.delete()
        .uri(uriBuilder ->
            uriBuilder.path("/api/admin/mealplans/" + MEALPLAN_UUID_ONE.toString())
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void uploadMealplanImage_success() throws IOException {

    UUID mealplanUUID = uuidHelper.randomUUID();
    Mealplan mealplan = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .softDelete(false)
        .build();

    mealplanRepository.save(mealplan).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<GetMealplanResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<GetMealplanResponse>>() {
        };

    MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
    multipartBodyBuilder.part("file", new ClassPathResource("test_image.jpg"))
        .contentType(MediaType.MULTIPART_FORM_DATA);

    BaseCommandResponse<GetMealplanResponse> response = this.webTestClient.post()
        .uri("/api/admin/mealplans/" + MEALPLAN_UUID_ONE.toString() + "/image")
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertNotNull(response.getData().getImageUrl());

    Path fileToDeletePath = Paths.get(Paths.get("storage").toAbsolutePath().normalize().toString(),
        StorageHelper.STORAGE_TYPE.MEALPLAN_IMAGE.getName(),
        MEALPLAN_UUID_ONE.toString());
    Files.delete(fileToDeletePath);
  }

  @Test
  void uploadMealplanImage_failed_unauthorized() {

    Mealplan mealplan = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();

    mealplanRepository.save(mealplan).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<GetMealplanResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<GetMealplanResponse>>() {
        };

    MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
    multipartBodyBuilder.part("file", new ClassPathResource("test_image.jpg"))
        .contentType(MediaType.MULTIPART_FORM_DATA);

    BaseCommandResponse<GetMealplanResponse> response = this.webTestClient.post()
        .uri("/api/admin/mealplans/" + MEALPLAN_UUID_ONE.toString() + "/image")
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  //  STOCK
  //  STOCK
  //  STOCK

  @Test
  void createStock_success() {
    CreateStockRequest request = CreateStockRequest
        .builder()
        .name(STOCK_NAME_ONE)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<StockResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<StockResponse>>() {
        };

    BaseCommandResponse<StockResponse> response = this.webTestClient.post()
        .uri("/api/admin/stocks")
        .body(Mono.just(request), CreateStockRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(STOCK_NAME_ONE, response.getData().getName());
    assertEquals(STOCK_DESC_ONE, response.getData().getDescription());
    assertNull(response.getData().getImageUrl());
    assertEquals(STOCK_QUANTITY_ONE, response.getData().getQuantity());
  }

  @Test
  void createStock_failed_unauthorized() {
    CreateStockRequest request = CreateStockRequest
        .builder()
        .name(STOCK_NAME_ONE)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<StockResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<StockResponse>>() {
        };

    BaseCommandResponse<StockResponse> response = this.webTestClient.post()
        .uri("/api/admin/stocks")
        .body(Mono.just(request), CreateStockRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
    assertNull(response.getData());
  }

  @Test
  void editStock_success() {


    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    stockRepository.save(stock).subscribe();

    EditStockRequest request = EditStockRequest
        .builder()
        .name(STOCK_NAME_TWO)
        .description(STOCK_DESC_TWO)
        .quantity(STOCK_QUANTITY_TWO)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<StockResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<StockResponse>>() {
        };

    BaseCommandResponse<StockResponse> response = this.webTestClient.put()
        .uri("/api/admin/stocks/" + STOCK_UUID_ONE.toString())
        .body(Mono.just(request), EditStockRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(STOCK_NAME_TWO, response.getData().getName());
    assertEquals(STOCK_DESC_TWO, response.getData().getDescription());
    assertNull(response.getData().getImageUrl());
    assertEquals(STOCK_QUANTITY_TWO, response.getData().getQuantity());
  }

  @Test
  void editStock_failed_unauthorized() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();
    stockRepository.save(stock).subscribe();

    EditStockRequest request = EditStockRequest
        .builder()
        .name(STOCK_NAME_TWO)
        .description(STOCK_DESC_TWO)
        .quantity(STOCK_QUANTITY_TWO)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<StockResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<StockResponse>>() {
        };

    BaseCommandResponse<StockResponse> response = this.webTestClient.put()
        .uri("/api/admin/stocks/" + STOCK_UUID_ONE.toString())
        .body(Mono.just(request), EditStockRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
    assertNull(response.getData());
  }

  @Test
  void deleteStock_success() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    stockRepository.save(stock).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<Void>> type =
        new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
        };

    BaseCommandResponse<Void> response = this.webTestClient.delete()
        .uri("/api/admin/stocks/" + STOCK_UUID_ONE.toString())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
  }

  @Test
  void deleteStock_failed_unauthorized() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();
    stockRepository.save(stock).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<Void>> type =
        new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
        };

    BaseCommandResponse<Void> response = this.webTestClient.delete()
        .uri("/api/admin/stocks/" + STOCK_UUID_ONE.toString())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void findByIdStock_success() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();
    stockRepository.save(stock).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<StockResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<StockResponse>>() {
        };

    BaseCommandResponse<StockResponse> response = this.webTestClient.get()
        .uri("/api/admin/stocks/" + STOCK_UUID_ONE.toString())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(STOCK_NAME_ONE, response.getData().getName());
    assertEquals(STOCK_DESC_ONE, response.getData().getDescription());
    assertNull(response.getData().getImageUrl());
    assertEquals(STOCK_QUANTITY_ONE, response.getData().getQuantity());
  }

  @Test
  void findByIdStock_failed_unauthorized() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();
    stockRepository.save(stock).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<StockResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<StockResponse>>() {
        };

    BaseCommandResponse<StockResponse> response = this.webTestClient.get()
        .uri("/api/admin/stocks/" + STOCK_UUID_ONE.toString())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
    assertNull(response.getData());
  }

  @Test
  void findAllWithParamStock_success_noParam() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();
    stockRepository.save(stock).subscribe();

    ParameterizedTypeReference<BasePaginatedCommandResponse<StockResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<StockResponse>>() {
        };

    BasePaginatedCommandResponse<StockResponse> response = this.webTestClient.get()
        .uri("/api/admin/stocks")
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(1, response.getPagination().getTotalItems());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(STOCK_NAME_ONE, response.getData().get(0).getName());
    assertEquals(STOCK_DESC_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(STOCK_QUANTITY_ONE, response.getData().get(0).getQuantity());
  }

  @Test
  void findAllWithParamStock_success_nameParam() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();
    stockRepository.save(stock).subscribe();

    ParameterizedTypeReference<BasePaginatedCommandResponse<StockResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<StockResponse>>() {
        };

    BasePaginatedCommandResponse<StockResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/admin/stocks")
                .queryParam("name", STOCK_NAME_ONE)
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(1, response.getPagination().getTotalItems());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(STOCK_NAME_ONE, response.getData().get(0).getName());
    assertEquals(STOCK_DESC_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(STOCK_QUANTITY_ONE, response.getData().get(0).getQuantity());
  }

  @Test
  void findAllWithParamStock_failed_unauthorized() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    stockRepository.save(stock).subscribe();

    ParameterizedTypeReference<BasePaginatedCommandResponse<StockResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<StockResponse>>() {
        };

    BasePaginatedCommandResponse<StockResponse> response = this.webTestClient.get()
        .uri("/api/admin/stocks")
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
    assertNull(response.getData());
  }

  @Test
  void uploadStockImage_success() throws IOException {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();


    stockRepository.save(stock).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<GetMealplanResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<GetMealplanResponse>>() {
        };

    MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
    multipartBodyBuilder.part("file", new ClassPathResource("test_image.jpg"))
        .contentType(MediaType.MULTIPART_FORM_DATA);

    BaseCommandResponse<GetMealplanResponse> response = this.webTestClient.post()
        .uri("/api/admin/stocks/" + STOCK_UUID_ONE.toString() + "/image")
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertNotNull(response.getData().getImageUrl());

    Path fileToDeletePath = Paths.get(Paths.get("storage").toAbsolutePath().normalize().toString(),
        StorageHelper.STORAGE_TYPE.STOCK_IMAGE.getName(),
        STOCK_UUID_ONE.toString());
    Files.delete(fileToDeletePath);
  }

  @Test
  void uploadStockImage_failed_unauthorized() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();


    stockRepository.save(stock).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<GetMealplanResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<GetMealplanResponse>>() {
        };

    MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
    multipartBodyBuilder.part("file", new ClassPathResource("test_image.jpg"))
        .contentType(MediaType.MULTIPART_FORM_DATA);

    BaseCommandResponse<GetMealplanResponse> response = this.webTestClient.post()
        .uri("/api/admin/stocks/" + STOCK_UUID_ONE.toString() + "/image")
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());

  }

  @Test
  void checkStock_success() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    Stock stock_two = Stock
        .builder()
        .id(STOCK_UUID_TWO)
        .name(STOCK_NAME_TWO)
        .imageUrl(null)
        .description(STOCK_DESC_TWO)
        .quantity(STOCK_QUANTITY_TWO)
        .softDelete(false)
        .build();

    Necessity necessity = Necessity.builder()
        .id(UUID.randomUUID())
        .timestamp(MONDAY)
        .ingredientList(Arrays.asList(
            Ingredient.builder()
                .stockId(STOCK_UUID_ONE.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_ONE)
                .build(),
            Ingredient.builder()
                .stockId(STOCK_UUID_TWO.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_TWO)
                .build()
        )).build();

    stockRepository.save(stock_one).subscribe();
    stockRepository.save(stock_two).subscribe();
    necessityRepository.save(necessity).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<CheckStockResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<CheckStockResponse>>() {
        };

    BaseCommandResponse<CheckStockResponse> response = this.webTestClient.get()
        .uri("/api/admin/stocks/check-stocks/"+MONDAY)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MONDAY,response.getData().getTimestamp());
//    assertEquals(MENU_INGREDIENT_QUANTITY_ONE - STOCK_QUANTITY_ONE,response.getData().getDeficitList().get(0).getQuantity());
//    assertEquals(MENU_INGREDIENT_QUANTITY_TWO - STOCK_QUANTITY_TWO,response.getData().getDeficitList().get(1).getQuantity());
    // gmn cara test kalau ngga sequential ya?
  }



  // BANNER
  // BANNER
  // BANNER

  @Test
  void uploadBannerImage_success() throws IOException {

    ParameterizedTypeReference<BaseCommandResponse<BannerResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<BannerResponse>>() {
        };

    MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
    multipartBodyBuilder.part("file", new ClassPathResource("test_image.jpg"))
        .contentType(MediaType.MULTIPART_FORM_DATA);

    BaseCommandResponse<BannerResponse> response = this.webTestClient.post()
        .uri("/api/admin/banners")
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertNotNull(response.getData().getImageUrl());

    Path fileToDeletePath = Paths.get(Paths.get("storage").toAbsolutePath().normalize().toString(),
        StorageHelper.STORAGE_TYPE.BANNER_IMAGE.getName(),
        response.getData().getId().toString());
    Files.delete(fileToDeletePath);

  }

  @Test
  void uploadBannerImage_failed() {

    ParameterizedTypeReference<BaseCommandResponse<BannerResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<BannerResponse>>() {
        };

    MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
    multipartBodyBuilder.part("file", new ClassPathResource("test_image.jpg"))
        .contentType(MediaType.MULTIPART_FORM_DATA);

    BaseCommandResponse<BannerResponse> response = this.webTestClient.post()
        .uri("/api/admin/banners")
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());

  }

  @Test
  void deleteBannerImage_success() throws IOException {

    ParameterizedTypeReference<BaseCommandResponse<BannerResponse>> upload_type =
        new ParameterizedTypeReference<BaseCommandResponse<BannerResponse>>() {
        };

    MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
    multipartBodyBuilder.part("file", new ClassPathResource("test_image.jpg"))
        .contentType(MediaType.MULTIPART_FORM_DATA);

    BaseCommandResponse<BannerResponse> response = this.webTestClient.post()
        .uri("/api/admin/banners")
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
        .exchange()
        .expectBody(upload_type)
        .returnResult().getResponseBody();

    String success_banner_id = response.getData().getId();
    System.out.println(success_banner_id);

    ParameterizedTypeReference<BaseCommandResponse<Void>> delete_type =
        new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
        };

    BaseCommandResponse<Void> deleteResponse = this.webTestClient.delete()
        .uri("/api/admin/banners/"+success_banner_id)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(delete_type)
        .returnResult().getResponseBody();

    assertEquals(HttpStatus.OK.value(),deleteResponse.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(),deleteResponse.getStatus());
    assertNull(deleteResponse.getData());


    storageHelper.deleteFile(StorageHelper.STORAGE_TYPE.BANNER_IMAGE, uuidHelper.fromString(success_banner_id)).subscribe();

  }

  @Test
  void deleteBannerImage_failed_unauthorized() throws IOException {

    ParameterizedTypeReference<BaseCommandResponse<BannerResponse>> upload_type =
        new ParameterizedTypeReference<BaseCommandResponse<BannerResponse>>() {
        };

    MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
    multipartBodyBuilder.part("file", new ClassPathResource("test_image.jpg"))
        .contentType(MediaType.MULTIPART_FORM_DATA);

    BaseCommandResponse<BannerResponse> response = this.webTestClient.post()
        .uri("/api/admin/banners")
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
        .exchange()
        .expectBody(upload_type)
        .returnResult().getResponseBody();

    ParameterizedTypeReference<BaseCommandResponse<Void>> delete_type =
        new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
        };

    String success_banner_id = response.getData().getId();

    BaseCommandResponse<Void> deleteResponse = this.webTestClient.delete()
        .uri("/api/admin/banners/"+success_banner_id)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(delete_type)
        .returnResult().getResponseBody();

    assertEquals(HttpStatus.UNAUTHORIZED.value(),deleteResponse.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(),deleteResponse.getStatus());

    Path fileToDeletePath = Paths.get(Paths.get("storage").toAbsolutePath().normalize().toString(),
        StorageHelper.STORAGE_TYPE.BANNER_IMAGE.getName(),
        response.getData().getId().toString());
    Files.delete(fileToDeletePath);


  }

  // MENU
  // MENU
  // MENU

  @Test
  void createMenu_success() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    Mealplan mealplan = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .softDelete(false)
        .build();

    stockRepository.save(stock).subscribe();
    mealplanRepository.save(mealplan).subscribe();

    CreateMenuRequest request = CreateMenuRequest
        .builder()
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .build();

    ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>>() {
        };

    BaseCommandResponse<MenuAdminResponse> response = this.webTestClient.post()
        .uri("/api/admin/menus")
        .body(Mono.just(request), CreateMenuRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().getDescription());
    assertNull(response.getData().getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().getCalorie());
    assertEquals(MEALPLAN_UUID_ONE, response.getData().getMealplan().getId());
    assertEquals(STOCK_UUID_ONE, response.getData().getRecipes().get(0).getStock().getId());
    assertEquals(MENU_INGREDIENT_QUANTITY_ONE, response.getData().getRecipes().get(0).getQuantity());

  }

  @Test
  void createMenu_failed_unauthorized() {

    Stock stock = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();


    Mealplan mealplan = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();

    stockRepository.save(stock).subscribe();
    mealplanRepository.save(mealplan).subscribe();

    CreateMenuRequest request = CreateMenuRequest
        .builder()
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .build();

    ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>>() {
        };

    BaseCommandResponse<MenuAdminResponse> response = this.webTestClient.post()
        .uri("/api/admin/menus")
        .body(Mono.just(request), CreateMenuRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
    assertNull(response.getData());
  }

  @Test
  void editMenu_success_fullBody() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    Stock stock_two = Stock
        .builder()
        .id(STOCK_UUID_TWO)
        .name(STOCK_NAME_TWO)
        .imageUrl(null)
        .description(STOCK_DESC_TWO)
        .quantity(STOCK_QUANTITY_TWO)
        .softDelete(false)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .softDelete(false)
        .build();

    Mealplan mealplan_two = Mealplan
        .builder()
        .id(MEALPLAN_UUID_TWO)
        .name(MEALPLAN_NAME_TWO)
        .description(MEALPLAN_DESCRIPTION_TWO)
        .softDelete(false)
        .build();

    UUID menuUUID = uuidHelper.randomUUID();
    Menu menu_one = Menu.builder()
        .id(menuUUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .softDelete(false)
        .build();

    stockRepository.save(stock_one).subscribe();
    stockRepository.save(stock_two).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    mealplanRepository.save(mealplan_two).subscribe();
    menuRepository.save(menu_one).subscribe();


    EditMenuRequest request = EditMenuRequest
        .builder()
        .name(MENU_NAME_TWO)
        .description(MENU_DESCRIPTION_TWO)
        .calorie(MENU_CALORIE_TWO)
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_TWO.toString()).quantity(MENU_INGREDIENT_QUANTITY_TWO).build()))
        .mealplanId(MEALPLAN_UUID_TWO.toString())
        .build();

    ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>>() {
        };

    BaseCommandResponse<MenuAdminResponse> response = this.webTestClient.put()
        .uri("/api/admin/menus/" + menuUUID)
        .body(Mono.just(request), EditMenuRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_TWO, response.getData().getName());
    assertEquals(MENU_DESCRIPTION_TWO, response.getData().getDescription());
    assertNull(response.getData().getImageUrl());
    assertEquals(MENU_CALORIE_TWO, response.getData().getCalorie());
    assertEquals(MEALPLAN_UUID_TWO, response.getData().getMealplan().getId());
    assertEquals(STOCK_UUID_TWO, response.getData().getRecipes().get(0).getStock().getId());
    assertEquals(MENU_INGREDIENT_QUANTITY_TWO, response.getData().getRecipes().get(0).getQuantity());
  }

  @Test
  void editMenu_success_1toNFields() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .imageUrl(null)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .softDelete(false)
        .build();

    UUID menuUUID = uuidHelper.randomUUID();
    Menu menu_one = Menu.builder()
        .id(menuUUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .softDelete(false)
        .build();

    stockRepository.save(stock_one).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    menuRepository.save(menu_one).subscribe();


    EditMenuRequest request = EditMenuRequest
        .builder()
        .name(MENU_NAME_TWO)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>>() {
        };

    BaseCommandResponse<MenuAdminResponse> response = this.webTestClient.put()
        .uri("/api/admin/menus/" + menuUUID)
        .body(Mono.just(request), EditMenuRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_TWO, response.getData().getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().getDescription());
    assertNull(response.getData().getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().getCalorie());
    assertEquals(MEALPLAN_UUID_ONE, response.getData().getMealplan().getId());
    assertEquals(STOCK_UUID_ONE, response.getData().getRecipes().get(0).getStock().getId());
    assertEquals(MENU_INGREDIENT_QUANTITY_ONE, response.getData().getRecipes().get(0).getQuantity());

  }

  @Test
  void editMenu_failed_unauthorized() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .imageUrl(null)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();

    UUID menuUUID = uuidHelper.randomUUID();
    Menu menu_one = Menu.builder()
        .id(menuUUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .build();

    stockRepository.save(stock_one).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    menuRepository.save(menu_one).subscribe();


    EditMenuRequest request = EditMenuRequest
        .builder()
        .name(MENU_NAME_TWO)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>>() {
        };

    BaseCommandResponse<MenuAdminResponse> response = this.webTestClient.put()
        .uri("/api/admin/menus/" + menuUUID)
        .body(Mono.just(request), EditMenuRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void deleteMenu_success() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    Stock stock_two = Stock
        .builder()
        .id(STOCK_UUID_TWO)
        .name(STOCK_NAME_TWO)
        .imageUrl(null)
        .description(STOCK_DESC_TWO)
        .quantity(STOCK_QUANTITY_TWO)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();

    Mealplan mealplan_two = Mealplan
        .builder()
        .id(MEALPLAN_UUID_TWO)
        .name(MEALPLAN_NAME_TWO)
        .description(MEALPLAN_DESCRIPTION_TWO)
        .build();

    UUID menuUUID = uuidHelper.randomUUID();
    Menu menu_one = Menu.builder()
        .id(menuUUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .build();

    stockRepository.save(stock_one).subscribe();
    stockRepository.save(stock_two).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    mealplanRepository.save(mealplan_two).subscribe();
    menuRepository.save(menu_one).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<Void>> type =
        new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
        };

    BaseCommandResponse<Void> response = this.webTestClient.delete()
        .uri("/api/admin/menus/" + menuUUID)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
  }

  @Test
  void deleteMenu_failed_unauthorized() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    Stock stock_two = Stock
        .builder()
        .id(STOCK_UUID_TWO)
        .name(STOCK_NAME_TWO)
        .imageUrl(null)
        .description(STOCK_DESC_TWO)
        .quantity(STOCK_QUANTITY_TWO)
        .softDelete(false)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .softDelete(false)
        .build();

    Mealplan mealplan_two = Mealplan
        .builder()
        .id(MEALPLAN_UUID_TWO)
        .name(MEALPLAN_NAME_TWO)
        .description(MEALPLAN_DESCRIPTION_TWO)
        .softDelete(false)
        .build();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .build();

    stockRepository.save(stock_one).subscribe();
    stockRepository.save(stock_two).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    mealplanRepository.save(mealplan_two).subscribe();
    menuRepository.save(menu_one).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<Void>> type =
        new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
        };

    BaseCommandResponse<Void> response = this.webTestClient.delete()
        .uri("/api/admin/menus/" + MENU_UUID)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void findMenuByName_success() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .softDelete(false)
        .build();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .build();

    stockRepository.save(stock_one).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    menuRepository.save(menu_one).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>>() {
        };

    BaseCommandResponse<MenuAdminResponse> response = this.webTestClient.get()
        .uri("/api/admin/menus/" + MENU_UUID)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().getDescription());
    assertNull(response.getData().getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().getCalorie());
    assertEquals(MEALPLAN_UUID_ONE, response.getData().getMealplan().getId());
    assertEquals(STOCK_UUID_ONE, response.getData().getRecipes().get(0).getStock().getId());
    assertEquals(MENU_INGREDIENT_QUANTITY_ONE, response.getData().getRecipes().get(0).getQuantity());

  }

  @Test
  void findMenuByName_failed_unauthorized() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .softDelete(false)
        .build();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .build();

    stockRepository.save(stock_one).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    menuRepository.save(menu_one).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>>() {
        };

    BaseCommandResponse<MenuAdminResponse> response = this.webTestClient.get()
        .uri("/api/admin/menus/" + MENU_UUID)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());

  }

  @Test
  void findAllMenu_noParam_success() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .softDelete(false)
        .build();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .softDelete(false)
        .build();

    stockRepository.save(stock_one).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    menuRepository.save(menu_one).subscribe();

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuAdminResponse>>() {
        };

    BasePaginatedCommandResponse<MenuAdminResponse> response = this.webTestClient.get()
        .uri("/api/admin/menus")
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    System.out.println(response.getData());
    assertEquals(MENU_NAME_ONE, response.getData().get(0).getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().get(0).getCalorie());
    assertEquals(MEALPLAN_UUID_ONE, response.getData().get(0).getMealplan().getId());
    assertEquals(STOCK_UUID_ONE, response.getData().get(0).getRecipes().get(0).getStock().getId());
    assertEquals(MENU_INGREDIENT_QUANTITY_ONE, response.getData().get(0).getRecipes().get(0).getQuantity());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(1, response.getPagination().getTotalItems());
  }

  @Test
  void findAllMenu_noParam_failed() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .build();

    stockRepository.save(stock_one).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    menuRepository.save(menu_one).subscribe();

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuAdminResponse>>() {
        };

    BasePaginatedCommandResponse<MenuAdminResponse> response = this.webTestClient.get()
        .uri("/api/admin/menus")
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void findAllMenu_withNameParam_success() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .softDelete(false)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .softDelete(false)
        .build();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .softDelete(false)
        .build();

    stockRepository.save(stock_one).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    menuRepository.save(menu_one).subscribe();

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuAdminResponse>>() {
        };

    BasePaginatedCommandResponse<MenuAdminResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/admin/menus")
                .queryParam("name", MENU_NAME_ONE)
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().get(0).getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().get(0).getDescription());
    assertNull(response.getData().get(0).getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().get(0).getCalorie());
    assertEquals(MEALPLAN_UUID_ONE, response.getData().get(0).getMealplan().getId());
    assertEquals(STOCK_UUID_ONE, response.getData().get(0).getRecipes().get(0).getStock().getId());
    assertEquals(MENU_INGREDIENT_QUANTITY_ONE, response.getData().get(0).getRecipes().get(0).getQuantity());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(0, response.getPagination().getCurrentPage());
    assertEquals(1, response.getPagination().getTotalItems());
  }

  @Test
  void findAllMenu_withNameParam_failed_unauthorized() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .build();

    stockRepository.save(stock_one).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    menuRepository.save(menu_one).subscribe();

    ParameterizedTypeReference<BasePaginatedCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<MenuAdminResponse>>() {
        };

    BasePaginatedCommandResponse<MenuAdminResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/admin/menus")
                .queryParam("name", MENU_NAME_ONE)
                .build()
        )
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void uploadMenuImage_success() throws IOException {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .softDelete(false)
        .build();

    stockRepository.save(stock_one).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    menuRepository.save(menu_one).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>>() {
        };

    MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
    multipartBodyBuilder.part("file", new ClassPathResource("test_image.jpg"))
        .contentType(MediaType.MULTIPART_FORM_DATA);


    BaseCommandResponse<MenuAdminResponse> response = this.webTestClient.post()
        .uri("/api/admin/menus/" + MENU_UUID.toString() + "/image")
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MENU_NAME_ONE, response.getData().getName());
    assertEquals(MENU_DESCRIPTION_ONE, response.getData().getDescription());
    assertNotNull(response.getData().getImageUrl());
    assertEquals(MENU_CALORIE_ONE, response.getData().getCalorie());
    assertEquals(MEALPLAN_UUID_ONE, response.getData().getMealplan().getId());
    assertEquals(STOCK_UUID_ONE, response.getData().getRecipes().get(0).getStock().getId());
    assertEquals(MENU_INGREDIENT_QUANTITY_ONE, response.getData().getRecipes().get(0).getQuantity());

    Path fileToDeletePath = Paths.get(Paths.get("storage").toAbsolutePath().normalize().toString(),
        StorageHelper.STORAGE_TYPE.MENU_IMAGE.getName(),
        MENU_UUID.toString());
    Files.delete(fileToDeletePath);

  }

  @Test
  void uploadMenuImage_failed_unauthorized() {
    Stock stock_one = Stock
        .builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .imageUrl(null)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    Mealplan mealplan_one = Mealplan
        .builder()
        .id(MEALPLAN_UUID_ONE)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .build();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID_ONE.toString())
        .recipes(Arrays.asList(Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build()))
        .build();

    stockRepository.save(stock_one).subscribe();
    mealplanRepository.save(mealplan_one).subscribe();
    menuRepository.save(menu_one).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<MenuAdminResponse>>() {
        };

    MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
    multipartBodyBuilder.part("file", new ClassPathResource("test_image.jpg"))
        .contentType(MediaType.MULTIPART_FORM_DATA);


    BaseCommandResponse<MenuAdminResponse> response = this.webTestClient.post()
        .uri("/api/admin/menus/" + MENU_UUID.toString() + "/image")
        .contentType(MediaType.MULTIPART_FORM_DATA)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  // Subscription
  // Subscription
  // Subscription
  // Ditaruh di SubscriptionControllerTests




}
