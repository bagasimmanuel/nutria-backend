package com.blibli.nutria.nutria.controller;

import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.GetMealplanResponse;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MealplanControllerTests {

  public static final UUID MEALPLAN_UUID = UUID.randomUUID();
  public static final UUID MEALPLAN_UUID_TWO = UUID.randomUUID();
  public static final String MEALPLAN_NAME = "MEALPLAN_ONE";
  public static final String MEALPLAN_DESCRIPTION = "MEALPLAN_ONE_DESCRIPTION";
  public static final String MEALPLAN_IMAGE_URL = "/files/images/mealplan/1";

  @Autowired
  private WebTestClient webTestClient;
  @Autowired
  private MealplanRepository mealplanRepository;
  @Autowired
  private UUIDHelper uuidHelper;

  @BeforeAll
  public void setup(){
  }

  @AfterEach
  void teardown() {
    mealplanRepository.deleteAll().subscribe();
  }

  @Test
  void getMealplan_success(){
    Mealplan mealplan_one = Mealplan.builder()
        .id(MEALPLAN_UUID)
        .name(MEALPLAN_NAME)
        .description(MEALPLAN_DESCRIPTION)
        .softDelete(false)
        .imageUrl(MEALPLAN_IMAGE_URL).build();

    Mealplan mealplan_two = Mealplan.builder()
        .id(MEALPLAN_UUID_TWO)
        .name(MEALPLAN_NAME)
        .description(MEALPLAN_DESCRIPTION)
        .softDelete(true)
        .imageUrl(MEALPLAN_IMAGE_URL).build();

    mealplanRepository.save(mealplan_one).subscribe();
    mealplanRepository.save(mealplan_two).subscribe();

    ParameterizedTypeReference<BasePaginatedCommandResponse<GetMealplanResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<GetMealplanResponse>>() {
        };

    BasePaginatedCommandResponse<GetMealplanResponse> response = this.webTestClient.get()
        .uri("/api/mealplans").exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(),response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(),response.getStatus());
    assertEquals(MEALPLAN_NAME,response.getData().get(0).getName());
    assertEquals(MEALPLAN_DESCRIPTION,response.getData().get(0).getDescription());
    assertEquals(MEALPLAN_IMAGE_URL,response.getData().get(0).getImageUrl());
    assertEquals(0,response.getPagination().getCurrentPage());
    assertEquals(10,response.getPagination().getItemsPerPage());
    assertEquals(1,response.getPagination().getTotalItems());
  }

  @Test
  void getMealplan_success_withNameParam(){

    Mealplan mealplan_one = Mealplan.builder()
        .id(MEALPLAN_UUID)
        .name(MEALPLAN_NAME)
        .description(MEALPLAN_DESCRIPTION)
        .imageUrl(MEALPLAN_IMAGE_URL)
        .softDelete(false)
        .build();

    Mealplan mealplan_two = Mealplan.builder()
        .id(MEALPLAN_UUID_TWO)
        .name(MEALPLAN_NAME)
        .description(MEALPLAN_DESCRIPTION)
        .imageUrl(MEALPLAN_IMAGE_URL)
        .softDelete(true)
        .build();

    mealplanRepository.save(mealplan_one).subscribe();
    mealplanRepository.save(mealplan_two).subscribe();

    ParameterizedTypeReference<BasePaginatedCommandResponse<GetMealplanResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<GetMealplanResponse>>() {
        };

    BasePaginatedCommandResponse<GetMealplanResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/mealplans")
                .queryParam("name","MEALPLAN")
                .build()
        )
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(),response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(),response.getStatus());
    assertEquals(MEALPLAN_NAME,response.getData().get(0).getName());
    assertEquals(MEALPLAN_DESCRIPTION,response.getData().get(0).getDescription());
    assertEquals(MEALPLAN_IMAGE_URL,response.getData().get(0).getImageUrl());
    assertEquals(0,response.getPagination().getCurrentPage());
    assertEquals(10,response.getPagination().getItemsPerPage());
    assertEquals(1,response.getPagination().getTotalItems());
  }

  @Test
  void findMealplan_ById_success(){
    Mealplan mealplan = Mealplan.builder()
        .id(MEALPLAN_UUID)
        .name(MEALPLAN_NAME)
        .description(MEALPLAN_DESCRIPTION)
        .imageUrl(MEALPLAN_IMAGE_URL)
        .softDelete(false)
        .build();

    mealplanRepository.save(mealplan).subscribe();

    ParameterizedTypeReference<BaseCommandResponse<GetMealplanResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<GetMealplanResponse>>() {
        };

    BaseCommandResponse<GetMealplanResponse> response = this.webTestClient.get()
        .uri("/api/mealplans/"+MEALPLAN_UUID.toString())
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(),response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(),response.getStatus());
    assertEquals(MEALPLAN_NAME,response.getData().getName());
    assertEquals(MEALPLAN_DESCRIPTION,response.getData().getDescription());
    assertEquals(MEALPLAN_IMAGE_URL,response.getData().getImageUrl());
  }

}
