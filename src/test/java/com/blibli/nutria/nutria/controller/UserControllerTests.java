package com.blibli.nutria.nutria.controller;

import com.blibli.nutria.nutria.exception.BadCredentialsException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.user.ChangePasswordRequest;
import com.blibli.nutria.nutria.model.command.request.user.EditProfileUserRequest;
import com.blibli.nutria.nutria.model.command.request.user.LoginUserRequest;
import com.blibli.nutria.nutria.model.command.request.user.NewAccessTokenRequest;
import com.blibli.nutria.nutria.model.command.request.user.RegisterUserRequest;
import com.blibli.nutria.nutria.model.command.request.user.VerifyEmailRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.EditProfileUserResponse;
import com.blibli.nutria.nutria.model.command.response.user.LoginUserResponse;
import com.blibli.nutria.nutria.model.command.response.user.NewAccessTokenResponse;
import com.blibli.nutria.nutria.model.command.response.user.RegisterUserResponse;
import com.blibli.nutria.nutria.model.command.response.user.UserProfileResponse;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.model.entity.VerifyToken;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.VerifyTokenRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "36000")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserControllerTests {

  public static final String EMAIL = "test@gmail.com";
  public static final String FULL_NAME = "Test";
  public static final String PASSWORD = "123456";
  public static final User.Gender MALE = User.Gender.MALE;
  public static final User.Gender FEMALE = User.Gender.FEMALE;
  public static final String BEARER_PREFIX = "Bearer ";
  public static final String NEW_PASSWORD = "1234567";
  public static final int HEIGHT = 175;
  public static final int WEIGHT = 72;
  public static final String MEALPLAN_ID_1 = String.valueOf(UUID.randomUUID());
  public static final String MEALPLAN_NAME_1 = "weightloss";
  public static final String MEALPLAN_DESC_1 = "mealplan1";
  public static final String MEALPLAN_ID_2 = String.valueOf(UUID.randomUUID());
  public static final String VERIFY_TOKEN = String.valueOf(UUID.randomUUID());
  public static final UUID USER_UUID = UUID.randomUUID();
  public static final String MEALPLAN_NAME_2 = "postbirth";
  public static final String MEALPLAN_DESC_2 = "mealplan2";
  public static final int AGE = 21;
  public static final List<String> LOCATION = Arrays.asList("Jln. Panjaitan No 91, Lumajang");
  public static final int CALORIE_RECOM_SEDENTARY_FEMALE = 1042;
  public static final int CALORIE_RECOM_ACTIVE_MALE = 2276;
  public static final int CALORIE_RECOM_VERY_MALE = 2589;
  public static final int CALORIE_RECOM_EXTREME_MALE = 2992;
  public static final int CALORIE_RECOM_LIGHT_MALE = 1962;


  @Autowired
  private UserRepository userRepository;
  @Autowired
  private MealplanRepository mealplanRepository;
  @Autowired
  VerifyTokenRepository verifyTokenRepository;
  @Autowired
  private UUIDHelper uuidHelper;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  @Qualifier("springRedisTest")
  private ReactiveRedisTemplate<String, String> reactiveRedisTemplate;
  @Autowired
  @Qualifier("otpRedisTest")
  private ReactiveRedisTemplate<String,String> otpRedisTemplate;
  @Autowired
  private WebTestClient webTestClient;


  @BeforeAll
  public void setUp() {

    Mealplan mealplan = Mealplan.builder()
        .id(UUID.fromString(MEALPLAN_ID_1))
        .name("weightloss")
        .description("mealplan")
        .softDelete(false)
        .build();

    mealplanRepository.save(mealplan).subscribe();

  }


  @AfterEach
  void teardown() {
    userRepository.deleteAll().subscribe();
    reactiveRedisTemplate.delete(reactiveRedisTemplate.keys("*")).subscribe();
    otpRedisTemplate.delete(reactiveRedisTemplate.keys("*")).subscribe();
  }

  @AfterAll
  void cleanUp() {
    userRepository.deleteAll().subscribe();
    mealplanRepository.deleteAll().subscribe();
  }

  @Test
  void registerUser_success() {

    RegisterUserRequest registerUserRequest = RegisterUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .fullName(FULL_NAME)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<RegisterUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<RegisterUserResponse>>() {
        };

    BaseCommandResponse<RegisterUserResponse> response = this.webTestClient.post()
        .uri("/api/users/register")
        .body(Mono.just(registerUserRequest), RegisterUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(200, response.getCode());
    assertEquals(EMAIL, response.getData().getEmail());
    assertEquals(FULL_NAME, response.getData().getFullName());
    assertEquals(true, response.getData().getVerified());
  }

  @Test
  void registerUser_failed_duplicateEmail() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME).build();
    userRepository.save(user).log().subscribe();

    RegisterUserRequest registerUserRequest = RegisterUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .fullName(FULL_NAME)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<RegisterUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<RegisterUserResponse>>() {
        };

    BaseCommandResponse<RegisterUserResponse> response = this.webTestClient.post()
        .uri("/api/users/register")
        .body(Mono.just(registerUserRequest), RegisterUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(400, response.getCode());
  }

  @Test
  void registerUser_failed_emptyBody() {

    ParameterizedTypeReference<BaseCommandResponse<BadCredentialsException>> type =
        new ParameterizedTypeReference<BaseCommandResponse<BadCredentialsException>>() {
        };

    BaseCommandResponse<BadCredentialsException> response = this.webTestClient.post()
        .uri("/api/users/register")
        .body(Mono.empty(), RegisterUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    System.out.println(response.getCode());
    System.out.println(response.getStatus());
    assertEquals(400, response.getCode());
  }

  @Test
  void loginUser_success() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> response = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(200, response.getCode());
    assertEquals(Arrays.asList("ROLE_USER"), response.getData().getRoles());
    assertNotNull(response.getData().getAccessToken());
  }

  @Test
  void loginUser_failed_badCredentials() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email("wrongemail@gmail.com")
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> response = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assertEquals(400, response.getCode());
    System.out.println(response.getStatus());
    assertEquals("Bad Request", response.getStatus());
  }

  @Test
  void loginUser_failed_notVerified() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(false).build();
    userRepository.save(user).log().subscribe();

    otpRedisTemplate.opsForValue().set(EMAIL,"123456").subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> response = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    System.out.println(response);
    assertEquals(400, response.getCode());
    assertEquals("Bad Request", response.getStatus());
    StepVerifier.create(otpRedisTemplate.hasKey(EMAIL)).expectNext(true).verifyComplete();
  }

  @Test
  void loginUser_failed_expiredOtp() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(false).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> response = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    System.out.println(response);
    assertEquals(400, response.getCode());
    assertEquals("Bad Request", response.getStatus());
    StepVerifier.create(otpRedisTemplate.hasKey(EMAIL)).expectNext(true).verifyComplete();
  }

  @Test
  void getCurrentUser_success() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .mealplanId(MEALPLAN_ID_1)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(User.Gender.MALE).build();
    userRepository.save(user).log().subscribe();

    //  Login
    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();

    ParameterizedTypeReference<BaseCommandResponse<UserProfileResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<UserProfileResponse>>() {
        };

    BaseCommandResponse<UserProfileResponse> response = this.webTestClient.get()
        .uri("/api/users/user").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + String.valueOf(accessToken))
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assertNotNull(response);
    assertEquals(200, response.getCode());
    System.out.println(response.getStatus());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MALE, response.getData().getGender());
    assertEquals(FULL_NAME, response.getData().getFullName());
    assertEquals(EMAIL, response.getData().getEmail());
    assertEquals(0, response.getData().getAge());
    assertEquals(0, response.getData().getHeight());
    assertEquals(0, response.getData().getWeight());
  }

  @Test
  void getCurrentUser_failed_invalidToken() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(MALE).build();
    userRepository.save(user).log().subscribe();

    ParameterizedTypeReference<BaseCommandResponse<UserProfileResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<UserProfileResponse>>() {
        };

    BaseCommandResponse<UserProfileResponse> response = this.webTestClient.get()
        .uri("/api/users/user").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + String.valueOf("xxx"))
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assertEquals(401, response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void Logout_success() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(MALE).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();

    ParameterizedTypeReference<BaseCommandResponse<Void>> type = new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
    };

    BaseCommandResponse<Void> response = this.webTestClient.post()
        .uri("/api/users/logout").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();
    // Ini bener ga ya? dee ga ada assert si cuman return response spec
    assertEquals(200, response.getCode());
    assertNull(response.getData());
  }

  @Test
  void Logout_failed_notLoggedIn() {

    ParameterizedTypeReference<BaseCommandResponse<Void>> type = new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
    };

    BaseCommandResponse<Void> response = this.webTestClient.post()
        .uri("/api/users/change-password").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();
    // Ini bener ga ya? dee ga ada assert si cuman return response spec
    assertEquals(401, response.getCode());
  }

  @Test
  void changePassword_success() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(MALE).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();

    ChangePasswordRequest passwordRequest = ChangePasswordRequest.builder()
        .oldPassword(PASSWORD)
        .newPassword(NEW_PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<Void>> type = new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
    };

    BaseCommandResponse<Void> response = this.webTestClient.post()
        .uri("/api/users/change-password").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(Mono.just(passwordRequest), ChangePasswordRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertNull(response.getData());
  }

  @Test
  void changePassword_failed_notLoggedIn() {

    ChangePasswordRequest passwordRequest = ChangePasswordRequest.builder()
        .oldPassword(PASSWORD)
        .newPassword(NEW_PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<Void>> type = new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
    };

    BaseCommandResponse<Void> response = this.webTestClient.put()
        .uri("/api/users/change-password").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .body(Mono.just(passwordRequest), ChangePasswordRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void updateProfile_success_MALE_ACTIVE() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(MALE).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();

    EditProfileUserRequest editProfileUserRequest = EditProfileUserRequest.builder()
        .age(AGE)
        .gender(MALE)
        .height(HEIGHT)
        .weight(WEIGHT)
        .activityLevel(User.ActivityLevel.ACTIVE)
        .mealplanId(MEALPLAN_ID_1)
        .location(LOCATION)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>>() {
        };

    BaseCommandResponse<EditProfileUserResponse> response = this.webTestClient.put()
        .uri("/api/users/profile").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(Mono.just(editProfileUserRequest), EditProfileUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(AGE, response.getData().getAge());
    assertEquals(HEIGHT, response.getData().getHeight());
    assertEquals(LOCATION, response.getData().getLocation());
    assertEquals(MALE, response.getData().getGender());
    assertEquals(MEALPLAN_ID_1, response.getData().getMealplan().getId().toString());
    assertEquals(CALORIE_RECOM_ACTIVE_MALE, response.getData().getCalorieRecom());
    assertEquals(WEIGHT, response.getData().getWeight());
  }

  @Test
  void updateProfile_success_incompleteUserFields() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(MALE).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();

    EditProfileUserRequest editProfileUserRequest = EditProfileUserRequest.builder()
        .gender(User.Gender.MALE)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>>() {
        };

    BaseCommandResponse<EditProfileUserResponse> response = this.webTestClient.put()
        .uri("/api/users/profile").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(Mono.just(editProfileUserRequest), EditProfileUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(0, response.getData().getCalorieRecom());
  }

  @Test
  void updateProfile_success_FEMALE_SEDENTARY() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(User.Gender.MALE).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();

    EditProfileUserRequest editProfileUserRequest = EditProfileUserRequest.builder()
        .age(AGE)
        .gender(User.Gender.FEMALE)
        .height(HEIGHT)
        .weight(WEIGHT)
        .activityLevel(User.ActivityLevel.SEDENTARY)
        .mealplanId(MEALPLAN_ID_1)
        .location(LOCATION)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>>() {
        };

    BaseCommandResponse<EditProfileUserResponse> response = this.webTestClient.put()
        .uri("/api/users/profile").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(Mono.just(editProfileUserRequest), EditProfileUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(AGE, response.getData().getAge());
    assertEquals(HEIGHT, response.getData().getHeight());
    assertEquals(LOCATION, response.getData().getLocation());
    assertEquals(WEIGHT, response.getData().getWeight());
    assertEquals(FEMALE, response.getData().getGender());
    assertEquals(MEALPLAN_ID_1, response.getData().getMealplan().getId().toString());
    assertEquals(CALORIE_RECOM_SEDENTARY_FEMALE, response.getData().getCalorieRecom());
  }

  @Test
  void updateProfile_success_MALE_LIGHT() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(User.Gender.MALE).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();

    EditProfileUserRequest editProfileUserRequest = EditProfileUserRequest.builder()
        .age(AGE)
        .gender(User.Gender.MALE)
        .height(HEIGHT)
        .weight(WEIGHT)
        .activityLevel(User.ActivityLevel.LIGHT)
        .mealplanId(MEALPLAN_ID_1)
        .location(LOCATION)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>>() {
        };

    BaseCommandResponse<EditProfileUserResponse> response = this.webTestClient.put()
        .uri("/api/users/profile").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(Mono.just(editProfileUserRequest), EditProfileUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(AGE, response.getData().getAge());
    assertEquals(HEIGHT, response.getData().getHeight());
    assertEquals(LOCATION, response.getData().getLocation());
    assertEquals(WEIGHT, response.getData().getWeight());
    assertEquals(MALE, response.getData().getGender());
    assertEquals(MEALPLAN_ID_1, response.getData().getMealplan().getId().toString());
    assertEquals(CALORIE_RECOM_LIGHT_MALE, response.getData().getCalorieRecom());
  }

  @Test
  void updateProfile_success_MALE_VERY() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(User.Gender.MALE).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();

    EditProfileUserRequest editProfileUserRequest = EditProfileUserRequest.builder()
        .age(AGE)
        .gender(User.Gender.MALE)
        .height(HEIGHT)
        .weight(WEIGHT)
        .activityLevel(User.ActivityLevel.VERY)
        .mealplanId(MEALPLAN_ID_1)
        .location(LOCATION)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>>() {
        };

    BaseCommandResponse<EditProfileUserResponse> response = this.webTestClient.put()
        .uri("/api/users/profile").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(Mono.just(editProfileUserRequest), EditProfileUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(AGE, response.getData().getAge());
    assertEquals(HEIGHT, response.getData().getHeight());
    assertEquals(LOCATION, response.getData().getLocation());
    assertEquals(WEIGHT, response.getData().getWeight());
    assertEquals(MALE, response.getData().getGender());
    assertEquals(MEALPLAN_ID_1, response.getData().getMealplan().getId().toString());
    assertEquals(CALORIE_RECOM_VERY_MALE, response.getData().getCalorieRecom());
  }

  @Test
  void updateProfile_success_MALE_EXTREME() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(User.Gender.MALE).build();
    userRepository.save(user).log().subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();

    EditProfileUserRequest editProfileUserRequest = EditProfileUserRequest.builder()
        .age(AGE)
        .gender(User.Gender.MALE)
        .height(HEIGHT)
        .weight(WEIGHT)
        .activityLevel(User.ActivityLevel.EXTREME)
        .mealplanId(MEALPLAN_ID_1)
        .location(LOCATION)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<EditProfileUserResponse>>() {
        };

    BaseCommandResponse<EditProfileUserResponse> response = this.webTestClient.put()
        .uri("/api/users/profile").header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .body(Mono.just(editProfileUserRequest), EditProfileUserRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(AGE, response.getData().getAge());
    assertEquals(HEIGHT, response.getData().getHeight());
    assertEquals(LOCATION, response.getData().getLocation());
    assertEquals(WEIGHT, response.getData().getWeight());
    assertEquals(MALE, response.getData().getGender());
    assertEquals(MEALPLAN_ID_1, response.getData().getMealplan().getId().toString());
    assertEquals(CALORIE_RECOM_EXTREME_MALE, response.getData().getCalorieRecom());
  }

  @Test
  void getNewAccessToken_success() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .mealplanId(MEALPLAN_ID_1)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(User.Gender.MALE).build();
    userRepository.save(user).subscribe();

    //  Login
    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();
    UUID refreshToken = loginResponse.getData().getRefreshToken();
    NewAccessTokenRequest tokenRequest = NewAccessTokenRequest.builder().refreshToken(refreshToken.toString()).build();

    ParameterizedTypeReference<BaseCommandResponse<NewAccessTokenResponse>> newAccessTokenType =
        new ParameterizedTypeReference<BaseCommandResponse<NewAccessTokenResponse>>() {
        };

    BaseCommandResponse<NewAccessTokenResponse> tokenResponse = this.webTestClient.post()
        .uri("/api/users/refreshToken")
        .body(Mono.just(tokenRequest), NewAccessTokenRequest.class)
        .exchange()
        .expectBody(newAccessTokenType)
        .returnResult().getResponseBody();

    StepVerifier.create(reactiveRedisTemplate.hasKey(accessToken.toString()))
        .expectNext(false)
        .expectComplete()
        .verify();

    assertNotNull(tokenResponse);
    assertEquals(200, tokenResponse.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), tokenResponse.getStatus());
    assertNotNull(tokenResponse.getData().getRefreshToken());
    assertNotNull(tokenResponse.getData().getAccessToken());
  }

  @Test
  void getNewAccessToken_failed_invalidAccessToken() {

    User user = User.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .fullName(FULL_NAME)
        .mealplanId(MEALPLAN_ID_1)
        .roles(Arrays.asList("USER"))
        .verified(true)
        .gender(User.Gender.MALE).build();
    userRepository.save(user).subscribe();

    //  Login
    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    UUID accessToken = loginResponse.getData().getAccessToken();
    UUID refreshToken = loginResponse.getData().getRefreshToken();
    NewAccessTokenRequest tokenRequest = NewAccessTokenRequest.builder().refreshToken("xxx").build();

    ParameterizedTypeReference<BaseCommandResponse<NewAccessTokenResponse>> newAccessTokenType =
        new ParameterizedTypeReference<BaseCommandResponse<NewAccessTokenResponse>>() {
        };

    BaseCommandResponse<NewAccessTokenResponse> tokenResponse = this.webTestClient.post()
        .uri("/api/users/refreshToken")
        .body(Mono.just(tokenRequest), NewAccessTokenRequest.class)
        .exchange()
        .expectBody(newAccessTokenType)
        .returnResult().getResponseBody();

    assertNotNull(tokenResponse);
    assertEquals(400, tokenResponse.getCode());
    assertEquals(HttpStatus.BAD_REQUEST.getReasonPhrase(), tokenResponse.getStatus());
  }

  @Test
  void verifyEmail_success() {

    VerifyToken verifyToken = VerifyToken.builder()
        .id(uuidHelper.randomUUID())
        .email(EMAIL)
        .verifyToken(VERIFY_TOKEN).build();
    verifyTokenRepository.save(verifyToken).subscribe();

    User user = User.builder()
        .id(USER_UUID)
        .email(EMAIL)
        .password(PASSWORD)
        .fullName(FULL_NAME)
        .mealplanId(MEALPLAN_ID_1)
        .roles(Arrays.asList("USER"))
        .verified(false)
        .gender(User.Gender.MALE).build();

    userRepository.save(user).subscribe();

    otpRedisTemplate.opsForValue().set(EMAIL,"123456").subscribe();

    VerifyEmailRequest verifyEmailRequest = VerifyEmailRequest.builder().email(EMAIL).otp(123456).build();


    ParameterizedTypeReference<BaseCommandResponse<Void>> type =
        new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
        };

    BaseCommandResponse<Void> tokenResponse = this.webTestClient.post()
        .uri("/api/users/verify")
        .body(Mono.just(verifyEmailRequest),VerifyEmailRequest.class)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assertNotNull(tokenResponse);
    assertEquals(HttpStatus.OK.value(), tokenResponse.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), tokenResponse.getStatus());

    StepVerifier.create(userRepository.findFirstByEmail(EMAIL))
        .expectNext(User.builder()
            .id(USER_UUID)
            .email(EMAIL)
            .password(PASSWORD)
            .fullName(FULL_NAME)
            .mealplanId(MEALPLAN_ID_1)
            .roles(Arrays.asList("USER"))
            .verified(true)
            .gender(User.Gender.MALE).build())
        .verifyComplete();
  }

  @Test
  void verifyEmail_failed_tokenExpiredNUserNotRegistered() {

      User user = User.builder()
          .id(USER_UUID)
          .email(EMAIL)
          .password(PASSWORD)
          .fullName(FULL_NAME)
          .mealplanId(MEALPLAN_ID_1)
          .roles(Arrays.asList("USER"))
          .verified(false)
          .gender(User.Gender.MALE).build();

      userRepository.save(user).subscribe();

    VerifyEmailRequest verifyEmailRequest = VerifyEmailRequest.builder().email(EMAIL).otp(123456).build();

      ParameterizedTypeReference<BaseCommandResponse<Void>> type =
          new ParameterizedTypeReference<BaseCommandResponse<Void>>() {
          };

      BaseCommandResponse<Void> response = this.webTestClient.post()
          .uri("/api/users/verify")
          .body(Mono.just(verifyEmailRequest),VerifyEmailRequest.class)
          .exchange()
          .expectBody(type)
          .returnResult().getResponseBody();

    System.out.println(response);
      assertEquals(HttpStatus.BAD_REQUEST.value(), response.getCode());
      assertEquals(HttpStatus.BAD_REQUEST.getReasonPhrase(), response.getStatus());

    }



}
