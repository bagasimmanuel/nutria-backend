package com.blibli.nutria.nutria.controller;


import com.blibli.nutria.nutria.helper.DateHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.subscription.CreateSubscriptionRequest;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionDeliveryStatusRequest;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionItemRequest;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionPaymentStatusRequest;
import com.blibli.nutria.nutria.model.command.request.user.LoginUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.ActiveOrderResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import com.blibli.nutria.nutria.model.command.response.user.LoginUserResponse;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.model.entity.Menu;
import com.blibli.nutria.nutria.model.entity.Necessity;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.NecessityRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.DayOfWeek;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SubscriptionControllerTests {


  public static final String EMAIL = "bagasimmanuel00@gmail.com";
  public static final String ADMIN_EMAIL = "admin@gmail.com";
  public static final String PASSWORD = "123456";
  public static final String FULL_NAME = "Bagas Immanuel";
  private static final String BEARER_PREFIX = "Bearer ";

  public static final String MEALPLAN_NAME_ONE = "MEALPLAN_ONE";
  public static final String MEALPLAN_DESCRIPTION_ONE = "MEALPLAN_ONE_DESCRIPTION";

  public static final String STOCK_NAME_ONE = "STOCK_ONE";
  public static final String STOCK_DESC_ONE = "STOCK_ONE_DESCRIPTION";
  public static final int STOCK_QUANTITY_ONE = 1;
  public static final String STOCK_NAME_TWO = "STOCK_ONE_UPDATED";
  public static final String STOCK_DESC_TWO = "STOCK_ONE_DESCRIPTION_UPDATED";
  public static final int STOCK_QUANTITY_TWO = 2;

  public static final String MENU_NAME_ONE = "MENU_NAME_ONE";
  public static final String MENU_DESCRIPTION_ONE = "MENU_DESCRIPTION_ONE";
  public static final int MENU_CALORIE_ONE = 450;
  public static final int MENU_INGREDIENT_QUANTITY_ONE = 5;
  public static final int MENU_INGREDIENT_QUANTITY_TWO = 4;

  public static final String MENU_NAME_TWO = "MENU_NAME_TWO";
  public static final String MENU_DESCRIPTION_TWO = "MENU_DESCRIPTION_TWO";
  public static final int MENU_CALORIE_TWO = 300;

  public static final UUID ADMIN_UUID = UUID.randomUUID();
  public static final UUID USER_UUID = UUID.randomUUID();
  public static final UUID MEALPLAN_UUID = UUID.randomUUID();
  public static final UUID SUBSCRIPTION_UUID = UUID.randomUUID();
  public static final UUID MENU_UUID = UUID.randomUUID();
  public static final UUID MENU_UUID_TWO = UUID.randomUUID();
  public static final UUID STOCK_UUID_ONE = UUID.randomUUID();
  public static final UUID STOCK_UUID_TWO = UUID.randomUUID();
  public static final UUID MONDAY_NECESSITY_UUID = UUID.randomUUID();

  public static final SubscriptionItem.DeliveryTime LUNCH = SubscriptionItem.DeliveryTime.LUNCH;
  public static final SubscriptionItem.DeliveryTime DINNER = SubscriptionItem.DeliveryTime.DINNER;
  public static final SubscriptionItem.DeliveryStatus NOT_PROCESSED = SubscriptionItem.DeliveryStatus.NOT_PROCESSED;
  public static final String DELIVERY_LOC = "Lokasi 1";
  public static final String DELIVERY_LOC_TWO = "Lokasi 2";
  public static final int DAY_IN_SECONDS = 86400;
  public static final int WEEK_IN_SECONDS = 604800;

  @Autowired
  private MealplanRepository mealplanRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private StockRepository stockRepository;
  @Autowired
  private MenuRepository menuRepository;
  @Autowired
  private SubscriptionRepository subscriptionRepository;
  @Autowired
  private WebTestClient webTestClient;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  private NecessityRepository necessityRepository;
  @Autowired
  private UUIDHelper uuidHelper;
  @Autowired
  private DateHelper dateHelper;
  @Autowired
  @Qualifier("springRedisTest")
  private ReactiveRedisTemplate<String, String> reactiveRedisTemplate;

  private UUID accessToken;
  private UUID adminAccessToken;
  private long MONDAY;
  private long TUESDAY;
  private long WEDNESDAY;
  private long THURSDAY;
  private long FRIDAY;
  private long NEXT_WEEK_MONDAY;
  private long NEXT_WEEK_TUESDAY;
  private long NEXT_WEEK_WEDNESDAY;
  private long NEXT_WEEK_THURSDAY;
  private long NEXT_WEEK_FRIDAY;


  @BeforeAll
  public void setup() {
    User user = User
        .builder()
        .id(USER_UUID)
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .roles(Arrays.asList("USER"))
        .fullName(FULL_NAME)
        .mealplanId(MEALPLAN_UUID.toString())
        .verified(true)
        .build();

    User admin = User
        .builder()
        .id(ADMIN_UUID)
        .email(ADMIN_EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .roles(Arrays.asList("USER", "ADMIN"))
        .fullName(FULL_NAME)
        .verified(true)
        .build();

    userRepository.save(user).subscribe();
    userRepository.save(admin).subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    accessToken = loginResponse.getData().getAccessToken();


    LoginUserRequest adminLoginRequest = LoginUserRequest.builder()
        .email(ADMIN_EMAIL)
        .password(PASSWORD)
        .build();


    BaseCommandResponse<LoginUserResponse> adminLoginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(adminLoginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    adminAccessToken = adminLoginResponse.getData().getAccessToken();

    // Mock Data

    Mealplan mealplan = Mealplan.builder()
        .id(MEALPLAN_UUID)
        .name(MEALPLAN_NAME_ONE)
        .description(MEALPLAN_DESCRIPTION_ONE)
        .imageUrl(null)
        .build();

    Stock stock_ONE = Stock.builder()
        .id(STOCK_UUID_ONE)
        .name(STOCK_NAME_ONE)
        .description(STOCK_DESC_ONE)
        .quantity(STOCK_QUANTITY_ONE)
        .build();

    Stock stock_TWO = Stock.builder()
        .id(STOCK_UUID_TWO)
        .name(STOCK_NAME_TWO)
        .description(STOCK_DESC_TWO)
        .quantity(STOCK_QUANTITY_TWO)
        .build();

    mealplanRepository.save(mealplan).subscribe();
    stockRepository.save(stock_ONE).subscribe();
    stockRepository.save(stock_TWO).subscribe();

    Menu menu_one = Menu.builder()
        .id(MENU_UUID)
        .name(MENU_NAME_ONE)
        .description(MENU_DESCRIPTION_ONE)
        .calorie(MENU_CALORIE_ONE)
        .mealplanId(MEALPLAN_UUID.toString())
        .recipes(Arrays.asList(
            Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build(),
            Ingredient.builder().stockId(STOCK_UUID_TWO.toString()).quantity(MENU_INGREDIENT_QUANTITY_TWO).build()))
        .softDelete(false)
        .build();

    Menu menu_two = Menu.builder()
        .id(MENU_UUID_TWO)
        .name(MENU_NAME_TWO)
        .description(MENU_DESCRIPTION_TWO)
        .calorie(MENU_CALORIE_TWO)
        .mealplanId(MEALPLAN_UUID.toString())
        .recipes(Arrays.asList(
            Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build(),
            Ingredient.builder().stockId(STOCK_UUID_TWO.toString()).quantity(MENU_INGREDIENT_QUANTITY_TWO).build()))
        .softDelete(false)
        .build();

    menuRepository.save(menu_one).subscribe();
    menuRepository.save(menu_two).subscribe();

    MONDAY = dateHelper.fridaySaturdayOrSunday(Instant.now().toEpochMilli() / 1000) ?
        // Kalau hari ini jumat sabtu or minggu, brarti mondaynya start hari minggu depan
        dateHelper.getNextDayOfTheWeek(DayOfWeek.MONDAY) :
        dateHelper.getPreviousOrSameDayOfTheWeek(DayOfWeek.MONDAY); // Knp? soalnya untuk edit suatu subcriptionItem hanya boleh dilakukan maksimal h-1 sedangkan kalau kita coba test
    // hari sabtu, maka sabtu > senin - jumat dan testing slalu error
    TUESDAY = MONDAY + DAY_IN_SECONDS;
    WEDNESDAY = MONDAY + 2 * DAY_IN_SECONDS;
    THURSDAY = MONDAY + 3 * DAY_IN_SECONDS;
    FRIDAY = MONDAY + 4 * DAY_IN_SECONDS;
    NEXT_WEEK_MONDAY = MONDAY + WEEK_IN_SECONDS;
    NEXT_WEEK_TUESDAY = NEXT_WEEK_MONDAY + DAY_IN_SECONDS;
    NEXT_WEEK_WEDNESDAY = NEXT_WEEK_MONDAY + 2 * DAY_IN_SECONDS;
    NEXT_WEEK_THURSDAY = NEXT_WEEK_MONDAY + 3 * DAY_IN_SECONDS;
    NEXT_WEEK_FRIDAY = NEXT_WEEK_MONDAY + 4 * DAY_IN_SECONDS;
  }

  @AfterEach
  void teardown() {
    this.subscriptionRepository.deleteAll().subscribe();
    this.necessityRepository.deleteAll().subscribe();
  }

  @AfterAll
  void cleanUp() {
    this.mealplanRepository.deleteAll().subscribe();
    this.userRepository.deleteAll().subscribe();
    this.stockRepository.deleteAll().subscribe();
    this.menuRepository.deleteAll().subscribe();
    reactiveRedisTemplate.delete(reactiveRedisTemplate.keys("*")).subscribe();
  }


  @Test
  void createSubscription_success() {

    CreateSubscriptionRequest request = CreateSubscriptionRequest
        .builder()
        .timestamp(NEXT_WEEK_MONDAY)
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .timestamp(NEXT_WEEK_MONDAY)
                .menuId(MENU_UUID.toString())
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .timestamp(NEXT_WEEK_TUESDAY)
                .menuId(MENU_UUID.toString())
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .timestamp(NEXT_WEEK_WEDNESDAY)
                .menuId(MENU_UUID.toString())
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .timestamp(NEXT_WEEK_THURSDAY)
                .menuId(MENU_UUID.toString())
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .timestamp(NEXT_WEEK_FRIDAY)
                .menuId(MENU_UUID.toString())
                .build()
        ))
        .build();

    ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>>() {
        };

    BaseCommandResponse<SubscriptionResponse> response = this.webTestClient.post()
        .uri("/api/subscriptions/subscribe")
        .body(Mono.just(request), CreateSubscriptionRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    System.out.println(response.getData());
    System.out.println(response.getData());
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().getSubscriptionList().size());
    assertEquals(NEXT_WEEK_MONDAY, response.getData().getTimestamp());
    assertEquals(MENU_UUID.toString(), response.getData().getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().getSubscriptionList().get(0).getDeliveryLoc());
  }


  @Test
  void createSubscription_failed_invalidDate() {

    CreateSubscriptionRequest request = CreateSubscriptionRequest
        .builder()
        .timestamp(MONDAY)
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder().deliveryTime(LUNCH).deliveryLoc(DELIVERY_LOC).menuId(MENU_UUID.toString()).build(),
            SubscriptionItem.builder().deliveryTime(LUNCH).deliveryLoc(DELIVERY_LOC).menuId(MENU_UUID.toString()).build(),
            SubscriptionItem.builder().deliveryTime(LUNCH).deliveryLoc(DELIVERY_LOC).menuId(MENU_UUID.toString()).build(),
            SubscriptionItem.builder().deliveryTime(LUNCH).deliveryLoc(DELIVERY_LOC).menuId(MENU_UUID.toString()).build(),
            SubscriptionItem.builder().deliveryTime(LUNCH).deliveryLoc(DELIVERY_LOC).menuId(MENU_UUID.toString()).build()
        ))
        .build();

    ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>>() {
        };

    BaseCommandResponse<SubscriptionResponse> response = this.webTestClient.post()
        .uri("/api/subscriptions/subscribe")
        .body(Mono.just(request), CreateSubscriptionRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getCode());
    assertEquals(HttpStatus.BAD_REQUEST.getReasonPhrase(), response.getStatus());
  }

  @Test
  void editSubscriptionPaymentStatusUser_success_cancelled() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();

    EditSubscriptionPaymentStatusRequest request =
        EditSubscriptionPaymentStatusRequest.builder().paymentStatus(Subscription.PaymentStatus.CANCELLED).build();

    ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>>() {
        };

    BaseCommandResponse<SubscriptionResponse> response = this.webTestClient.put()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString())
        .body(Mono.just(request), EditSubscriptionPaymentStatusRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(Subscription.PaymentStatus.CANCELLED, response.getData().getPaymentStatus());
  }

  @Test
  void editSubscriptionPaymentStatusUser_failed_paid() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();

    EditSubscriptionPaymentStatusRequest request =
        EditSubscriptionPaymentStatusRequest.builder().paymentStatus(Subscription.PaymentStatus.PAID).build();

    ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>>() {
        };

    BaseCommandResponse<SubscriptionResponse> response = this.webTestClient.put()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString())
        .body(Mono.just(request), EditSubscriptionPaymentStatusRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + "xxx")
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }

  @Test
  void editSubscriptionPaymentStatusAdmin_success_paid() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();

    EditSubscriptionPaymentStatusRequest request =
        EditSubscriptionPaymentStatusRequest.builder().paymentStatus(Subscription.PaymentStatus.PAID).build();

    ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>>() {
        };

    BaseCommandResponse<SubscriptionResponse> response = this.webTestClient.put()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString())
        .body(Mono.just(request), EditSubscriptionPaymentStatusRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(Subscription.PaymentStatus.PAID, response.getData().getPaymentStatus());
  }

  @Test
  void findByIdSubscription_success() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();


    ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>>() {
        };

    BaseCommandResponse<SubscriptionResponse> response = this.webTestClient.get()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().getSubscriptionList().size());
    assertEquals(MENU_UUID.toString(), response.getData().getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().getSubscriptionList().get(0).getDeliveryLoc());
  }

  @Test
  void getAllSubscriptionUser_success_userIdOnly() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();


    ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>>() {
        };

    BasePaginatedCommandResponse<SubscriptionResponse> response = this.webTestClient.get()
        .uri("/api/subscriptions")
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().get(0).getSubscriptionList().size());
    assertEquals(MENU_UUID.toString(), response.getData().get(0).getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().get(0).getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().get(0).getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().get(0).getSubscriptionList().get(0).getDeliveryLoc());
    assertEquals(1, response.getPagination().getTotalItems());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(0, response.getPagination().getCurrentPage());
  }

  @Test
  void getAllSubscriptionUser_success_userIdNTimestamp() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .build())
        )
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();


    ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>>() {
        };

    BasePaginatedCommandResponse<SubscriptionResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/subscriptions")
                .queryParam("timestamp", NEXT_WEEK_MONDAY)
                .build())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(3, response.getData().get(0).getSubscriptionList().size());
    assertEquals(MENU_UUID.toString(), response.getData().get(0).getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().get(0).getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().get(0).getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().get(0).getSubscriptionList().get(0).getDeliveryLoc());
    assertEquals(1, response.getPagination().getTotalItems());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(0, response.getPagination().getCurrentPage());
  }

  @Test
  void getAllSubscriptionUser_success_userIdNPaymentStatus() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
                SubscriptionItem.builder()
                    .deliveryTime(LUNCH)
                    .deliveryLoc(DELIVERY_LOC)
                    .menuId(MENU_UUID.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(MONDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(LUNCH)
                    .deliveryLoc(DELIVERY_LOC)
                    .menuId(MENU_UUID.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(TUESDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(LUNCH)
                    .deliveryLoc(DELIVERY_LOC)
                    .menuId(MENU_UUID.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(WEDNESDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(LUNCH)
                    .deliveryLoc(DELIVERY_LOC)
                    .menuId(MENU_UUID.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(THURSDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(LUNCH)
                    .deliveryLoc(DELIVERY_LOC)
                    .menuId(MENU_UUID.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(FRIDAY)
                    .build()
            )
        )
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();


    ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>>() {
        };

    BasePaginatedCommandResponse<SubscriptionResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/subscriptions")
                .queryParam("paymentStatus", Subscription.PaymentStatus.UNPAID)
                .build())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().get(0).getSubscriptionList().size());
    assertEquals(MENU_UUID.toString(), response.getData().get(0).getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().get(0).getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().get(0).getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().get(0).getSubscriptionList().get(0).getDeliveryLoc());
    assertEquals(1, response.getPagination().getTotalItems());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(0, response.getPagination().getCurrentPage());
  }

  @Test
  void getAllSubscriptionUser_success_userIdNTimestampNPaymentStatus() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build())
        )
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();


    ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>>() {
        };

    BasePaginatedCommandResponse<SubscriptionResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/subscriptions")
                .queryParam("timestamp", NEXT_WEEK_MONDAY)
                .queryParam("paymentStatus", Subscription.PaymentStatus.UNPAID)
                .build())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().get(0).getSubscriptionList().size());
    assertEquals(MENU_UUID.toString(), response.getData().get(0).getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().get(0).getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().get(0).getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().get(0).getSubscriptionList().get(0).getDeliveryLoc());
    assertEquals(1, response.getPagination().getTotalItems());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(0, response.getPagination().getCurrentPage());
  }

  @Test
  void getAllSubscriptionAdmin_success_timestampOnly() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build())
        )
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();


    ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>>() {
        };

    BasePaginatedCommandResponse<SubscriptionResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/subscriptions")
                .queryParam("timestamp", NEXT_WEEK_MONDAY)
                .build())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().get(0).getSubscriptionList().size());
    assertEquals(MENU_UUID.toString(), response.getData().get(0).getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().get(0).getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().get(0).getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().get(0).getSubscriptionList().get(0).getDeliveryLoc());
    assertEquals(1, response.getPagination().getTotalItems());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(0, response.getPagination().getCurrentPage());
  }

  @Test
  void getAllSubscriptionAdmin_success_paymentStatusOnly() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build())
        )
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();


    ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>>() {
        };

    BasePaginatedCommandResponse<SubscriptionResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/subscriptions")
                .queryParam("paymentStatus", Subscription.PaymentStatus.UNPAID)
                .build())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().get(0).getSubscriptionList().size());
    assertEquals(MENU_UUID.toString(), response.getData().get(0).getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().get(0).getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().get(0).getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().get(0).getSubscriptionList().get(0).getDeliveryLoc());
    assertEquals(1, response.getPagination().getTotalItems());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(0, response.getPagination().getCurrentPage());
  }

  @Test
  void getAllSubscriptionAdmin_success_timestampNpaymentStatus() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build())
        )
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();


    ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>>() {
        };

    BasePaginatedCommandResponse<SubscriptionResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/subscriptions")
                .queryParam("timestamp", NEXT_WEEK_MONDAY)
                .queryParam("paymentStatus", Subscription.PaymentStatus.UNPAID)
                .build())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().get(0).getSubscriptionList().size());
    assertEquals(MENU_UUID.toString(), response.getData().get(0).getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().get(0).getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().get(0).getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().get(0).getSubscriptionList().get(0).getDeliveryLoc());
    assertEquals(1, response.getPagination().getTotalItems());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(0, response.getPagination().getCurrentPage());
  }

  @Test
  void getAllSubscriptionAdmin_success_noParam() {
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build())
        )
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(NEXT_WEEK_MONDAY)
        .renewal(false)
        .build()).subscribe();


    ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<SubscriptionResponse>>() {
        };

    BasePaginatedCommandResponse<SubscriptionResponse> response = this.webTestClient.get()
        .uri(uriBuilder ->
            uriBuilder.path("/api/subscriptions")
                .build())
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().get(0).getSubscriptionList().size());
    assertEquals(MENU_UUID.toString(), response.getData().get(0).getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().get(0).getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().get(0).getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().get(0).getSubscriptionList().get(0).getDeliveryLoc());
    assertEquals(1, response.getPagination().getTotalItems());
    assertEquals(10, response.getPagination().getItemsPerPage());
    assertEquals(0, response.getPagination().getCurrentPage());
  }


  @Test
  void renewSubscription_success() {
    System.out.println(MONDAY);
    System.out.println(NEXT_WEEK_MONDAY);
    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build())
        )
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.PAID)
        .timestamp(MONDAY)
        .renewal(false)
        .build()).subscribe();


    ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>>() {
        };

    BaseCommandResponse<SubscriptionResponse> response = this.webTestClient.post()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString() + "/renewal")
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().getSubscriptionList().size());
    assertEquals(NEXT_WEEK_MONDAY, response.getData().getTimestamp());
    assertEquals(MENU_UUID.toString(), response.getData().getSubscriptionList().get(0).getMenu().getId().toString());
    assertEquals(LUNCH, response.getData().getSubscriptionList().get(0).getDeliveryTime());
    assertEquals(NOT_PROCESSED, response.getData().getSubscriptionList().get(0).getDeliveryStatus());
    assertEquals(DELIVERY_LOC, response.getData().getSubscriptionList().get(0).getDeliveryLoc());
    assertEquals(NEXT_WEEK_MONDAY, response.getData().getSubscriptionList().get(0).getTimestamp());
    assertEquals(NEXT_WEEK_MONDAY + 1 * 86400, response.getData().getSubscriptionList().get(1).getTimestamp());
    assertEquals(NEXT_WEEK_MONDAY + 2 * 86400, response.getData().getSubscriptionList().get(2).getTimestamp());
    //    assertEquals(true,); ?????

  }


  // Subscription Item
  // Subscription Item
  // Subscription Item

  @Test
  void editSubscriptionItem_success_UNPAID() {

    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.UNPAID)
        .timestamp(MONDAY)
        .renewal(false)
        .build()).subscribe();

    EditSubscriptionItemRequest request =
        EditSubscriptionItemRequest.builder().subscriptionList(
            Arrays.asList(
                SubscriptionItem.builder()
                    .deliveryTime(DINNER)
                    .deliveryLoc(DELIVERY_LOC_TWO)
                    .menuId(MENU_UUID_TWO.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(MONDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(DINNER)
                    .deliveryLoc(DELIVERY_LOC_TWO)
                    .menuId(MENU_UUID_TWO.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(TUESDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(DINNER)
                    .deliveryLoc(DELIVERY_LOC_TWO)
                    .menuId(MENU_UUID_TWO.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(WEDNESDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(DINNER)
                    .deliveryLoc(DELIVERY_LOC_TWO)
                    .menuId(MENU_UUID_TWO.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(THURSDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(DINNER)
                    .deliveryLoc(DELIVERY_LOC_TWO)
                    .menuId(MENU_UUID_TWO.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(FRIDAY)
                    .build())
        ).build();

    ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>>() {
        };

    BaseCommandResponse<SubscriptionResponse> response = this.webTestClient.put()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString() + "/item")
        .body(Mono.just(request), EditSubscriptionItemRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();


    assert response != null;
    System.out.println(response.getData());
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().getSubscriptionList().size());
    assertEquals(DINNER, response.getData().getSubscriptionList().get(4).getDeliveryTime());
    assertEquals(DELIVERY_LOC_TWO, response.getData().getSubscriptionList().get(4).getDeliveryLoc());
    assertEquals(MENU_UUID_TWO.toString(), response.getData().getSubscriptionList().get(4).getMenu().getId().toString());
  }

  @Test
  void editSubscriptionItem_success_PAID() {

    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.PAID)
        .timestamp(MONDAY)
        .renewal(false)
        .build()).subscribe();

    List<Necessity> necessityList = new ArrayList<>();
    Necessity necessity_monday = Necessity.builder()
        .id(MONDAY_NECESSITY_UUID)
        .timestamp(MONDAY)
        .ingredientList(Arrays.asList(
            Ingredient.builder()
                .stockId(STOCK_UUID_ONE.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_ONE)
                .build(),
            Ingredient.builder()
                .stockId(STOCK_UUID_TWO.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_TWO)
                .build()
        ))
        .build();
    Necessity necessity_tuesday = Necessity.builder()
        .id(uuidHelper.randomUUID())
        .timestamp(TUESDAY)
        .ingredientList(Arrays.asList(
            Ingredient.builder()
                .stockId(STOCK_UUID_ONE.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_ONE)
                .build(),
            Ingredient.builder()
                .stockId(STOCK_UUID_TWO.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_TWO)
                .build()
        ))
        .build();
    Necessity necessity_wednesday = Necessity.builder()
        .id(uuidHelper.randomUUID())
        .timestamp(WEDNESDAY)
        .ingredientList(Arrays.asList(
            Ingredient.builder()
                .stockId(STOCK_UUID_ONE.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_ONE)
                .build(),
            Ingredient.builder()
                .stockId(STOCK_UUID_TWO.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_TWO)
                .build()
        ))
        .build();
    Necessity necessity_thursday = Necessity.builder()
        .id(uuidHelper.randomUUID())
        .timestamp(THURSDAY)
        .ingredientList(Arrays.asList(
            Ingredient.builder()
                .stockId(STOCK_UUID_ONE.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_ONE)
                .build(),
            Ingredient.builder()
                .stockId(STOCK_UUID_TWO.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_TWO)
                .build()
        ))
        .build();
    Necessity necessity_friday = Necessity.builder()
        .id(uuidHelper.randomUUID())
        .timestamp(FRIDAY)
        .ingredientList(Arrays.asList(
            Ingredient.builder()
                .stockId(STOCK_UUID_ONE.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_ONE)
                .build(),
            Ingredient.builder()
                .stockId(STOCK_UUID_TWO.toString())
                .quantity(MENU_INGREDIENT_QUANTITY_TWO)
                .build()
        ))
        .build();
    necessityList.add(necessity_monday);
    necessityList.add(necessity_tuesday);
    necessityList.add(necessity_wednesday);
    necessityList.add(necessity_thursday);
    necessityList.add(necessity_friday);


    necessityRepository.saveAll(necessityList).subscribe();

    EditSubscriptionItemRequest request =
        EditSubscriptionItemRequest.builder().subscriptionList(
            Arrays.asList(
                SubscriptionItem.builder()
                    .deliveryTime(DINNER)
                    .deliveryLoc(DELIVERY_LOC_TWO)
                    .menuId(MENU_UUID_TWO.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(MONDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(DINNER)
                    .deliveryLoc(DELIVERY_LOC_TWO)
                    .menuId(MENU_UUID_TWO.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(TUESDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(DINNER)
                    .deliveryLoc(DELIVERY_LOC_TWO)
                    .menuId(MENU_UUID_TWO.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(WEDNESDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(DINNER)
                    .deliveryLoc(DELIVERY_LOC_TWO)
                    .menuId(MENU_UUID_TWO.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(THURSDAY)
                    .build(),
                SubscriptionItem.builder()
                    .deliveryTime(DINNER)
                    .deliveryLoc(DELIVERY_LOC_TWO)
                    .menuId(MENU_UUID_TWO.toString())
                    .deliveryStatus(NOT_PROCESSED)
                    .timestamp(FRIDAY)
                    .build())
        ).build();

    ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<SubscriptionResponse>>() {
        };

    BaseCommandResponse<SubscriptionResponse> response = this.webTestClient.put()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString() + "/item")
        .body(Mono.just(request), EditSubscriptionItemRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();


    assert response != null;
    System.out.println(response.getData());
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(5, response.getData().getSubscriptionList().size());
    assertEquals(DINNER, response.getData().getSubscriptionList().get(4).getDeliveryTime());
    assertEquals(DELIVERY_LOC_TWO, response.getData().getSubscriptionList().get(4).getDeliveryLoc());
    assertEquals(MENU_UUID_TWO.toString(), response.getData().getSubscriptionList().get(4).getMenu().getId().toString());

    StepVerifier.create(necessityRepository.findFirstByTimestamp(MONDAY))
        .expectNext(Necessity.builder()
            .id(MONDAY_NECESSITY_UUID)
            .timestamp(MONDAY)
            .ingredientList(Arrays.asList(
                Ingredient.builder().stockId(STOCK_UUID_ONE.toString()).quantity(MENU_INGREDIENT_QUANTITY_ONE).build(),
                Ingredient.builder().stockId(STOCK_UUID_TWO.toString()).quantity(MENU_INGREDIENT_QUANTITY_TWO).build()))
            .build())
        .verifyComplete();
  }


  @Test
  void editSubscriptionDeliveryStatusUser_success_Delivered() {

    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.PAID)
        .timestamp(MONDAY)
        .renewal(false)
        .build()).subscribe();

    EditSubscriptionDeliveryStatusRequest request =
        EditSubscriptionDeliveryStatusRequest.builder().deliveryStatus(SubscriptionItem.DeliveryStatus.DELIVERED).timestamp(MONDAY).build();

    ParameterizedTypeReference<BaseCommandResponse<ActiveOrderResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<ActiveOrderResponse>>() {
        };

    BaseCommandResponse<ActiveOrderResponse> response = this.webTestClient.put()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString() + "/deliveryStatus")
        .body(Mono.just(request), EditSubscriptionDeliveryStatusRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MONDAY, response.getData().getTimestamp());
    assertEquals(SubscriptionItem.DeliveryStatus.DELIVERED, response.getData().getDeliveryStatus());
  }

  @Test
  void editSubscriptionDeliveryStatusAdmin_success_Processed() {

    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.PAID)
        .timestamp(MONDAY)
        .renewal(false)
        .build()).subscribe();

    EditSubscriptionDeliveryStatusRequest request =
        EditSubscriptionDeliveryStatusRequest.builder().deliveryStatus(SubscriptionItem.DeliveryStatus.PROCESSED).timestamp(MONDAY).build();

    ParameterizedTypeReference<BaseCommandResponse<ActiveOrderResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<ActiveOrderResponse>>() {
        };

    BaseCommandResponse<ActiveOrderResponse> response = this.webTestClient.put()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString() + "/deliveryStatus")
        .body(Mono.just(request), EditSubscriptionDeliveryStatusRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MONDAY, response.getData().getTimestamp());
    assertEquals(SubscriptionItem.DeliveryStatus.PROCESSED, response.getData().getDeliveryStatus());
  }

  @Test
  void editSubscriptionDeliveryStatusAdmin_success_onDelivery() {

    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.PAID)
        .timestamp(MONDAY)
        .renewal(false)
        .build()).subscribe();

    EditSubscriptionDeliveryStatusRequest request =
        EditSubscriptionDeliveryStatusRequest.builder()
            .deliveryStatus(SubscriptionItem.DeliveryStatus.ON_DELIVERY)
            .timestamp(MONDAY)
            .build();

    ParameterizedTypeReference<BaseCommandResponse<ActiveOrderResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<ActiveOrderResponse>>() {
        };

    BaseCommandResponse<ActiveOrderResponse> response = this.webTestClient.put()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString() + "/deliveryStatus")
        .body(Mono.just(request), EditSubscriptionDeliveryStatusRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(MONDAY, response.getData().getTimestamp());
    assertEquals(SubscriptionItem.DeliveryStatus.ON_DELIVERY, response.getData().getDeliveryStatus());
  }

  @Test
  void editSubscriptionDeliveryStatusProcessed_failed_notAuthorized() {

    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.PAID)
        .timestamp(MONDAY)
        .renewal(false)
        .build()).subscribe();

    EditSubscriptionDeliveryStatusRequest request =
        EditSubscriptionDeliveryStatusRequest.builder().deliveryStatus(SubscriptionItem.DeliveryStatus.PROCESSED).timestamp(MONDAY).build();

    ParameterizedTypeReference<BaseCommandResponse<ActiveOrderResponse>> type =
        new ParameterizedTypeReference<BaseCommandResponse<ActiveOrderResponse>>() {
        };

    BaseCommandResponse<ActiveOrderResponse> response = this.webTestClient.put()
        .uri("/api/subscriptions/" + SUBSCRIPTION_UUID.toString() + "/deliveryStatus")
        .body(Mono.just(request), EditSubscriptionDeliveryStatusRequest.class)
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getCode());
    assertEquals(HttpStatus.UNAUTHORIZED.getReasonPhrase(), response.getStatus());
  }


  @Test
  void getAllActiveOrderAdmin_success() {

    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(SUBSCRIPTION_UUID)
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.PAID)
        .timestamp(MONDAY)
        .renewal(false)
        .build()).subscribe();

    subscriptionRepository.save(Subscription.builder()
        .subscriptionList(Arrays.asList(
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(MONDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(TUESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(WEDNESDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(THURSDAY)
                .build(),
            SubscriptionItem.builder()
                .deliveryTime(LUNCH)
                .deliveryLoc(DELIVERY_LOC)
                .menuId(MENU_UUID.toString())
                .deliveryStatus(NOT_PROCESSED)
                .timestamp(FRIDAY)
                .build()
        ))
        .id(uuidHelper.randomUUID())
        .userId(USER_UUID.toString())
        .paymentStatus(Subscription.PaymentStatus.PAID)
        .timestamp(MONDAY)
        .renewal(false)
        .build()).subscribe();

    EditSubscriptionDeliveryStatusRequest request =
        EditSubscriptionDeliveryStatusRequest.builder().deliveryStatus(SubscriptionItem.DeliveryStatus.PROCESSED).build();

    ParameterizedTypeReference<BasePaginatedCommandResponse<ActiveOrderResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<ActiveOrderResponse>>() {
        };

    BasePaginatedCommandResponse<ActiveOrderResponse> response = this.webTestClient.get()
        .uri("/api/admin/subscriptions/" + TUESDAY + "/active")
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + adminAccessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    assertEquals(TUESDAY, response.getData().get(0).getTimestamp());
    assertEquals(TUESDAY, response.getData().get(1).getTimestamp());
  }

}
