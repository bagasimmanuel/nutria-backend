package com.blibli.nutria.nutria.controller;

import com.blibli.nutria.nutria.model.command.request.user.LoginUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.banner.BannerResponse;
import com.blibli.nutria.nutria.model.command.response.user.LoginUserResponse;
import com.blibli.nutria.nutria.model.entity.Banner;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.BannerRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BannerControllerTests {


  public static final String EMAIL = "bagasimmanuel00@gmail.com";
  public static final String PASSWORD = "123456";
  public static final String FULL_NAME = "Bagas Immanuel";
  public static final UUID USER_UUID = UUID.randomUUID();
  public static final UUID BANNER_ID_ONE = UUID.randomUUID();
  public static final UUID BANNER_ID_TWO = UUID.randomUUID();
  public static final UUID BANNER_ID_THREE = UUID.randomUUID();
  public static final String BEARER_PREFIX = "Bearer ";
  public static final String BANNER_URL_ONE = "/files/banner/test1";
  public static final String BANNER_URL_TWO = "/files/banner/test2";
  public static final String BANNER_URL_THREE = "/files/banner/test3";

  private UUID accessToken;

  @Autowired
  private WebTestClient webTestClient;
  @Autowired
  BannerRepository bannerRepository;
  @Autowired
  UserRepository userRepository;
  @Autowired
  PasswordEncoder passwordEncoder;
  @Autowired
  @Qualifier("springRedisTest")
  private ReactiveRedisTemplate<String, String> reactiveRedisTemplate;



  @BeforeAll
  public void setup() {
    User user = User
        .builder()
        .id(USER_UUID)
        .email(EMAIL)
        .password(passwordEncoder.encode(PASSWORD))
        .roles(Arrays.asList("USER"))
        .fullName(FULL_NAME)
        .verified(true)
        .build();

    userRepository.save(user).subscribe();

    LoginUserRequest loginRequest = LoginUserRequest.builder()
        .email(EMAIL)
        .password(PASSWORD)
        .build();

    ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>> loginType =
        new ParameterizedTypeReference<BaseCommandResponse<LoginUserResponse>>() {
        };

    BaseCommandResponse<LoginUserResponse> loginResponse = this.webTestClient.post()
        .uri("/api/users/login")
        .body(Mono.just(loginRequest), LoginUserRequest.class)
        .exchange()
        .expectBody(loginType)
        .returnResult().getResponseBody();

    accessToken = loginResponse.getData().getAccessToken();

    bannerRepository.deleteAll().subscribe();
  }

  @AfterEach
  void teardown(){
    bannerRepository.deleteAll().subscribe();
  }

  @AfterAll
  public void cleanUp(){
    userRepository.deleteAll().subscribe();
    reactiveRedisTemplate.delete(reactiveRedisTemplate.keys("*")).subscribe();
  }


  @Test
  void findAllBanners_success(){
    Banner banner_one = Banner.builder()
        .id(BANNER_ID_ONE)
        .imageUrl(BANNER_URL_ONE)
        .softDelete(false)
        .build();
    Banner banner_two = Banner.builder()
        .id(BANNER_ID_TWO)
        .imageUrl(BANNER_URL_TWO)
        .softDelete(false)
        .build();
    Banner banner_three = Banner.builder()
        .id(BANNER_ID_THREE)
        .imageUrl(BANNER_URL_TWO)
        .softDelete(true)
        .build();

    bannerRepository.save(banner_one).subscribe();
    bannerRepository.save(banner_two).subscribe();
    bannerRepository.save(banner_three).subscribe();

    ParameterizedTypeReference<BasePaginatedCommandResponse<BannerResponse>> type =
        new ParameterizedTypeReference<BasePaginatedCommandResponse<BannerResponse>>() {
        };

    BasePaginatedCommandResponse<BannerResponse> response = this.webTestClient.get()
        .uri("/api/banners")
        .header(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + accessToken)
        .exchange()
        .expectBody(type)
        .returnResult().getResponseBody();

    assert response != null;
    assertEquals(HttpStatus.OK.value(), response.getCode());
    assertEquals(HttpStatus.OK.getReasonPhrase(), response.getStatus());
    System.out.println(response.getData());
    assertEquals(2,response.getData().size());
    assertEquals(BANNER_URL_ONE,response.getData().get(0).getImageUrl());
    assertEquals(BANNER_URL_TWO,response.getData().get(1).getImageUrl());

  }

}
