package com.blibli.nutria.nutria.security;

import com.blibli.nutria.nutria.exception.UnauthorizedException;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Component
@AllArgsConstructor
public class SessionAuthenticationManager implements ReactiveAuthenticationManager {


  private ReactiveRedisTemplate<UUID,String> redisTemplate;
  private ReactiveUserDetailsService reactiveUserDetailsService;

  @Override
  public Mono<Authentication> authenticate(Authentication authentication) {
    return Mono.just(authentication)
        .map(Authentication::getPrincipal)
        .flatMap(accessToken -> redisTemplate.opsForValue().get(accessToken))
        .onErrorResume(UnauthorizedException.class, e -> Mono.error(UnauthorizedException::new))
        .flatMap(reactiveUserDetailsService::findByUsername)
        .map(userDetails ->  new UsernamePasswordAuthenticationToken(userDetails.getUsername(),userDetails.getUsername(),
            userDetails.getAuthorities()));
  }

}
