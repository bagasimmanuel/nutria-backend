package com.blibli.nutria.nutria.security;

import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class ReactiveUserDetailsServiceImpl implements ReactiveUserDetailsService {

  private UserRepository userRepository;

  @Override
  public Mono<UserDetails> findByUsername(String s) {

    Mono<User> userMono = userRepository.findFirstByEmail(s);
    return userMono.map(user -> UserDetailsImpl.builder()
        .username(user.getEmail())
        .password(user.getPassword())
        .enabled(user.isVerified())
        .authorities(toSimpleGrantedAuthorities(user.getRoles())).build());
  }

  private List<SimpleGrantedAuthority> toSimpleGrantedAuthorities(List<String> roles) {
    return roles.stream()
        .map(role -> new SimpleGrantedAuthority("ROLE_" + role)
    ).collect(Collectors.toList());
  }
}
