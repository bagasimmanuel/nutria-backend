package com.blibli.nutria.nutria.model.entity;

import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;
import java.util.UUID;

@Data
@Builder
public class Subscription {

  @Id
  private UUID id;
  private String userId;
  private Long timestamp;
  private PaymentStatus paymentStatus;
  private List<SubscriptionItem> subscriptionList;
  private boolean renewal;

  public enum PaymentStatus{
    PAID,UNPAID,CANCELLED;

    public static PaymentStatus valueOfOrNull(String name) {
      if(name.equals(PAID.name()) || name.equals(UNPAID.name())  || name.equals(CANCELLED.name()))
        return PaymentStatus.valueOf(name);
      else
        return null;
    }

  }

}
