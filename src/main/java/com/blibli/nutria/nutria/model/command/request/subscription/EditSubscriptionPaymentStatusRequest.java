package com.blibli.nutria.nutria.model.command.request.subscription;

import com.blibli.nutria.nutria.model.entity.Subscription;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EditSubscriptionPaymentStatusRequest {

  private Subscription.PaymentStatus paymentStatus;

}
