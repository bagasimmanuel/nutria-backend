package com.blibli.nutria.nutria.model.command.request.stock;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateStockRequest {

  private String name;
  private String description;
  private Integer quantity;

}
