package com.blibli.nutria.nutria.model.entity;

import com.blibli.nutria.nutria.model.dto.Ingredient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document
public class Menu {

  @Id
  private UUID id;
  private String name;
  private String description;
  private String imageUrl;
  private String mealplanId;
  private int calorie;
  private List<Ingredient> recipes;
  @Indexed
  private Boolean softDelete;

}
