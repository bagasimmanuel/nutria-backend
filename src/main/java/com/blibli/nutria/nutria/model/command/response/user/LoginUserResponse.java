package com.blibli.nutria.nutria.model.command.response.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginUserResponse {

  private UUID accessToken;
  private UUID refreshToken;
  private List<String> roles;

}
