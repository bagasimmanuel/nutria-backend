package com.blibli.nutria.nutria.model.command.request.menu;

import com.blibli.nutria.nutria.model.dto.Ingredient;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class EditMenuRequest {

  private String name;
  private String description;
  private String mealplanId;
  private Integer calorie;
  private List<Ingredient> recipes;

}
