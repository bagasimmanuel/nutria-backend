package com.blibli.nutria.nutria.model.command.response.user;

import com.blibli.nutria.nutria.model.command.response.mealplan.MealplanResponse;
import com.blibli.nutria.nutria.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import reactor.util.function.Tuple2;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

  private UUID id;
  private String email;
  private String fullName;
  private List<String> location;
  private MealplanResponse mealplan;

  public UserResponse (Tuple2<User,MealplanResponse> tuple2) {
    this.id = tuple2.getT1().getId();
    this.email = tuple2.getT1().getEmail();
    this.fullName = tuple2.getT1().getFullName();
    this.location = tuple2.getT1().getLocation();
    this.mealplan = tuple2.getT2();
  }

}
