package com.blibli.nutria.nutria.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Stock {

  @Id
  private UUID id;
  private String name;
  private String description;
  private String imageUrl;
  private int quantity;
  @Indexed
  private Boolean softDelete;

}
