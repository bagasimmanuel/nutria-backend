package com.blibli.nutria.nutria.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.UUID;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

  @Id
  private UUID id;
  @Indexed(unique = true)
  private String email;
  private String fullName;
  private String password;
  private List<String> roles;
  private boolean verified;
  private int height;
  private int weight;
  private int age;
  private ActivityLevel activityLevel; // sedentary,light,moderate,active,extreme (TDEE)
  private String mealplanId;
  private Gender gender;
  private int calorieRecom;
  private List<String> location;

  public enum ActivityLevel {
    SEDENTARY,LIGHT,ACTIVE,VERY,EXTREME;
  }
  public enum Gender {
    MALE,FEMALE
  }

}
