package com.blibli.nutria.nutria.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Ingredient {

  private String stockId;
  private Integer quantity;

}
