package com.blibli.nutria.nutria.model.command.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class BasePaginatedCommandResponse<T> extends BaseCommandResponse<List<T>> {

  private Pagination pagination;

  public BasePaginatedCommandResponse(
      HttpStatus httpStatus,
      List<T> data,
      Pagination pagination) {
    super(httpStatus, data);
    setPagination(pagination);
  }

  @Data
  @Builder
  @AllArgsConstructor
  @NoArgsConstructor
  public static class Pagination {
    private int currentPage;
    private int itemsPerPage;
    private long totalItems;
  }

}
