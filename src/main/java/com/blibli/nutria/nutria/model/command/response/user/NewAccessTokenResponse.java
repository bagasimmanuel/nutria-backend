package com.blibli.nutria.nutria.model.command.response.user;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class NewAccessTokenResponse {

  private UUID accessToken;
  private UUID refreshToken;

}
