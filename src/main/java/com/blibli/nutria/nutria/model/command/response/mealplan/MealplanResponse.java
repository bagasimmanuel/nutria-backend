package com.blibli.nutria.nutria.model.command.response.mealplan;

import com.blibli.nutria.nutria.model.entity.Mealplan;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MealplanResponse {

  private UUID id;
  private String name;
  private String description;
  private String imageUrl;

  public MealplanResponse(Mealplan mealplan){
    this.id = mealplan.getId();
    this.name = mealplan.getName();
    this.description = mealplan.getDescription();
    this.imageUrl = mealplan.getImageUrl();
  }
}
