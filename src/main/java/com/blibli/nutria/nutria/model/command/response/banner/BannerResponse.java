package com.blibli.nutria.nutria.model.command.response.banner;

import com.blibli.nutria.nutria.model.entity.Banner;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerResponse {

  private String id;
  private String imageUrl;


  public BannerResponse(Banner banner){
    this.id = banner.getId().toString();
    this.imageUrl = banner.getImageUrl();

  }

}
