package com.blibli.nutria.nutria.model.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.UUID;

@Data
@Document
@Builder
public class RefreshToken {

  @Id
  private UUID id;
  private UUID refreshToken;
  private UUID accessToken;
  private String email;

  @Indexed(expireAfterSeconds = 86400)
  private Date ttl;

}
