package com.blibli.nutria.nutria.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Report {

  private UUID id;
  private long timestamp;
  private long subscriptionCount;
  private long revenue;
  private List<ReportItem> items;

  @Data
  @Builder
  public static class ReportItem {
    private String menuId;
    private int purchaseCount;
  }

}
