package com.blibli.nutria.nutria.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Mealplan {

  @Id
  private UUID id;
  private String name;
  private String description;
  private String imageUrl;
  private Boolean softDelete;



}
