package com.blibli.nutria.nutria.model.command.response;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"code","status"})
public class BaseCommandResponse<T> {

  @Getter(AccessLevel.NONE)
  private HttpStatus httpStatus;

  public void setCode(int code){
    this.httpStatus = HttpStatus.valueOf(code);
  }

  public int getCode() { return httpStatus.value(); }

  public String getStatus() { return httpStatus.getReasonPhrase(); }

  private T data;


}
