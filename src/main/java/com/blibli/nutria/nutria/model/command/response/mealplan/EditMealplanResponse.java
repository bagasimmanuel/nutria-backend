package com.blibli.nutria.nutria.model.command.response.mealplan;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EditMealplanResponse {

  private String name;
  private String description;

}
