package com.blibli.nutria.nutria.model.command.response.stock;

import com.blibli.nutria.nutria.model.command.response.IngredientResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CheckStockResponse {

  private long timestamp;
  private List<IngredientResponse> deficitList;

}
