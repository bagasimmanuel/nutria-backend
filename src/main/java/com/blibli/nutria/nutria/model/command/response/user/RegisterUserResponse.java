package com.blibli.nutria.nutria.model.command.response.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterUserResponse {

    private UUID id;
    private String email;
    private String fullName;
    private List<String> roles;
    private Boolean verified;

}
