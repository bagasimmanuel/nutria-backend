package com.blibli.nutria.nutria.model.command.response.stock;

import com.blibli.nutria.nutria.model.entity.Stock;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StockResponse {

  private UUID id;
  private String name;
  private String description;
  private String imageUrl;
  private int quantity;

  public StockResponse(Stock stock){
    this.id = stock.getId();
    this.name = stock.getName();
    this.description = stock.getDescription();
    this.quantity = stock.getQuantity();
    this.imageUrl = stock.getImageUrl();
  }

}
