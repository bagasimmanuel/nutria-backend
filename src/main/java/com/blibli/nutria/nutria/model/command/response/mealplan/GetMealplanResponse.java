package com.blibli.nutria.nutria.model.command.response.mealplan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetMealplanResponse {

  private UUID id;
  private String name;
  private String description;
  private String imageUrl;

}
