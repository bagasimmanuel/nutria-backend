package com.blibli.nutria.nutria.model.command.response;

import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IngredientResponse {

  private StockResponse stock;
  private int quantity;

}
