package com.blibli.nutria.nutria.model.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document
@Data
@Builder
public class Banner {

  @Id
  private UUID id;
  private String imageUrl;
  @Indexed
  private boolean softDelete;

}
