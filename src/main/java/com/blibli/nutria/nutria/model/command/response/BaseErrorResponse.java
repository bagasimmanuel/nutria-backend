package com.blibli.nutria.nutria.model.command.response;
import com.blibli.nutria.nutria.exception.BaseException;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"code", "status"})
public class BaseErrorResponse<T extends BaseException> {

  @Getter(AccessLevel.NONE)
  private HttpStatus httpStatus;

  public int getCode() {
    return httpStatus.value();
  }

  public String getStatus() {
    return httpStatus.getReasonPhrase();
  }

  @Getter(AccessLevel.NONE)
  private T error;

  public String getError() {
    return error.getError();
  }

}
