package com.blibli.nutria.nutria.model.command.response.menu;

import com.blibli.nutria.nutria.model.command.response.IngredientResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.MealplanResponse;
import com.blibli.nutria.nutria.model.entity.Menu;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MenuAdminResponse {

  private UUID id;
  private String name;
  private String description;
  private String imageUrl;
  private MealplanResponse mealplan;
  private int calorie;
  private List<IngredientResponse> recipes;


  public MenuAdminResponse(Menu menu, MealplanResponse mealplan,List<IngredientResponse> ingredientResponses){
    this.id = menu.getId();
    this.name = menu.getName();
    this.description = menu.getDescription();
    this.imageUrl = menu.getImageUrl();
    this.mealplan = mealplan;
    this.calorie = menu.getCalorie();
    this.recipes = ingredientResponses;
  }



}
