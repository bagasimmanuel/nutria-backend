package com.blibli.nutria.nutria.model.command.request.user;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoginUserRequest {

  private String email;
  private String password;

}
