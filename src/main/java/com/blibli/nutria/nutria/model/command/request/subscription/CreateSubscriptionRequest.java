package com.blibli.nutria.nutria.model.command.request.subscription;

import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CreateSubscriptionRequest {

  private Long timestamp;
  private List<SubscriptionItem> subscriptionList;

}
