package com.blibli.nutria.nutria.model.command.response.user;

import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EditProfileUserResponse {

  private UUID id;
  private String email;
  private String fullName;
  private Integer height;
  private Integer weight;
  private Integer age;
  private User.Gender gender;
  private User.ActivityLevel activityLevel;
  private Mealplan mealplan;
  private int calorieRecom;
  private List<String> location;

  public EditProfileUserResponse(User user) {
    EditProfileUserResponse editProfileUserResponse = new EditProfileUserResponse();
    BeanUtils.copyProperties(user,editProfileUserResponse);
    this.weight = user.getWeight();
  }



}
