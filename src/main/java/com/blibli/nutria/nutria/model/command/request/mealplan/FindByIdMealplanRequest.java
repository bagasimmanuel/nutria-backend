package com.blibli.nutria.nutria.model.command.request.mealplan;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FindByIdMealplanRequest {

  private String mealplanId;

}
