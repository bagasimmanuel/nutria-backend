package com.blibli.nutria.nutria.model.entity;

import com.blibli.nutria.nutria.model.dto.Ingredient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Necessity {

  @Id
  private UUID id;
  private long timestamp;
  private List<Ingredient> ingredientList;

}
