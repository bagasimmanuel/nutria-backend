package com.blibli.nutria.nutria.model.command.response.subscription;

import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import com.blibli.nutria.nutria.model.command.response.user.UserResponse;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ActiveOrderResponse {

  private String subscriptionId;
  private long timestamp;
  private UserResponse user;
  private MenuAdminResponse menu;
  private SubscriptionItem.DeliveryStatus deliveryStatus;
  private String deliveryLoc;
  private SubscriptionItem.DeliveryTime deliveryTime;

}
