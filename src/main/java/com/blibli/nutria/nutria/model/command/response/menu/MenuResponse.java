package com.blibli.nutria.nutria.model.command.response.menu;

import com.blibli.nutria.nutria.model.command.response.mealplan.MealplanResponse;
import com.blibli.nutria.nutria.model.entity.Menu;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MenuResponse {

  public MenuResponse (Menu menu,MealplanResponse mealplan){
    this.id = menu.getId();
    this.name = menu.getName();
    this.description = menu.getDescription();
    this.imageUrl = menu.getImageUrl();
    this.calorie = menu.getCalorie();
    this.mealplan = mealplan;
  }

  private UUID id;
  private String name;
  private String description;
  private String imageUrl;
  private MealplanResponse mealplan;
  private int calorie;



}
