package com.blibli.nutria.nutria.model.command.response.user;

import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserProfileResponse {

  private UUID id;
  private String email;
  private String fullName;
  private List<String> roles;
  private Integer height;
  private Integer weight;
  private Integer age;
  private User.ActivityLevel activityLevel;
  private Mealplan mealplan;
  private User.Gender gender;
  private Integer calorieRecom;
  private List<String> location;

}
