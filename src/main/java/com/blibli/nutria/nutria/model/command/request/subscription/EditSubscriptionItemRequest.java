package com.blibli.nutria.nutria.model.command.request.subscription;

import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EditSubscriptionItemRequest {

  private List<SubscriptionItem> subscriptionList;

}
