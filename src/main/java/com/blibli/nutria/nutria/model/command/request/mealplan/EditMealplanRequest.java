package com.blibli.nutria.nutria.model.command.request.mealplan;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EditMealplanRequest {

  private String name;
  private String description;

}
