package com.blibli.nutria.nutria.model.command.response.report;

import com.blibli.nutria.nutria.model.command.response.menu.MenuResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReportResponse {

  @Id
  private UUID id;
  private long timestamp;
  private long subscriptionCount;
  private long revenue;
  private List<ReportItemResponse> items;

  @Data
  @Builder
  public static class ReportItemResponse {
    private MenuResponse menu;
    private int purchaseCount;
  }

}
