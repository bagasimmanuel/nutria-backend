package com.blibli.nutria.nutria.model.command.request.user;

import com.blibli.nutria.nutria.model.entity.User;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class EditProfileUserRequest {

  private String fullName;
  private Integer height;
  private Integer weight;
  private Integer age;
  private User.ActivityLevel activityLevel; // sedentary,light,moderate,active,extreme (TDEE)
  private String mealplanId;
  private User.Gender gender;
  private List<String> location;

}
