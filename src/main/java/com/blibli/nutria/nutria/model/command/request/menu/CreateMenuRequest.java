package com.blibli.nutria.nutria.model.command.request.menu;

import com.blibli.nutria.nutria.model.command.request.BaseCommandRequest;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateMenuRequest extends BaseCommandRequest {

  private String name;
  private String description;
  private String mealplanId;
  private Integer calorie;
  private List<Ingredient> recipes;

}
