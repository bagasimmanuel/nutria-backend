package com.blibli.nutria.nutria.model.command.request.subscription;

import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EditSubscriptionDeliveryStatusRequest {

  private long timestamp;
  private SubscriptionItem.DeliveryStatus deliveryStatus;

}
