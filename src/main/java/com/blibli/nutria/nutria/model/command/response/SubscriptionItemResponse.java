package com.blibli.nutria.nutria.model.command.response;

import com.blibli.nutria.nutria.model.command.response.menu.MenuResponse;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SubscriptionItemResponse {

  private MenuResponse menu;
  private long timestamp;
  private SubscriptionItem.DeliveryStatus deliveryStatus;
  private String deliveryLoc;
  private SubscriptionItem.DeliveryTime deliveryTime;

}
