package com.blibli.nutria.nutria.model.command.response.subscription;

import com.blibli.nutria.nutria.model.command.response.SubscriptionItemResponse;
import com.blibli.nutria.nutria.model.command.response.user.UserResponse;
import com.blibli.nutria.nutria.model.entity.Subscription;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import reactor.util.function.Tuple3;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class SubscriptionResponse {

  private String id;
  private long timestamp;
  private Subscription.PaymentStatus paymentStatus;
  private List<SubscriptionItemResponse> subscriptionList;
  private UserResponse user;
  private boolean renewal;

  public SubscriptionResponse(Tuple3<Subscription, List<SubscriptionItemResponse>,UserResponse> tuple3){
    this.id = tuple3.getT1().getId().toString();
    this.timestamp = tuple3.getT1().getTimestamp();
    this.user = tuple3.getT3();
    this.renewal = tuple3.getT1().isRenewal();
    this.paymentStatus = tuple3.getT1().getPaymentStatus();
    this.subscriptionList = tuple3.getT2();
  }



}
