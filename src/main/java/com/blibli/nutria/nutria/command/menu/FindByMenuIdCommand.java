package com.blibli.nutria.nutria.command.menu;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.menu.FindByIdMenuRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuResponse;

public interface FindByMenuIdCommand extends Command<FindByIdMenuRequest, BaseCommandResponse<MenuResponse>> {
}
