package com.blibli.nutria.nutria.command.mealplan;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.mealplan.FindByIdMealplanRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.FindByIdMealplanResponse;

public interface FindByIdMealplanCommand extends Command<FindByIdMealplanRequest, BaseCommandResponse<FindByIdMealplanResponse>> {
}
