package com.blibli.nutria.nutria.command.impl.menu;

import com.blibli.nutria.nutria.command.menu.UploadImageMenuCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.StorageHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import com.blibli.nutria.nutria.model.entity.Menu;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
@AllArgsConstructor
public class UploadImageMenuCommandImpl implements UploadImageMenuCommand {

  private MenuRepository menuRepository;
  private MealplanRepository mealplanRepository;
  private StockRepository stockRepository;
  private StorageHelper storageHelper;
  private UUIDHelper uuidHelper;
  private ConstructorHelper constructorHelper;

  @Override
  public Mono<BaseCommandResponse<MenuAdminResponse>> execute(Mono<Tuple2<String, FilePart>> request) {
    return request.flatMap(this::handle)
        .map(menuAdminResponse -> new BaseCommandResponse<>(HttpStatus.OK, menuAdminResponse));
  }

  private Mono<MenuAdminResponse> handle(Tuple2<String, FilePart> request) {
    return menuRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(request.getT1()))
        .switchIfEmpty(Mono.error(new NotFoundException("MENU")))
        .flatMap(menu -> uploadImage(menu, request.getT2()))
        .flatMap(constructorHelper::constructMenuAdminResponse);
  }

  private Mono<Menu> uploadImage(Menu menu, FilePart filePart) {
    return storageHelper.storeFile(filePart, StorageHelper.STORAGE_TYPE.MENU_IMAGE, menu.getId())
        .map(imageUrl -> {
          menu.setImageUrl(imageUrl);
          return menu;
        }).flatMap(menuRepository::save);
  }


}
