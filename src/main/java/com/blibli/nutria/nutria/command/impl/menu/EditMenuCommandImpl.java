package com.blibli.nutria.nutria.command.impl.menu;

import com.blibli.nutria.nutria.command.menu.EditMenuCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.menu.EditMenuRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.IngredientResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.MealplanResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.model.entity.Menu;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuple3;
import reactor.util.function.Tuples;

import java.util.List;

@Component
@AllArgsConstructor
public class EditMenuCommandImpl implements EditMenuCommand {

  private UUIDHelper uuidHelper;
  private MealplanRepository mealplanRepository;
  private StockRepository stockRepository;
  private MenuRepository menuRepository;
  private ConstructorHelper constructorHelper;

  @Override
  public Mono<BaseCommandResponse<MenuAdminResponse>> execute(Mono<Tuple2<EditMenuRequest, String>> request) {
    return request.flatMap(this::handle)
        .map(x -> new BaseCommandResponse<>(HttpStatus.OK, x));
  }


  private Mono<MenuAdminResponse> handle(Tuple2<EditMenuRequest, String> tuple2) {

    return menuRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(tuple2.getT2()))
        .switchIfEmpty(Mono.error(new NotFoundException("MENU")))
        .flatMap(menu -> updateMenu(tuple2.getT1(), menu))
        .flatMap(constructorHelper::constructMenuAdminResponse);

  }

  private Mono<Menu> updateMenu(EditMenuRequest editMenuRequest, Menu menu) {
    if (editMenuRequest.getName() != null) {
      menu.setName(editMenuRequest.getName());
    }
    if (editMenuRequest.getDescription() != null) {
      menu.setDescription(editMenuRequest.getDescription());
    }
    if (editMenuRequest.getMealplanId() != null) {
      menu.setMealplanId(editMenuRequest.getMealplanId());
    }
    if (editMenuRequest.getCalorie() != null) {
      menu.setCalorie(editMenuRequest.getCalorie());
    }
    if (editMenuRequest.getRecipes() != null) {
      menu.setRecipes(editMenuRequest.getRecipes());
    }
    return menuRepository.save(menu);
  }

  private MenuAdminResponse toMenuAdminResponse(Tuple3<Menu, Mealplan, List<IngredientResponse>> tuple3) {
    MealplanResponse mealplanResponse = new MealplanResponse(tuple3.getT2());
    return MenuAdminResponse.builder()
        .id(tuple3.getT1().getId())
        .name(tuple3.getT1().getName())
        .description(tuple3.getT1().getDescription())
        .imageUrl(tuple3.getT1().getImageUrl())
        .calorie(tuple3.getT1().getCalorie())
        .mealplan(mealplanResponse)
        .recipes(tuple3.getT3())
        .build();
  }


  private Mono<IngredientResponse> constructNValidateIngredientResponse(Ingredient ingredient) {
    return stockRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(ingredient.getStockId()))
        .map(StockResponse::new)
        .map(stockResponse -> IngredientResponse.builder()
            .stock(stockResponse)
            .quantity(ingredient.getQuantity())
            .build());
  }


}
