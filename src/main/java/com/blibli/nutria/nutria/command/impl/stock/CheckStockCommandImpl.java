package com.blibli.nutria.nutria.command.impl.stock;

import com.blibli.nutria.nutria.command.stock.CheckStockCommand;
import com.blibli.nutria.nutria.exception.BadRequestException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.IngredientResponse;
import com.blibli.nutria.nutria.model.command.response.stock.CheckStockResponse;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import com.blibli.nutria.nutria.model.entity.Necessity;
import com.blibli.nutria.nutria.repository.NecessityRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class CheckStockCommandImpl implements CheckStockCommand {

  private NecessityRepository necessityRepository;
  private StockRepository stockRepository;
  private UUIDHelper uuidHelper;
  private ConstructorHelper constructorHelper;

  @Override
  public Mono<BaseCommandResponse<CheckStockResponse>> execute(Mono<Long> request) {
    return request.flatMap(necessityRepository::findFirstByTimestamp)
        .switchIfEmpty(Mono.error(new BadRequestException("Empty Necessity")))
        .flatMap(this::calculateNecessity)
        .flatMap(this::constructResponse)
        .map(response -> new BaseCommandResponse<>(HttpStatus.OK, response));
  }

  private Mono<Necessity> calculateNecessity(Necessity necessity) {
    return Flux.fromIterable(necessity.getIngredientList()).flatMap(ingredient ->
            stockRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(ingredient.getStockId()))
            .map(stock -> {
              if(stock.getQuantity() - ingredient.getQuantity() < 0){
                ingredient.setQuantity(ingredient.getQuantity() - stock.getQuantity());
                return ingredient;
              }else{
                return Ingredient.builder().stockId("none").build();
              }
            })).collectList()
        .map(this::removeNonDeficitStock)
        .map(calculatedNecessity -> Necessity.builder().timestamp(necessity.getTimestamp()).ingredientList(calculatedNecessity).build());
  }

  private List<Ingredient> removeNonDeficitStock(List<Ingredient> ingredientList){
    return ingredientList.stream().filter(ingredient -> !ingredient.getStockId().equals("none")).collect(Collectors.toList());
  }

  private Mono<CheckStockResponse> constructResponse(Necessity necessity) {

    Mono<List<IngredientResponse>> listMono = Flux.fromIterable(necessity.getIngredientList())
        .flatMap(constructorHelper::constructIngredientResponse)
        .collectList();

    return listMono.map(ingredientResponses -> CheckStockResponse.builder()
        .deficitList(ingredientResponses)
        .timestamp(necessity.getTimestamp()).build());

  }

}
