package com.blibli.nutria.nutria.command.impl.banner;

import com.blibli.nutria.nutria.command.banner.FindAllBannerCommand;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.banner.BannerResponse;
import com.blibli.nutria.nutria.repository.BannerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class FindAllBannerCommandImpl implements FindAllBannerCommand {

  private final BannerRepository bannerRepository;

  @Value("${itemsPerPage:#{10}}")
  private int ITEMS_PER_PAGE;

  @Override
  public Mono<BasePaginatedCommandResponse<BannerResponse>> execute(Mono<Integer> request) {
    return request.flatMap(this::handler);
  }

  private Mono<BasePaginatedCommandResponse<BannerResponse>> handler(int page) {
    Pageable pageable = PageRequest.of(page, ITEMS_PER_PAGE);
    return bannerRepository.findAllByIdIsNotNullAndSoftDeleteFalse(pageable)
        .map(BannerResponse::new).collectList()
        .zipWith(bannerRepository.countAllByIdIsNotNullAndSoftDeleteFalse())
        .map(tupleResponse -> new BasePaginatedCommandResponse<>(HttpStatus.OK,
            tupleResponse.getT1(),
            BasePaginatedCommandResponse.Pagination.builder().itemsPerPage(ITEMS_PER_PAGE)
                .currentPage(page)
                .totalItems(tupleResponse.getT2()).build()));
  }
}
