package com.blibli.nutria.nutria.command.impl.mealplan;

import com.blibli.nutria.nutria.command.mealplan.EditMealplanCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.mealplan.EditMealplanRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.EditMealplanResponse;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
@AllArgsConstructor
public class EditMealplanCommandImpl implements EditMealplanCommand {

  private MealplanRepository mealplanRepository;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<EditMealplanResponse>> execute(Mono<Tuple2<EditMealplanRequest, String>> request) {

    return request.flatMap(tuple -> {
          Mono<Mealplan> mealplanMono = mealplanRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(tuple.getT2()))
              .switchIfEmpty(Mono.error(new NotFoundException("MEALPLAN")));
          return mealplanMono.map(mealplan -> updateMealplanData(tuple.getT1(), mealplan));
        }).flatMap(mealplanRepository::save)
        .map(updatedMealplan -> new BaseCommandResponse<>(HttpStatus.OK, toEditMealplanResponse(updatedMealplan)));
  }


  private Mealplan updateMealplanData(EditMealplanRequest editMealplanRequest, Mealplan mealplan) {
    mealplan.setName(editMealplanRequest.getName());
    mealplan.setDescription(editMealplanRequest.getDescription());
    return mealplan;
  }

  private EditMealplanResponse toEditMealplanResponse(Mealplan mealplan) {
    return EditMealplanResponse.builder()
        .name(mealplan.getName())
        .description(mealplan.getDescription())
        .build();
  }
}
