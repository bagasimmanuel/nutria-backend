package com.blibli.nutria.nutria.command.impl.subscription;

import com.blibli.nutria.nutria.command.subscription.RenewSubscriptionCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.DateHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class RenewSubscriptionCommandImpl implements RenewSubscriptionCommand {

  private UUIDHelper uuidHelper;
  private ConstructorHelper constructorHelper;
  private DateHelper dateHelper;
  private SubscriptionRepository subscriptionRepository;
  private UserRepository userRepository;
  private MenuRepository menuRepository;
  private final static long WEEK_IN_SECONDS = 604800;

  @Override
  public Mono<BaseCommandResponse<SubscriptionResponse>> execute(Mono<String> request) {
    return request.flatMap(this::handler);
  }


  private Mono<BaseCommandResponse<SubscriptionResponse>> handler(String subscriptionId) {
    return subscriptionRepository.findFirstById(uuidHelper.fromString(subscriptionId))
        .switchIfEmpty(Mono.error(new NotFoundException("SUBSCRIPTION")))
        .flatMap(subscription -> {
          subscription.setRenewal(true);
          return subscriptionRepository.save(subscription);
        }).flatMap(updatedSubscription -> {
          Subscription newSubscription = updateSubscription(updatedSubscription);
          return subscriptionRepository.save(newSubscription);
        })
        .flatMap(constructorHelper::constructSubscriptionResponse)
        .map(subscriptionResponse -> new BaseCommandResponse<>(HttpStatus.OK, subscriptionResponse));


  }

  private Subscription updateSubscription(Subscription updatedSubscription) {

    int day = 0;
    updatedSubscription.setId(uuidHelper.randomUUID());
    updatedSubscription.setPaymentStatus(Subscription.PaymentStatus.UNPAID);
    updatedSubscription.setTimestamp(updatedSubscription.getTimestamp() + WEEK_IN_SECONDS);
    updatedSubscription.setRenewal(false);
    for (SubscriptionItem subscriptionItem : updatedSubscription.getSubscriptionList()) {
      subscriptionItem.setDeliveryStatus(SubscriptionItem.DeliveryStatus.NOT_PROCESSED);
      subscriptionItem.setTimestamp(dateHelper.getDayEpoch(updatedSubscription.getTimestamp(), day++));
    }
    return updatedSubscription;
  }

}
