package com.blibli.nutria.nutria.command.impl.subscription;

import com.blibli.nutria.nutria.command.subscription.CreateSubscriptionCommand;
import com.blibli.nutria.nutria.exception.InvalidDateException;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.DateHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.subscription.CreateSubscriptionRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.repository.NecessityRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class CreateSubscriptionCommandImpl implements CreateSubscriptionCommand {

  public static final int WEEK_IN_SECONDS = 604800;
  public static final int DAY_IN_SECONDS = 86400;
  private UUIDHelper uuidHelper;
  private DateHelper dateHelper;
  private ConstructorHelper constructorHelper;
  private SubscriptionRepository subscriptionRepository;
  private UserRepository userRepository;
  private MenuRepository menuRepository;

  @Override
  public Mono<BaseCommandResponse<SubscriptionResponse>> execute(Mono<CreateSubscriptionRequest> request) {
    return request.flatMap(this::handler);
  }

  private Mono<BaseCommandResponse<SubscriptionResponse>> handler(CreateSubscriptionRequest request) {
    Mono<Subscription> subscriptionMono = toSubscriptionMono(request);
    return subscriptionMono.flatMap(this::validateSubscriptionDate)
        .flatMap(subscriptionRepository::save)
        .flatMap(constructorHelper::constructSubscriptionResponse)
        .map(subscriptionResponse -> new BaseCommandResponse<>(HttpStatus.OK, subscriptionResponse));

  }

  private Mono<Subscription> toSubscriptionMono(CreateSubscriptionRequest subscriptionRequest) {

    List<SubscriptionItem> subscriptionItems = subscriptionRequest.getSubscriptionList();
    for (SubscriptionItem subscriptionItem : subscriptionItems) {
      subscriptionItem.setDeliveryStatus(SubscriptionItem.DeliveryStatus.NOT_PROCESSED);
    }
    Mono<List<SubscriptionItem>> listMono =
        Flux.fromIterable(subscriptionItems).flatMap(this::validateSubscriptionItems).collectList();

    return listMono.flatMap(subscriptionItem ->
        ReactiveSecurityContextHolder.getContext()
            .map(SecurityContext::getAuthentication)
            .map(x -> x.getPrincipal().toString())
            .flatMap(userRepository::findFirstByEmail)
            .map(user -> Subscription.builder()
                .id(uuidHelper.randomUUID())
                .userId(user.getId().toString())
                .timestamp(subscriptionRequest.getTimestamp())
                .subscriptionList(subscriptionItem)
                .paymentStatus(Subscription.PaymentStatus.UNPAID)
                .renewal(false)
                .build())
    );
  }

  private Mono<SubscriptionItem> validateSubscriptionItems(SubscriptionItem subscriptionItem) {
    return menuRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(subscriptionItem.getMenuId()))
        .switchIfEmpty(Mono.error(new NotFoundException("MENU")))
        .map(menu -> subscriptionItem);

  }

  private Mono<Subscription> validateSubscriptionDate(Subscription subscription) {

    long nextMondayLocalDate = dateHelper.getNextDayOfTheWeek(DayOfWeek.MONDAY);// GMT +7
    long subscriptionTimestamp = subscription.getTimestamp();
    long seconds = subscriptionTimestamp - nextMondayLocalDate;

    if (checkIfMondays(seconds)) { // Check apakah hari senin
      List<Long> daysTimestamp = new ArrayList<>(); // ngecheck apakah hari tersebut sudah ada? supaya nggak 1 hari 2 subscriptionItem
      boolean flag = true;
      for (int i = 0; i < subscription.getSubscriptionList().size(); i++) {
        long toBeValidatedTimestamp = subscription.getSubscriptionList().get(i).getTimestamp();
        if ((toBeValidatedTimestamp - subscriptionTimestamp) % DAY_IN_SECONDS == 0 && thisWeekRange(toBeValidatedTimestamp,
            subscriptionTimestamp)
            && !daysTimestamp.contains(toBeValidatedTimestamp)) {
          daysTimestamp.add(toBeValidatedTimestamp);
        } else {
          flag = false;
          break;
        }
      }
      if (flag) {
        return Mono.just(subscription);
      }
      return Mono.error(new InvalidDateException());
    } else {
      return Mono.error(new InvalidDateException());
    }

  }

  private boolean thisWeekRange(long subscriptionItemTimestamp,
      long subscriptionTimestamp) { // Check apakah subcsruptionItem masih dalam range 1 minggu itu.
    long monday = subscriptionTimestamp;
    long friday = subscriptionTimestamp + 4 * DAY_IN_SECONDS;
    if (subscriptionItemTimestamp >= monday && subscriptionItemTimestamp <= friday) {
      return true;
    }
    return false;
  }

  private boolean checkIfMondays(long seconds) {
    return seconds == 0 || (seconds / WEEK_IN_SECONDS < 5 && seconds / WEEK_IN_SECONDS > -2 // Maksimal 4 minggu dari minggu ini
        && seconds % WEEK_IN_SECONDS == 0);
  }

}