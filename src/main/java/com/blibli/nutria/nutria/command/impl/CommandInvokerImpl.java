package com.blibli.nutria.nutria.command.impl;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.command.CommandInvoker;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class CommandInvokerImpl implements CommandInvoker, ApplicationContextAware {

  private ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

  @Override
  public <R , S extends BaseCommandResponse<?>> Mono<S> invokeCommand(Class<? extends Command<R, S>> commandClass,
      Mono<R> request) {

    Command<R,S> bean = applicationContext.getBean(commandClass);
    return bean.execute(request);
  }

}
