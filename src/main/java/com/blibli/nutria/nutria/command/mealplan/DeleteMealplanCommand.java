package com.blibli.nutria.nutria.command.mealplan;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;

public interface DeleteMealplanCommand extends Command<String, BaseCommandResponse<Void>> {
}
