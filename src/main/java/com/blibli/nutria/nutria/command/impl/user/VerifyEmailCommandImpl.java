package com.blibli.nutria.nutria.command.impl.user;

import com.blibli.nutria.nutria.command.user.VerifyEmailCommand;
import com.blibli.nutria.nutria.exception.BadRequestException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.helper.mail.EmailHelper;
import com.blibli.nutria.nutria.model.command.request.user.VerifyEmailRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;


@Component
@AllArgsConstructor
public class VerifyEmailCommandImpl implements VerifyEmailCommand {

  private UserRepository userRepository;
  private EmailHelper emailHelper;
  private ReactiveRedisTemplate<String, String> otpRedisTemplate;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<Void>> execute(Mono<VerifyEmailRequest> request) {

    return request.flatMap(this::handler);

  }

  private Mono<BaseCommandResponse<Void>> handler(VerifyEmailRequest request) {
    return otpRedisTemplate.opsForValue().get(request.getEmail())
        .switchIfEmpty(Mono.error(new BadRequestException("Expired OTP / Email Not Registered")))
        .map(otp -> request.getOtp().toString().equals(otp))
        .flatMap(validToken -> {
          if (validToken) {
            return updateUserVerification(request.getEmail());
          } else {
            return Mono.error(new BadRequestException("Invalid OTP"));
          }
        });
  }

  private Mono<BaseCommandResponse<Void>> updateUserVerification(String email){
    return userRepository.findFirstByEmail(email)
        .map(user -> {
          user.setVerified(true);
          return user;
        }).flatMap(userRepository::save)
        .then(otpRedisTemplate.opsForValue().delete(email))
        .thenReturn(new BaseCommandResponse<>(HttpStatus.OK, null));
  }

}
