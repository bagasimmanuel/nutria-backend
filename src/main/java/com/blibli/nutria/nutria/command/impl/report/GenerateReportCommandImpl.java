package com.blibli.nutria.nutria.command.impl.report;

import com.blibli.nutria.nutria.command.report.GenerateReportCommand;
import com.blibli.nutria.nutria.exception.InvalidDateException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.DateHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.report.ReportResponse;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import com.blibli.nutria.nutria.model.entity.Report;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.repository.ReportRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class GenerateReportCommandImpl implements GenerateReportCommand {

  private DateHelper dateHelper;
  private UUIDHelper uuidHelper;
  private ConstructorHelper constructorHelper;
  private ReportRepository reportRepository;
  private SubscriptionRepository subscriptionRepository;

  @Override
  public Mono<BaseCommandResponse<ReportResponse>> execute(Mono<Long> request) {
    return request.flatMap(this::validateRequest)
        .flatMap(this::handler);
  }

  private Mono<Long> validateRequest(Long timestamp) {
    if (dateHelper.isMonday(timestamp))
      return Mono.just(timestamp);
    return Mono.error(new InvalidDateException());
  }

  private Mono<BaseCommandResponse<ReportResponse>> handler(Long timestamp) {
    return reportRepository.findFirstByTimestamp(timestamp)
        .switchIfEmpty(Mono.defer(() -> Mono.just(Report.builder().build())))
        .flatMap(report -> {
          if (report.getTimestamp() == 0) { // Brarti blom ada report, generate report dan jadikan report response
            return generateReport(timestamp);
          } else { // Brarti report udh ada tinggal dijadikan report response
            return Mono.just(report);
          }
        }).flatMap(this::toReportResponse)
        .map(reportResponse -> new BaseCommandResponse<>(HttpStatus.OK, reportResponse));
  }

  private Mono<Report> generateReport(Long timestamp) {

    return subscriptionRepository.findAllByPaymentStatusAndTimestamp(Subscription.PaymentStatus.PAID, timestamp)
        .switchIfEmpty(Mono.defer(() -> {
          return Mono.error(new InvalidDateException());
        })) // Berarti Nutria tanggal itu belum operasional even tho its monday.
        .collectList()
        .flatMap(this::processReport);
  }

  private Mono<Report> processReport(List<Subscription> subscriptionList) {
    int subscriptionCount = subscriptionList.size();
    long timestamp = subscriptionList.get(0).getTimestamp();
    long revenue = 0;
    HashMap<String, Integer> hashMap = new HashMap<>();

    for (Subscription subscription : subscriptionList) {
      for (SubscriptionItem subscriptionItem : subscription.getSubscriptionList()) {
        if (hashMap.containsKey(subscriptionItem.getMenuId())) {
          hashMap.put(subscriptionItem.getMenuId(), hashMap.get(subscriptionItem.getMenuId()) + 1);
        } else {
          hashMap.put(subscriptionItem.getMenuId(), 1);
        }
        revenue += 40000;
      }
    }

    List<Report.ReportItem> reportItems = hashMap.entrySet().stream()
        .map(e -> Report.ReportItem.builder()
            .menuId(e.getKey())
            .purchaseCount(e.getValue())
            .build())
        .collect(Collectors.toList());

    return reportRepository.save(Report.builder()
        .id(uuidHelper.randomUUID())
        .revenue(revenue)
        .timestamp(timestamp)
        .subscriptionCount(subscriptionCount)
        .items(reportItems)
        .build());
  }

  private Mono<ReportResponse> toReportResponse(Report report) {
    return constructorHelper.constructReportResponse(report);
  }

}

