package com.blibli.nutria.nutria.command.impl.user;

import com.blibli.nutria.nutria.command.user.GetNewTokenCommand;
import com.blibli.nutria.nutria.exception.BadRequestException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.user.NewAccessTokenRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.NewAccessTokenResponse;
import com.blibli.nutria.nutria.model.entity.RefreshToken;
import com.blibli.nutria.nutria.repository.RefreshTokenRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.time.Duration;
import java.util.Date;
import java.util.UUID;

@Component
@AllArgsConstructor
public class GetNewTokenCommandImpl implements GetNewTokenCommand {

  private UUIDHelper uuidHelper;
  private ReactiveRedisTemplate<UUID, String> redisTemplate;
  private RefreshTokenRepository refreshTokenRepository;
  private UserRepository userRepository;

  @Override
  public Mono<BaseCommandResponse<NewAccessTokenResponse>> execute(Mono<NewAccessTokenRequest> request) {
    return request.flatMap(this::handler);
  }

  private Mono<BaseCommandResponse<NewAccessTokenResponse>> handler(NewAccessTokenRequest request) {
    Mono<NewAccessTokenRequest> requestMono = Mono.just(request);
    return requestMono.flatMap(tokenRequest -> refreshTokenRepository.findFirstByRefreshToken(uuidHelper.fromString(request.getRefreshToken())))
        //     refreshTokenRepository.findFirstByRefreshToken(uuidHelper.fromString(request.getRefreshToken()))
        //      kalau pakai atas, dia throw Illegal Argument Exception tapi ngga di catch, soale rasae belum masuk Stream jdi mek Throw Exception doang.
        .onErrorResume(IllegalArgumentException.class, e -> Mono.error(new BadRequestException("Invalid UUID")))
        .switchIfEmpty(Mono.error(new BadRequestException("Invalid RefreshToken")))
        .flatMap(this::generateNewTokens)
        .map(this::constructTokenResponse)
        .map(response -> new BaseCommandResponse<>(HttpStatus.OK, response));

  }

  private Mono<Tuple2<RefreshToken, UUID>> generateNewTokens(RefreshToken refreshToken) {

    UUID oldAccessToken = refreshToken.getAccessToken();
    UUID newAccessToken = uuidHelper.randomUUID();
    RefreshToken newRefreshToken = RefreshToken.builder()
        .id(uuidHelper.randomUUID())
        .ttl(new Date())
        .refreshToken(uuidHelper.randomUUID())
        .email(refreshToken.getEmail())
        .accessToken(newAccessToken)
        .build();

    return refreshTokenRepository.delete(refreshToken)
        .then(redisTemplate.delete(oldAccessToken))
        .then(refreshTokenRepository.save(newRefreshToken))
        .zipWith(saveAccessToken(refreshToken.getEmail(),newAccessToken));


  }

  private Mono<UUID> saveAccessToken(String email,UUID newAccessToken) {
    return redisTemplate.opsForValue()
        .set(newAccessToken, email, Duration.ofHours(1))
        .thenReturn(newAccessToken);
  }

  private NewAccessTokenResponse constructTokenResponse(Tuple2<RefreshToken, UUID> tuple2) {
    return NewAccessTokenResponse.builder()
        .accessToken(tuple2.getT2())
        .refreshToken(tuple2.getT1().getRefreshToken()).build();
  }


}
