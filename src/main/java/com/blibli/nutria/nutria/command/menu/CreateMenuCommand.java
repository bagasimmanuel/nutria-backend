package com.blibli.nutria.nutria.command.menu;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.menu.CreateMenuRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;

public interface CreateMenuCommand extends Command<CreateMenuRequest,BaseCommandResponse<MenuAdminResponse>> {
}
