package com.blibli.nutria.nutria.command.impl.mealplan;

import com.blibli.nutria.nutria.command.mealplan.UploadImageMealplanCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.StorageHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.GetMealplanResponse;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
@AllArgsConstructor
public class UploadImageMealplanCommandImpl implements UploadImageMealplanCommand {

  private MealplanRepository mealplanRepository;
  private StorageHelper storageHelper;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<GetMealplanResponse>> execute(Mono<Tuple2<String, FilePart>> request) {
    return request.flatMap(this::updateMealplanData)
        .flatMap(mealplanRepository::save)
        .map(updatedMealplan -> new BaseCommandResponse<>(HttpStatus.OK, toGetMealplanResponse(updatedMealplan)));
  }

  Mono<Mealplan> updateMealplanData(Tuple2<String, FilePart> tuple2) {
    return mealplanRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(tuple2.getT1()))
        .switchIfEmpty(Mono.error(new NotFoundException("MEALPLAN")))
        .flatMap(mealplan -> storageHelper.storeFile(tuple2.getT2(), StorageHelper.STORAGE_TYPE.MEALPLAN_IMAGE,uuidHelper.fromString(tuple2.getT1())))
        .flatMap(path -> setMealplanImageUrl(path, tuple2.getT1()));

  }

  private Mono<Mealplan> setMealplanImageUrl(String path, String mealplanId){
    return mealplanRepository.findById(uuidHelper.fromString(mealplanId))
        .map(mealplan -> {
          mealplan.setImageUrl(path);
          return mealplan;
        });
  }

  private GetMealplanResponse toGetMealplanResponse(Mealplan mealplan) {
    return GetMealplanResponse.builder()
        .id(mealplan.getId())
        .description(mealplan.getDescription())
        .name(mealplan.getName())
        .imageUrl(mealplan.getImageUrl())
        .build();
  }

}
//  Path completePath = Paths.get(Paths.get("storage").toAbsolutePath().normalize().toString(), tuple2.getT2().filename());
//      return tuple2.getT2().transferTo(new File(completePath.toString())).subscribe(); // ??? this feels weird man

