package com.blibli.nutria.nutria.command.impl.subscription.item;

import com.blibli.nutria.nutria.command.subscription.item.GetAllActiveSubscriptionCommand;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.DateHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.ActiveOrderResponse;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
@RequiredArgsConstructor
public class GetAllActiveSubscriptionCommandImpl implements GetAllActiveSubscriptionCommand {

  private final DateHelper dateHelper;
  private final UUIDHelper uuidHelper;
  private final ConstructorHelper constructorHelper;
  private final SubscriptionRepository subscriptionRepository;
  private final MenuRepository menuRepository;

  @Value("${itemsPerPage:#{10}}")
  private int ITEMS_PER_PAGE;


  @Override
  public Mono<BasePaginatedCommandResponse<ActiveOrderResponse>> execute(Mono<Tuple2<Long, Integer>> request) {
    return request.flatMap(this::handler);
  }

  private Mono<BasePaginatedCommandResponse<ActiveOrderResponse>> handler(Tuple2<Long, Integer> tuple2) {
    return subscriptionRepository.findAllByPaymentStatusAndSubscriptionItemTimestamp(Subscription.PaymentStatus.PAID,
            tuple2.getT1(),
            PageRequest.of(tuple2.getT2(), ITEMS_PER_PAGE))
        .flatMapSequential(subscription -> constructorHelper.constructActiveOrderResponse(subscription, tuple2.getT1()))
        .collectList()
        .zipWith(subscriptionRepository.countAllByPaymentStatusAndSubscriptionItemTimestamp(Subscription.PaymentStatus.PAID,
            tuple2.getT1()))
        .map(tupleResponse -> new BasePaginatedCommandResponse<>(HttpStatus.OK,
            tupleResponse.getT1(),
            BasePaginatedCommandResponse.Pagination.builder()
                .currentPage(tuple2.getT2())
                .totalItems(tupleResponse.getT2())
                .itemsPerPage(ITEMS_PER_PAGE).build()));
  }

}
