package com.blibli.nutria.nutria.command.stock;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;

public interface FindByIdStockCommand extends Command<String, BaseCommandResponse<StockResponse>> {
}
