package com.blibli.nutria.nutria.command.user;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.user.NewAccessTokenRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.NewAccessTokenResponse;

public interface GetNewTokenCommand extends Command<NewAccessTokenRequest, BaseCommandResponse<NewAccessTokenResponse>> {
}
