package com.blibli.nutria.nutria.command.subscription.item;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionDeliveryStatusRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.ActiveOrderResponse;
import reactor.util.function.Tuple2;

public interface EditSubscriptionDeliveryStatusCommand extends Command<Tuple2<String, EditSubscriptionDeliveryStatusRequest>, BaseCommandResponse<ActiveOrderResponse>> {
}
