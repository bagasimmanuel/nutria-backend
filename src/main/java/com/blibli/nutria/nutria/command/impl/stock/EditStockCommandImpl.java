package com.blibli.nutria.nutria.command.impl.stock;

import com.blibli.nutria.nutria.command.stock.EditStockCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.stock.EditStockRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.repository.StockRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
@AllArgsConstructor
public class EditStockCommandImpl implements EditStockCommand {

  private StockRepository stockRepository;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<StockResponse>> execute(Mono<Tuple2<EditStockRequest, String>> request) {
    return request.flatMap(this::constructStock)
        .flatMap(stockRepository::save)
        .map(updatedStock -> new BaseCommandResponse<>(HttpStatus.OK, toStockResponse(updatedStock)));
  }

  private Mono<Stock> constructStock(Tuple2<EditStockRequest, String> request) {
    Mono<Stock> stockMono = stockRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(request.getT2()))
        .switchIfEmpty(Mono.error(new NotFoundException("STOCK")));
    return stockMono.map(stock -> updateStock(request.getT1(), stock));

  }

  private Stock updateStock(EditStockRequest request, Stock stock) {
    stock.setQuantity(request.getQuantity());
    stock.setDescription(request.getDescription());
    stock.setName(request.getName());
    return stock;
  }

  private StockResponse toStockResponse(Stock stock) {

    return StockResponse.builder()
        .description(stock.getDescription())
        .imageUrl(stock.getImageUrl())
        .name(stock.getName())
        .quantity(stock.getQuantity())
        .id(stock.getId())
        .build();

  }

}
