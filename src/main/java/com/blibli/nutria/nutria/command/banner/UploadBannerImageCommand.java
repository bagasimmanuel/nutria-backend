package com.blibli.nutria.nutria.command.banner;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.banner.BannerResponse;
import org.springframework.http.codec.multipart.FilePart;

public interface UploadBannerImageCommand extends Command<FilePart, BaseCommandResponse<BannerResponse>> {
}
