package com.blibli.nutria.nutria.command.user;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.user.LoginUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.LoginUserResponse;

public interface LoginUserCommand extends Command<LoginUserRequest, BaseCommandResponse<LoginUserResponse>> {
}
