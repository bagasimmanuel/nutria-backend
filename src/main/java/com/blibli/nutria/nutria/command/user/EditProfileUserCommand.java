package com.blibli.nutria.nutria.command.user;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.user.EditProfileUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.EditProfileUserResponse;

public interface EditProfileUserCommand extends Command<EditProfileUserRequest, BaseCommandResponse<EditProfileUserResponse>> {
}
