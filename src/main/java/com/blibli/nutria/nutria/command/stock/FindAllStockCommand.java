package com.blibli.nutria.nutria.command.stock;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;

public interface FindAllStockCommand extends Command<Integer, BasePaginatedCommandResponse<StockResponse>> {
}
