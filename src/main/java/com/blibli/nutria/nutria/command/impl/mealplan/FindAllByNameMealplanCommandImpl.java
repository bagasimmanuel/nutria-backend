package com.blibli.nutria.nutria.command.impl.mealplan;

import com.blibli.nutria.nutria.command.mealplan.FindAllByNameMealplanCommand;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.GetMealplanResponse;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class FindAllByNameMealplanCommandImpl implements FindAllByNameMealplanCommand {

  private final MealplanRepository mealplanRepository;

  @Value("${itemsPerPage:#{10}}")
  private int ITEMS_PER_PAGE;


  @Override
  public Mono<BasePaginatedCommandResponse<GetMealplanResponse>> execute(Mono<Tuple2<String, Integer>> request) {

    Mono<Tuple2<String, Integer>> cached = request.cache();

    return request.flatMapMany(tuple -> {
          Pageable pageable = PageRequest.of(tuple.getT2(), ITEMS_PER_PAGE);
          return mealplanRepository.findAllByNameLikeAndSoftDeleteFalse(tuple.getT1(), pageable); // Get All Mealplan by Name
        }).collectList()
        .zipWith(cached) // Zip with request to get Total Items
        .flatMap(tuple2 -> {
          Mono<Long> totalItems = mealplanRepository.countAllByNameLikeAndSoftDeleteFalse(tuple2.getT2().getT1());
          return totalItems.map(longTotalItem ->
              Tuples.of(tuple2.getT1(), longTotalItem, tuple2.getT2().getT2())// return tuplesOfThree List<Mealplan>,TotalItem,CurrentPage
          );
        }).map(tuple3 -> new BasePaginatedCommandResponse<>(
            HttpStatus.OK,
            toGetMealplanResponse(tuple3.getT1()),
            BasePaginatedCommandResponse.Pagination.builder()
                .currentPage(tuple3.getT3())
                .totalItems(tuple3.getT2())
                .itemsPerPage(ITEMS_PER_PAGE)
                .build()));
  }

  private List<GetMealplanResponse> toGetMealplanResponse(List<Mealplan> response) {
    return response.stream().map(mealplan -> {
      GetMealplanResponse response1 = new GetMealplanResponse();
      BeanUtils.copyProperties(mealplan, response1);
      return response1;
    }).collect(Collectors.toList());
  }


}
