package com.blibli.nutria.nutria.command.subscription;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.subscription.CreateSubscriptionRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;

public interface CreateSubscriptionCommand extends Command<CreateSubscriptionRequest, BaseCommandResponse<SubscriptionResponse>> {
}
