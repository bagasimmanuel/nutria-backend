package com.blibli.nutria.nutria.command.mealplan;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.GetMealplanResponse;
import org.springframework.http.codec.multipart.FilePart;
import reactor.util.function.Tuple2;

public interface UploadImageMealplanCommand extends Command<Tuple2<String,FilePart>, BaseCommandResponse<GetMealplanResponse>> {
}
