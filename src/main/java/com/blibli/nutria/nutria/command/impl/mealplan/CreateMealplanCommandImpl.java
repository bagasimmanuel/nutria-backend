package com.blibli.nutria.nutria.command.impl.mealplan;

import com.blibli.nutria.nutria.command.mealplan.CreateMealplanCommand;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.mealplan.CreateMealplanRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.CreateMealplanResponse;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class CreateMealplanCommandImpl implements CreateMealplanCommand {

  private MealplanRepository mealplanRepository;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<CreateMealplanResponse>> execute(Mono<CreateMealplanRequest> request) {

    Mono<Mealplan> mealplanMono = request.map(createMealplanRequest -> Mealplan.builder()
        .id(uuidHelper.randomUUID())
        .name(createMealplanRequest.getName())
        .description(createMealplanRequest.getDescription())
        .imageUrl(null)
        .softDelete(false)
        .build()).flatMap(mealplanRepository::save);
    return mealplanMono.map(response -> new BaseCommandResponse<>(HttpStatus.OK, toCreateMealplanResponse(response)));
  }

  private CreateMealplanResponse toCreateMealplanResponse(Mealplan mealplan){
    CreateMealplanResponse response = new CreateMealplanResponse();
    BeanUtils.copyProperties(mealplan,response);
    return response;
  }

}
