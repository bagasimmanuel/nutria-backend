package com.blibli.nutria.nutria.command.impl.mealplan;

import com.blibli.nutria.nutria.command.mealplan.FindByIdMealplanCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.mealplan.FindByIdMealplanRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.FindByIdMealplanResponse;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class FindByIdMealplanCommandImpl implements FindByIdMealplanCommand {

  private MealplanRepository mealplanRepository;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<FindByIdMealplanResponse>> execute(Mono<FindByIdMealplanRequest> request) {
    return request
        .flatMap(findByIdMealplanRequest -> mealplanRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(findByIdMealplanRequest.getMealplanId())))
        .switchIfEmpty(Mono.error(new NotFoundException("MEALPLAN")))
        .map(response -> new BaseCommandResponse<>(HttpStatus.OK,toFindByIdMealplanResponse(response)));
  }


  private FindByIdMealplanResponse toFindByIdMealplanResponse(Mealplan mealplan){

    FindByIdMealplanResponse response = new FindByIdMealplanResponse();
    BeanUtils.copyProperties(mealplan,response);
    return response;

  }
}
