package com.blibli.nutria.nutria.command.impl.banner;

import com.blibli.nutria.nutria.command.banner.UploadBannerImageCommand;
import com.blibli.nutria.nutria.helper.StorageHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.banner.BannerResponse;
import com.blibli.nutria.nutria.model.entity.Banner;
import com.blibli.nutria.nutria.repository.BannerRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.UUID;


@Component
@AllArgsConstructor
public class UploadBannerImageCommandImpl implements UploadBannerImageCommand {

  private BannerRepository bannerRepository;
  private UUIDHelper uuidHelper;
  private StorageHelper storageHelper;

  @Override
  public Mono<BaseCommandResponse<BannerResponse>> execute(Mono<FilePart> request) {
    return request.flatMap(this::handler);
  }

  private Mono<BaseCommandResponse<BannerResponse>> handler(FilePart filePart){
    UUID bannerId = uuidHelper.randomUUID();
    Mono<String> stringMono = storageHelper.storeFile(filePart, StorageHelper.STORAGE_TYPE.BANNER_IMAGE,bannerId);
    return stringMono.map(imagePath -> Banner.builder().id(bannerId).imageUrl(imagePath).build())
        .flatMap(bannerRepository::save)
        .map(this::toBannerResponse)
        .map(bannerResponse -> new BaseCommandResponse<>(HttpStatus.OK,bannerResponse));
  }


  private BannerResponse toBannerResponse(Banner banner){
    return BannerResponse.builder()
        .id(banner.getId().toString())
        .imageUrl(banner.getImageUrl())
        .build();
  }

}
