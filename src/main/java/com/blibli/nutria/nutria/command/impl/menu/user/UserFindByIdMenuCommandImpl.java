package com.blibli.nutria.nutria.command.impl.menu.user;

import com.blibli.nutria.nutria.command.menu.user.UserFindByIdMenuCommand;
import com.blibli.nutria.nutria.exception.BadRequestException;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuResponse;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class UserFindByIdMenuCommandImpl implements UserFindByIdMenuCommand {

  private UUIDHelper uuidHelper;
  private ConstructorHelper constructorHelper;
  private MealplanRepository mealplanRepository;
  private MenuRepository menuRepository;

  @Override
  public Mono<BaseCommandResponse<MenuResponse>> execute(Mono<String> request) {
    return request.flatMap(menuId -> menuRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(menuId)))
        .onErrorResume(IllegalArgumentException.class,e -> Mono.error(new BadRequestException("Invalid UUID")))
        .switchIfEmpty(Mono.error(new NotFoundException("MENU")))
        .flatMap(constructorHelper::constructMenuResponse)
        .map(menuResponse -> new BaseCommandResponse<>(HttpStatus.OK,menuResponse));
  }




}
