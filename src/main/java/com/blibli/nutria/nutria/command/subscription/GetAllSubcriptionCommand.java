package com.blibli.nutria.nutria.command.subscription;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionCriteria;
import reactor.util.function.Tuple2;

public interface GetAllSubcriptionCommand extends Command<Tuple2<SubscriptionCriteria,Integer>, BasePaginatedCommandResponse<SubscriptionResponse>> {



}
