package com.blibli.nutria.nutria.command.user;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.user.ChangePasswordRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;

public interface ChangePasswordUserCommand extends Command<ChangePasswordRequest, BaseCommandResponse<Void>> {


}
