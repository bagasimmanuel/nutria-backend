package com.blibli.nutria.nutria.command.impl.menu.user;

import com.blibli.nutria.nutria.command.menu.user.UserFindAllWithParam;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuResponse;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.menu.MenuCriteria;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.List;

@Component
@RequiredArgsConstructor
public class UserFindAllWithParamImpl implements UserFindAllWithParam {

  private final UUIDHelper uuidHelper;
  private final ConstructorHelper constructorHelper;
  private final MealplanRepository mealplanRepository;
  private final MenuRepository menuRepository;

  @Value("${itemsPerPage:#{10}}")
  private int ITEMS_PER_PAGE;

  @Override
  public Mono<BasePaginatedCommandResponse<MenuResponse>> execute(Mono<Tuple2<MenuCriteria, Integer>> request) {
    return request.flatMap(this::handle);
  }

  private Mono<BasePaginatedCommandResponse<MenuResponse>> handle(Tuple2<MenuCriteria, Integer> request) {

    Pageable pageRequest = PageRequest.of(request.getT2(), ITEMS_PER_PAGE, Sort.by("name"));
    Mono<Tuple2<List<MenuResponse>, Long>> tuple2Mono = menuRepository.findAllWithParam(request.getT1(),pageRequest)
        .flatMapSequential(constructorHelper::constructMenuResponse)
        .collectList()
        .zipWith(menuRepository.countAllWithParam(request.getT1()));

    return tuple2Mono.map(tuple -> new BasePaginatedCommandResponse<>(HttpStatus.OK,
        tuple.getT1(),
        BasePaginatedCommandResponse.Pagination.builder()
            .totalItems(tuple.getT2())
            .itemsPerPage(ITEMS_PER_PAGE)
            .currentPage(request.getT2())
            .build()));
  }

}
