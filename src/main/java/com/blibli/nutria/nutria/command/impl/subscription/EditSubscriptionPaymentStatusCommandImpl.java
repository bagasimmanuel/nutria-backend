package com.blibli.nutria.nutria.command.impl.subscription;

import com.blibli.nutria.nutria.command.subscription.EditSubscriptionPaymentStatusCommand;
import com.blibli.nutria.nutria.exception.EmptyRequestException;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.exception.UnauthorizedException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionPaymentStatusRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import com.blibli.nutria.nutria.model.entity.Necessity;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.NecessityRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class EditSubscriptionPaymentStatusCommandImpl implements EditSubscriptionPaymentStatusCommand {

  private SubscriptionRepository subscriptionRepository;
  private NecessityRepository necessityRepository;
  private MenuRepository menuRepository;
  private UserRepository userRepository;
  private UUIDHelper uuidHelper;
  private ConstructorHelper constructorHelper;

  @Override
  public Mono<BaseCommandResponse<SubscriptionResponse>> execute(Mono<Tuple2<String, EditSubscriptionPaymentStatusRequest>> request) {
    return request.switchIfEmpty(Mono.error(new EmptyRequestException()))
        .flatMap(this::handler);
  }

  private Mono<BaseCommandResponse<SubscriptionResponse>> handler(Tuple2<String, EditSubscriptionPaymentStatusRequest> tuple2) {
    Mono<User> userMono = ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(x -> x.getPrincipal().toString())
        .flatMap(userRepository::findFirstByEmail);

    return userMono.flatMap(user -> validateRequest(user, tuple2))
        .flatMap(validatedTuple -> subscriptionRepository.findFirstById(uuidHelper.fromString(validatedTuple.getT1())))
        .map(subscription -> {
          subscription.setPaymentStatus(tuple2.getT2().getPaymentStatus());
          return subscription;
        }).flatMap(subscriptionRepository::save)
        .flatMap(this::updateNecessity)
        .flatMap(constructorHelper::constructSubscriptionResponse)
        .map(subscriptionResponse -> new BaseCommandResponse<>(HttpStatus.OK, subscriptionResponse));
  }

  private Mono<Tuple2<String, EditSubscriptionPaymentStatusRequest>> validateRequest(User user,
      Tuple2<String, EditSubscriptionPaymentStatusRequest> tuple2) {
    return subscriptionRepository.findFirstById(uuidHelper.fromString(tuple2.getT1()))
        .switchIfEmpty(Mono.error(new NotFoundException("SUBSCRIPTION")))
        .flatMap(subscription -> {
          if (user.getRoles().contains("ADMIN") || user.getId().toString().equals(subscription.getUserId()) &&
              tuple2.getT2().getPaymentStatus().equals(Subscription.PaymentStatus.CANCELLED)) {
            return Mono.just(tuple2);
          } else {
            return Mono.error(new UnauthorizedException());
          }
        });
  }

  private Mono<Subscription> updateNecessity(Subscription subscription) {
    if(subscription.getPaymentStatus() == Subscription.PaymentStatus.PAID){
      Flux<SubscriptionItem> subscriptionItemFlux = Flux.fromIterable(subscription.getSubscriptionList());
      return subscriptionItemFlux.flatMap(subscriptionItem ->
              necessityRepository.findFirstByTimestamp(subscriptionItem.getTimestamp())
                  .defaultIfEmpty(Necessity.builder().timestamp(0).build())
                  .flatMap(necessity -> handleNecessity(necessity, subscriptionItem)))
          .collectList()
          .then(Mono.just(subscription));
    }else{
      return Mono.just(subscription);
    }
  }

  private Mono<Necessity> handleNecessity(Necessity necessity, SubscriptionItem subscriptionItem) {
    if (necessity.getTimestamp() == 0) {
      Necessity toBeSavedNecessity = Necessity.builder().build();
      toBeSavedNecessity.setId(uuidHelper.randomUUID());
      toBeSavedNecessity.setTimestamp(subscriptionItem.getTimestamp());
      return menuRepository.findFirstById(uuidHelper.fromString(subscriptionItem.getMenuId()))
          .map(menu -> {
            toBeSavedNecessity.setIngredientList(menu.getRecipes());
            return toBeSavedNecessity;
          }).flatMap(necessityRepository::save);
    } else {
      return menuRepository.findFirstById(uuidHelper.fromString(subscriptionItem.getMenuId()))
          .map(menu -> {
            System.out.println(necessity);
            List<Ingredient> menuIngredients = menu.getRecipes();
            List<Ingredient> currentNecessities = necessity.getIngredientList();
            List<Ingredient> updatedNecessities = new ArrayList<>();

            for (Ingredient currentNecessity : currentNecessities) {
              for (Ingredient menuIngredient : menuIngredients) {
                if (menuIngredient.getStockId().equals(currentNecessity.getStockId()))
                  currentNecessity.setQuantity(currentNecessity.getQuantity() + menuIngredient.getQuantity());
              }
              updatedNecessities.add(currentNecessity);
            }
            necessity.setIngredientList(updatedNecessities);
            return necessity;
          }).flatMap(necessityRepository::save);
    }
  }


}
