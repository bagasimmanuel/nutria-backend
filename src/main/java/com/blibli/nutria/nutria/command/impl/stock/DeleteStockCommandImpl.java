package com.blibli.nutria.nutria.command.impl.stock;

import com.blibli.nutria.nutria.command.stock.DeleteStockCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.StorageHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.repository.StockRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class DeleteStockCommandImpl implements DeleteStockCommand {

  private UUIDHelper uuidHelper;
  private StorageHelper storageHelper;
  private StockRepository stockRepository;

  @Override
  public Mono<BaseCommandResponse<Void>> execute(Mono<String> request) {
    return request.flatMap(this::handler);
  }

  private Mono<BaseCommandResponse<Void>> handler(String stockId) {
    System.out.println(stockId);
    return stockRepository.findById(uuidHelper.fromString(stockId))
        .switchIfEmpty(Mono.error(new NotFoundException("STOCK")))
        .map(stock -> {
          stock.setSoftDelete(true);
          return stock;
        })
        .flatMap(stockRepository::save)
        //        .then(storageHelper.deleteFile(StorageHelper.STORAGE_TYPE.STOCK_IMAGE, uuidHelper.fromString(stockId))
        .thenReturn(new BaseCommandResponse<>(HttpStatus.OK, null));
  }
}
