package com.blibli.nutria.nutria.command.subscription.item;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.ActiveOrderResponse;
import reactor.util.function.Tuple2;

public interface GetAllActiveSubscriptionCommand extends Command<Tuple2<Long,Integer>, BasePaginatedCommandResponse<ActiveOrderResponse>> {
}
