package com.blibli.nutria.nutria.command.menu;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import org.springframework.http.codec.multipart.FilePart;
import reactor.util.function.Tuple2;

public interface UploadImageMenuCommand extends Command<Tuple2<String, FilePart>, BaseCommandResponse<MenuAdminResponse>> {

}
