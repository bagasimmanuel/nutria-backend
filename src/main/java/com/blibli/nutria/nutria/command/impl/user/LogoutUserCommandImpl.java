package com.blibli.nutria.nutria.command.impl.user;

import com.blibli.nutria.nutria.command.user.LogoutUserCommand;
import com.blibli.nutria.nutria.exception.BadCredentialsException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Component
@AllArgsConstructor
public class LogoutUserCommandImpl implements LogoutUserCommand {

  private final ReactiveRedisTemplate<UUID,String> redisTemplate;
  private final UUIDHelper uuidHelper;
  @Override
  public Mono<BaseCommandResponse<Void>> execute(Mono<String> request) {
    Mono<Boolean> booleanMono = request
        .filter(header -> header.startsWith("Bearer "))
        .switchIfEmpty(Mono.error(new BadCredentialsException()))
        .map(authHeader -> authHeader.substring(7))
        .flatMap(accessToken -> redisTemplate.opsForValue().delete(uuidHelper.fromString(accessToken)));
    return booleanMono.map(response -> new BaseCommandResponse<Void>(HttpStatus.OK,null));
  }
}
