package com.blibli.nutria.nutria.command.impl.user;

import com.blibli.nutria.nutria.command.user.RegisterUserCommand;
import com.blibli.nutria.nutria.exception.DuplicateKeyException;
import com.blibli.nutria.nutria.exception.EmptyRequestException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.helper.mail.EmailHelper;
import com.blibli.nutria.nutria.model.command.request.user.RegisterUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.RegisterUserResponse;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
@AllArgsConstructor
public class RegisterUserCommandImpl implements RegisterUserCommand {

  private UserRepository userRepository;
  private PasswordEncoder passwordEncoder;
  private ReactiveRedisTemplate<String,String> otpRedisTemplate;
  private UUIDHelper uuidHelper;
  private EmailHelper emailHelper;

  @Override
  public Mono<BaseCommandResponse<RegisterUserResponse>> execute(Mono<RegisterUserRequest> request) {


    return request.switchIfEmpty(Mono.error(new EmptyRequestException()))
        .map(registerUserRequest -> User.builder()
            .id(uuidHelper.randomUUID())
            .email(registerUserRequest.getEmail())
            .fullName(registerUserRequest.getFullName())
            .password(passwordEncoder.encode(registerUserRequest.getPassword()))
            .roles(Arrays.asList("USER"))
            .verified(true) // Janlup diganti kalau mau pakai email verification
            .build())
        .flatMap(userRepository::save)
        .zipWhen(this::generateOtp)
        .onErrorResume(org.springframework.dao.DuplicateKeyException.class, e -> Mono.error(new DuplicateKeyException()))
        .map(tuple2 -> {
              Mono.fromCallable(() -> {

                HashMap<String, String> replacements = new HashMap<>();
                replacements.put("fullName", tuple2.getT1().getFullName());
                replacements.put("OTP",tuple2.getT2());

//
//                String template = String.join("\n", Files.readAllLines(
//                    new ClassPathResource("mail-template/email-verification.txt").getFile().toPath()));

                String template = "";
                ClassPathResource resource = new ClassPathResource("mail-template/email-verification.txt");
                byte[] bdata = FileCopyUtils.copyToByteArray(resource.getInputStream());
                template = new String(bdata, StandardCharsets.UTF_8);

                for (Map.Entry<String, String> entry : replacements.entrySet()) {
                  template = template.replaceAll("\\{" + entry.getKey() + "\\}", entry.getValue());
                }

                emailHelper.sendEmail(tuple2.getT1().getEmail(), "Nutria - Email Verification", template);
                return true;
              }).subscribe();
              return tuple2.getT1();
            }
        ).map(user -> new BaseCommandResponse<>(HttpStatus.OK, toRegisterUserResponse(user)));

  }

  private Mono<String> generateOtp(User user){
//    String otp = new DecimalFormat("000000").format(new SecureRandom().nextInt(999999));
    String otp = "111111";
    return otpRedisTemplate.opsForValue().set(user.getEmail(),otp, Duration.ofSeconds(300))
        .thenReturn(otp);
  }

  private RegisterUserResponse toRegisterUserResponse(User user) {

    return RegisterUserResponse.builder()
        .id(user.getId())
        .email(user.getEmail())
        .fullName(user.getFullName())
        .roles(user.getRoles())
        .verified(user.isVerified())
        .build();

  }

}
