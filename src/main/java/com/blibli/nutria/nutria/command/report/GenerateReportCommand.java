package com.blibli.nutria.nutria.command.report;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.report.ReportResponse;

public interface GenerateReportCommand extends Command<Long, BaseCommandResponse<ReportResponse>> {
}
