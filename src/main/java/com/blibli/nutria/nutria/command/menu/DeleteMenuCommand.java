package com.blibli.nutria.nutria.command.menu;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;

public interface DeleteMenuCommand extends Command<String, BaseCommandResponse<Void>> {
}
