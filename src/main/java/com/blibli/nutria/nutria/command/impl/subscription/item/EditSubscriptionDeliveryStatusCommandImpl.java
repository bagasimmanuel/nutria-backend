package com.blibli.nutria.nutria.command.impl.subscription.item;

import com.blibli.nutria.nutria.command.subscription.item.EditSubscriptionDeliveryStatusCommand;
import com.blibli.nutria.nutria.exception.BadRequestException;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.exception.UnauthorizedException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.DateHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionDeliveryStatusRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.ActiveOrderResponse;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import com.blibli.nutria.nutria.model.entity.Menu;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.List;

@Component
@AllArgsConstructor

public class EditSubscriptionDeliveryStatusCommandImpl implements EditSubscriptionDeliveryStatusCommand {

  private SubscriptionRepository subscriptionRepository;
  private MenuRepository menuRepository;
  private UserRepository userRepository;
  private StockRepository stockRepository;
  private ConstructorHelper constructorHelper;
  private DateHelper dateHelper;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<ActiveOrderResponse>> execute(Mono<Tuple2<String, EditSubscriptionDeliveryStatusRequest>> request) {
    return request.flatMap(this::handler);


  }


  private Mono<BaseCommandResponse<ActiveOrderResponse>> handler(Tuple2<String, EditSubscriptionDeliveryStatusRequest> tuple2) {
    Mono<User> userMono = ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(x -> x.getPrincipal().toString())
        .flatMap(userRepository::findFirstByEmail);

    return userMono.flatMap(user -> validateRequest(user, tuple2))
        .map(subscription -> updateSubscription(subscription, tuple2.getT2(), tuple2.getT2().getTimestamp()))
        .flatMap(subscriptionRepository::save)
        .flatMap(subscription -> updateNecessity(subscription, tuple2.getT2(), tuple2.getT2().getTimestamp()))
        .flatMap(updatedSubscription -> constructorHelper.constructActiveOrderResponse(updatedSubscription, tuple2.getT2().getTimestamp()))
        .map(response -> new BaseCommandResponse<>(HttpStatus.OK, response));
  }

  private Mono<Subscription> validateRequest(User user, Tuple2<String, EditSubscriptionDeliveryStatusRequest> tuple2) {
    return subscriptionRepository.findFirstByIdAndPaymentStatusAndSubscriptionItemTimestamp(uuidHelper.fromString(tuple2.getT1()),
            Subscription.PaymentStatus.PAID,
            tuple2.getT2().getTimestamp())
        .switchIfEmpty(Mono.error(new NotFoundException("SUBSCRIPTION")))
        .flatMap(subscription -> {
          if (user.getRoles().contains("ADMIN") || user.getId().toString().equals(subscription.getUserId()) &&
              tuple2.getT2().getDeliveryStatus().equals(SubscriptionItem.DeliveryStatus.DELIVERED)) {
            return Mono.just(subscription);
          } else {
            return Mono.error(new UnauthorizedException());
          }
        });
  }

  private Subscription updateSubscription(Subscription subscription, EditSubscriptionDeliveryStatusRequest statusRequest, Long timestamp) {
    for (SubscriptionItem item : subscription.getSubscriptionList()) {
      if (item.getTimestamp() == timestamp)
        item.setDeliveryStatus(SubscriptionItem.DeliveryStatus.valueOf(statusRequest.getDeliveryStatus().toString()));
    }
    return subscription;
  }

  private Mono<Subscription> updateNecessity(Subscription subscription,
      EditSubscriptionDeliveryStatusRequest statusRequest,
      Long timestamp) {

    if (statusRequest.getDeliveryStatus() == SubscriptionItem.DeliveryStatus.PROCESSED) {
      String menuId = null;
      for (SubscriptionItem item : subscription.getSubscriptionList()) {
        if (item.getTimestamp() == timestamp)
          menuId = item.getMenuId();
      }

      return menuRepository.findFirstById(uuidHelper.fromString(menuId))
          .onErrorResume(IllegalArgumentException.class, e -> Mono.error(new BadRequestException("Invalid UUID")))
          .flatMap(this::decreaseNSaveStock)
          .thenReturn(subscription);
    } else {
      return Mono.just(subscription);
    }
  }

  private Mono<List<Stock>> decreaseNSaveStock(Menu menu) {
    return Flux.fromIterable(menu.getRecipes())
        .flatMap(ingredient ->
            stockRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(ingredient.getStockId()))
                .map(stock -> {
                  stock.setQuantity(stock.getQuantity() - ingredient.getQuantity());
                  return stock;
                }).flatMap(stockRepository::save))
        .collectList();

  }



}
