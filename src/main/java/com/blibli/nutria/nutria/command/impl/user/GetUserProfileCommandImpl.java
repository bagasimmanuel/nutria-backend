package com.blibli.nutria.nutria.command.impl.user;

import com.blibli.nutria.nutria.command.user.GetUserProfileCommand;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.UserProfileResponse;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class GetUserProfileCommandImpl implements GetUserProfileCommand {

  private UserRepository userRepository;
  private MealplanRepository mealplanRepository;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<UserProfileResponse>> execute(Mono<Void> request) {

    return getCurrentUser()
        .flatMap(this::toGetCurrentUserResponse)
        .map(response -> new BaseCommandResponse<>(HttpStatus.OK, response));
  }

  private Mono<User> getCurrentUser() {
    return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(x -> x.getPrincipal().toString())
        .flatMap(userRepository::findFirstByEmail);
  }

  private Mono<UserProfileResponse> toGetCurrentUserResponse(User user) {
    UserProfileResponse response = UserProfileResponse.builder()
        .id(user.getId())
        .email(user.getEmail())
        .fullName(user.getFullName())
        .roles(user.getRoles())
        .height(user.getHeight())
        .weight(user.getWeight())
        .age(user.getAge())
        .activityLevel(user.getActivityLevel())
        .gender(user.getGender())
        .calorieRecom(user.getCalorieRecom())
        .location(user.getLocation()).build();
    if (user.getMealplanId() != null) {
      return mealplanRepository.findById(uuidHelper.fromString(user.getMealplanId()))
          .map(mealplan -> {
            response.setMealplan(mealplan);
            return response;
          });
    }else{
      return Mono.just(response);
    }
  }

}
