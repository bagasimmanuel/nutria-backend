package com.blibli.nutria.nutria.command;

import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import reactor.core.publisher.Mono;

public interface Command <REQUEST,RESPONSE extends BaseCommandResponse<?>> {

  Mono<RESPONSE> execute(Mono<REQUEST> request);

}
