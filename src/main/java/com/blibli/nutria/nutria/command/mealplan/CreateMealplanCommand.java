package com.blibli.nutria.nutria.command.mealplan;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.mealplan.CreateMealplanRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.CreateMealplanResponse;

public interface CreateMealplanCommand extends Command<CreateMealplanRequest, BaseCommandResponse<CreateMealplanResponse>> {
}
