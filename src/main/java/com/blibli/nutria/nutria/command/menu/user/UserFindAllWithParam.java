package com.blibli.nutria.nutria.command.menu.user;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuResponse;
import com.blibli.nutria.nutria.repository.menu.MenuCriteria;
import reactor.util.function.Tuple2;

public interface UserFindAllWithParam extends Command<Tuple2<MenuCriteria,Integer>, BasePaginatedCommandResponse<MenuResponse>> {
}
