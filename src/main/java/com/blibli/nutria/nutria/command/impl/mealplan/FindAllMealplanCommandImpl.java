package com.blibli.nutria.nutria.command.impl.mealplan;

import com.blibli.nutria.nutria.command.mealplan.FindAllMealplanCommand;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.GetMealplanResponse;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class FindAllMealplanCommandImpl implements FindAllMealplanCommand {

  @Value("${itemsPerPage:#{10}}")
  private int ITEMS_PER_PAGE;

  private final MealplanRepository mealplanRepository;

  @Override
  public Mono<BasePaginatedCommandResponse<GetMealplanResponse>> execute(Mono<Integer> request) {

    Mono<Integer> pageNum = request.cache();

    return request.flatMapMany(page -> {
          Pageable pageable = PageRequest.of(page, ITEMS_PER_PAGE);
          return mealplanRepository.findAllByIdNotNullAndSoftDeleteFalse(pageable).log();
        }).collectList()
        .zipWith(mealplanRepository.countAllByIdNotNullAndSoftDeleteFalse())
        .zipWith(pageNum)
        .map(tupleOfTuples -> Tuples.of(tupleOfTuples.getT1().getT1(),tupleOfTuples.getT1().getT2(),tupleOfTuples.getT2())).map(tuple3 -> new BasePaginatedCommandResponse<>(
            HttpStatus.OK,
            toGetMealplanResponse(tuple3.getT1()),
            BasePaginatedCommandResponse.Pagination.builder()
                .currentPage(tuple3.getT3())
                .totalItems(tuple3.getT2())
                .itemsPerPage(ITEMS_PER_PAGE)
                .build()));
  }

  private List<GetMealplanResponse> toGetMealplanResponse(List<Mealplan> response) {
    return response.stream().map(mealplan -> {
      GetMealplanResponse response1 = new GetMealplanResponse();
      BeanUtils.copyProperties(mealplan, response1);
      return response1;
    }).collect(Collectors.toList());
  }

}
