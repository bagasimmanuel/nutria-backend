package com.blibli.nutria.nutria.command.banner;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.banner.BannerResponse;

public interface FindAllBannerCommand extends Command<Integer, BasePaginatedCommandResponse<BannerResponse>> {



}
