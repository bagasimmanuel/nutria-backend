package com.blibli.nutria.nutria.command.impl.subscription.item;

import com.blibli.nutria.nutria.command.subscription.item.EditSubscriptionItemCommand;
import com.blibli.nutria.nutria.exception.BadRequestException;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.DateHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionItemRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import com.blibli.nutria.nutria.model.entity.Necessity;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.NecessityRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class EditSubscriptionItemCommandImpl implements EditSubscriptionItemCommand {

  private UUIDHelper uuidHelper;
  private DateHelper dateHelper;
  private ConstructorHelper constructorHelper;
  private UserRepository userRepository;
  private SubscriptionRepository subscriptionRepository;
  private NecessityRepository necessityRepository;
  private MenuRepository menuRepository;

  @Override
  public Mono<BaseCommandResponse<SubscriptionResponse>> execute(Mono<Tuple2<EditSubscriptionItemRequest, String>> request) {
    return request.flatMap(this::handler);
  }

  private Mono<BaseCommandResponse<SubscriptionResponse>> handler(Tuple2<EditSubscriptionItemRequest, String> tuple2) {
    return subscriptionRepository.findFirstById(uuidHelper.fromString(tuple2.getT2()))
        .switchIfEmpty(Mono.error(new NotFoundException("SUBSCRIPTION")))
        .flatMap(subscription -> validateRequest(subscription, tuple2.getT1()))
        .flatMap(validatedRequest -> updateSubscription(validatedRequest, tuple2.getT1()))
        .flatMap(subscription -> updateNecessity(subscription, "addition"))
        .flatMap(constructorHelper::constructSubscriptionResponse)
        .map(subscriptionResponse -> new BaseCommandResponse<>(HttpStatus.OK, subscriptionResponse));
  }

  private Mono<Subscription> validateRequest(Subscription subscription, EditSubscriptionItemRequest request) {

    Mono<User> userMono = ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(x -> x.getPrincipal().toString())
        .flatMap(userRepository::findFirstByEmail);

    return userMono.flatMap(user -> {
      System.out.println(user);
      System.out.println(subscription);
      System.out.println(request);
      if (user.getId().toString().equals(subscription.getUserId())
          && subscription.getSubscriptionList().size() == request.getSubscriptionList().size()) {
        return Mono.just(subscription);
      } else {
        return Mono.error(new BadRequestException("Invalid Length"));
      }
    });


  }

  private Mono<Subscription> updateSubscription(Subscription oldSubscription, EditSubscriptionItemRequest request) {
    long currentDayInEpoch = dateHelper.getTodayInEpoch(); // Ini testing e aneh
    // Kenapa?
    // Karena bergantung dengan Hari di server BE ne, semisal
    // kita kan mau assertEquals semua hari nih, nah kalau semisal hari server itu hari rabu, maka
    // Hari senin, selasa, rabu itu ngga mungkin bisa diganti, soale if e return false, tpi hari kamis dan jumat e bisa.

    // Paling gampang misal kita testing di hari sabtu, kalau testing di hari sabtu berarti kan Sabtu > Senin - Jumat, brarti hasil e slalu minus dan akan selalu fail.
    Subscription updatedSubscription = oldSubscription;
    int index = 0;
    for (SubscriptionItem item : request.getSubscriptionList()) {
      if (updatedSubscription.getSubscriptionList().get(index).getTimestamp() - currentDayInEpoch >= 86400) {
        updatedSubscription.getSubscriptionList().get(index).setDeliveryLoc(item.getDeliveryLoc());
        updatedSubscription.getSubscriptionList().get(index).setDeliveryTime(item.getDeliveryTime());
        updatedSubscription.getSubscriptionList().get(index).setMenuId(item.getMenuId());
      }
      index++;
    }
    return subscriptionRepository.save(updatedSubscription)
        .flatMap(subscription1 -> updateNecessity(oldSubscription, "subtraction"))
        .thenReturn(updatedSubscription);
    // setelah itu disini harus ngurangi necessity dari menu yang sebelumnya
  }

  private Mono<Subscription> updateNecessity(Subscription subscription, String identifier) {
    if (subscription.getPaymentStatus() == Subscription.PaymentStatus.PAID) {
      Flux<SubscriptionItem> subscriptionItemFlux = Flux.fromIterable(subscription.getSubscriptionList());
      return subscriptionItemFlux.flatMap(subscriptionItem ->
              necessityRepository.findFirstByTimestamp(subscriptionItem.getTimestamp())
                  .defaultIfEmpty(Necessity.builder().timestamp(0).build())
                  .flatMap(necessity -> handleNecessity(necessity, subscriptionItem, identifier)))
          .collectList()
          .then(Mono.just(subscription));
    }
    return Mono.just(subscription); // Brarti ngga update necessity soalnya blum paid
  }

  private Mono<Necessity> handleNecessity(Necessity necessity, SubscriptionItem subscriptionItem, String identifier) {
    long currentDayInEpoch = dateHelper.getTodayInEpoch();
    if (subscriptionItem.getTimestamp() - currentDayInEpoch >= 86400) {
      return menuRepository.findFirstById(uuidHelper.fromString(subscriptionItem.getMenuId()))
          .map(menu -> {
            System.out.println(necessity);
            List<Ingredient> menuIngredients = menu.getRecipes();
            List<Ingredient> currentNecessities = necessity.getIngredientList();
            List<Ingredient> updatedNecessities = new ArrayList<>();

            for (Ingredient currentNecessity : currentNecessities) {
              for (Ingredient menuIngredient : menuIngredients) {
                if (menuIngredient.getStockId().equals(currentNecessity.getStockId()) && identifier.equals("addition")) {
                  currentNecessity.setQuantity(currentNecessity.getQuantity() + menuIngredient.getQuantity());
                } else if (menuIngredient.getStockId().equals(currentNecessity.getStockId()) && identifier.equals("subtraction")) {
                  currentNecessity.setQuantity(currentNecessity.getQuantity() - menuIngredient.getQuantity());
                }
              }
              updatedNecessities.add(currentNecessity);
            }
            necessity.setIngredientList(updatedNecessities);
            return necessity;
          }).flatMap(necessityRepository::save);
    } else {
      return Mono.just(necessity); // Incase ternyata kurang dari h-1 brarti ga boleh di edit subscriptionItem e, jadi necessity ya ga di update
    }
  }
}
