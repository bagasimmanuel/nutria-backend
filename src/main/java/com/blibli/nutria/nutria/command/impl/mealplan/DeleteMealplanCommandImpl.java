package com.blibli.nutria.nutria.command.impl.mealplan;

import com.blibli.nutria.nutria.command.mealplan.DeleteMealplanCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.StorageHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class DeleteMealplanCommandImpl implements DeleteMealplanCommand {

  private MealplanRepository mealplanRepository;
  private StorageHelper storageHelper;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<Void>> execute(Mono<String> request) {
    return request.flatMap(this::handler);
  }

  private Mono<BaseCommandResponse<Void>> handler(String mealplanId) {
    return mealplanRepository.findById(uuidHelper.fromString(mealplanId))
        .switchIfEmpty(Mono.error(new NotFoundException("MEALPLAN")))
        .map(mealplan -> {
          mealplan.setSoftDelete(true);
          return  mealplan;
        })
        .flatMap(mealplanRepository::save)
//        .then(storageHelper.deleteFile(StorageHelper.STORAGE_TYPE.MEALPLAN_IMAGE,uuidHelper.fromString(mealplanId))
        .thenReturn( new BaseCommandResponse<>(HttpStatus.OK,null));
  }

}
