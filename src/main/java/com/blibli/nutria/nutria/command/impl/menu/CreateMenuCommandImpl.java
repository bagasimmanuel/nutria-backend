package com.blibli.nutria.nutria.command.impl.menu;

import com.blibli.nutria.nutria.command.menu.CreateMenuCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.menu.CreateMenuRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.IngredientResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.MealplanResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import com.blibli.nutria.nutria.model.entity.Mealplan;
import com.blibli.nutria.nutria.model.entity.Menu;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple3;
import reactor.util.function.Tuples;

import java.util.List;

@AllArgsConstructor
@Component
public class CreateMenuCommandImpl implements CreateMenuCommand {

  private UUIDHelper uuidHelper;
  private MenuRepository menuRepository;
  private MealplanRepository mealplanRepository;
  private StockRepository stockRepository;
  private ConstructorHelper constructorHelper;

  @Override
  public Mono<BaseCommandResponse<MenuAdminResponse>> execute(Mono<CreateMenuRequest> request) {

  return request.flatMap(this::handler)
      .map(data -> new BaseCommandResponse<>(HttpStatus.OK, data));

  }
  private Mono<MenuAdminResponse> handler(CreateMenuRequest request){

    Mono<List<IngredientResponse>> ingredientFlux =
        Flux.fromIterable(request.getRecipes()) // Flux <Ingredients>
        .flatMap(this::constructNValidateIngredientResponse) // Flux <IngredientResponse>
        .collectList(); // Mono<List<IngredientResponse>>

    Mono<Mealplan> mealplanMono = mealplanRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(request.getMealplanId()))
        .switchIfEmpty(Mono.error(new NotFoundException("MEALPLAN")));

    return menuRepository.save(constructMenu(request))
        .flatMap(menuRepository::save)
        .flatMap(constructorHelper::constructMenuAdminResponse);

  }


  private Menu constructMenu(CreateMenuRequest menuRequest) {
    return Menu.builder()
        .id(uuidHelper.randomUUID())
        .name(menuRequest.getName())
        .description(menuRequest.getDescription())
        .imageUrl(null)
        .mealplanId(menuRequest.getMealplanId())
        .calorie(menuRequest.getCalorie())
        .recipes(menuRequest.getRecipes())
        .softDelete(false)
        .build();
  }


  private MenuAdminResponse toMenuAdminResponse(Tuple3<Menu, Mealplan, List<IngredientResponse>> tuple3) {
    MealplanResponse mealplanResponse = new MealplanResponse(tuple3.getT2());
    return MenuAdminResponse.builder()
        .id(tuple3.getT1().getId())
        .name(tuple3.getT1().getName())
        .description(tuple3.getT1().getDescription())
        .imageUrl(tuple3.getT1().getImageUrl())
        .calorie(tuple3.getT1().getCalorie())
        .mealplan(mealplanResponse)
        .recipes(tuple3.getT3())
        .build();
  }

  private Mono<IngredientResponse> constructNValidateIngredientResponse(Ingredient ingredient) {
    return stockRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(ingredient.getStockId()))
        .switchIfEmpty(Mono.error(new NotFoundException("STOCK")))
        .map(StockResponse::new)
        .map(stockResponse -> IngredientResponse.builder()
            .stock(stockResponse)
            .quantity(ingredient.getQuantity())
            .build());
  }



}
