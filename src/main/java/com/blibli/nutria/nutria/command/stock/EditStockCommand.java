package com.blibli.nutria.nutria.command.stock;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.stock.EditStockRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import reactor.util.function.Tuple2;

public interface EditStockCommand extends Command<Tuple2<EditStockRequest,String>, BaseCommandResponse<StockResponse>> {
}
