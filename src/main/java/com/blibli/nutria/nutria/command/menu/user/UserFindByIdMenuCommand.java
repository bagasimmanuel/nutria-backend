package com.blibli.nutria.nutria.command.menu.user;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuResponse;

public interface UserFindByIdMenuCommand extends Command<String, BaseCommandResponse<MenuResponse>> {
}
