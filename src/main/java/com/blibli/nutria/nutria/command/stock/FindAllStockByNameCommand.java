package com.blibli.nutria.nutria.command.stock;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import reactor.util.function.Tuple2;

public interface FindAllStockByNameCommand extends Command<Tuple2<String,Integer>, BasePaginatedCommandResponse<StockResponse>> {
}
