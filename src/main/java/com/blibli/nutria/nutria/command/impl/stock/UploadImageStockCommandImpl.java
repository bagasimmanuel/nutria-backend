package com.blibli.nutria.nutria.command.impl.stock;

import com.blibli.nutria.nutria.command.stock.UploadImageStockCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.StorageHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.repository.StockRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
@AllArgsConstructor
public class UploadImageStockCommandImpl implements UploadImageStockCommand {

  private StockRepository stockRepository;
  private StorageHelper storageHelper;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<StockResponse>> execute(Mono<Tuple2<String, FilePart>> request) {
    return request.flatMap(this::updateStockData)
        .flatMap(stockRepository::save)
        .map(stock -> new BaseCommandResponse<>(HttpStatus.OK,toStockResponse(stock)));
  }


  private Mono<Stock> updateStockData(Tuple2<String,FilePart> tuple2){
    return stockRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(tuple2.getT1()))
        .switchIfEmpty(Mono.error(new NotFoundException("STOCK")))
        .flatMap(stock ->storageHelper.storeFile(tuple2.getT2(), StorageHelper.STORAGE_TYPE.STOCK_IMAGE,uuidHelper.fromString(tuple2.getT1())))
        .flatMap(path -> setStockImageUrl(path,tuple2.getT1()));
  }

  private Mono<Stock> setStockImageUrl(String path, String stockId){
    return stockRepository.findById(uuidHelper.fromString(stockId))
        .map(stock -> {
          stock.setImageUrl(path);
          return stock;
        });
  }

  private StockResponse toStockResponse(Stock stock){
    return StockResponse.builder()
        .id(stock.getId())
        .quantity(stock.getQuantity())
        .name(stock.getName())
        .imageUrl(stock.getImageUrl())
        .description(stock.getDescription())
        .build();
  }
}
