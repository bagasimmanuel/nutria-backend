package com.blibli.nutria.nutria.command.impl.menu;

import com.blibli.nutria.nutria.command.menu.FindAllByNameMenuCommand;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
@RequiredArgsConstructor
public class FindAllByNameMenuCommandImpl implements FindAllByNameMenuCommand {

  private final UUIDHelper uuidHelper;
  private final ConstructorHelper constructorHelper;
  private final MealplanRepository mealplanRepository;
  private final MenuRepository menuRepository;
  private final StockRepository stockRepository;


  @Value("${itemsPerPage:#{10}}")
  private int ITEMS_PER_PAGE;

  @Override
  public Mono<BasePaginatedCommandResponse<MenuAdminResponse>> execute(Mono<Tuple2<String, Integer>> request) {
    return request.flatMap(this::handle);
  }

  public Mono<BasePaginatedCommandResponse<MenuAdminResponse>> handle(Tuple2<String,Integer> request){
    Pageable pageable = PageRequest.of(request.getT2(),ITEMS_PER_PAGE, Sort.by("name").ascending());
    return menuRepository.findAllByNameLikeAndSoftDeleteFalse(request.getT1(),pageable)
        .flatMapSequential(constructorHelper::constructMenuAdminResponse)
        .collectList()
        .zipWith(menuRepository.countAllByNameLikeAndSoftDeleteFalse(request.getT1()))
        .map(tuple2 -> new BasePaginatedCommandResponse<>(HttpStatus.OK,
            tuple2.getT1(),
            BasePaginatedCommandResponse.Pagination.builder()
                .currentPage(request.getT2())
                .itemsPerPage(ITEMS_PER_PAGE)
                .totalItems(tuple2.getT2())
                .build()));// Ini harus dijadikan Flux<MenuAdminRepsonse>, zipWithPage,Count
  }

}
