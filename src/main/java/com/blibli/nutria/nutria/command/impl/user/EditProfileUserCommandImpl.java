package com.blibli.nutria.nutria.command.impl.user;

import com.blibli.nutria.nutria.command.user.EditProfileUserCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.user.EditProfileUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.EditProfileUserResponse;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class EditProfileUserCommandImpl implements EditProfileUserCommand {

  private UserRepository userRepository;
  private MealplanRepository mealplanRepository;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<EditProfileUserResponse>> execute(Mono<EditProfileUserRequest> request) {
    return request
        .flatMap(this::constructUser)
        .flatMap(userRepository::save)
        .flatMap(this::toEditProfileUserResponse)
        .map(user -> new BaseCommandResponse<>(HttpStatus.OK, user));
  }

  private Mono<User> constructUser(EditProfileUserRequest request) {
    return getCurrentUser()
        .flatMap(user -> updateUserProfile(user, request))
        .flatMap(this::setUserCalorieRecom);
  }

  private Mono<User> updateUserProfile(User user, EditProfileUserRequest request) {
    // Rasae isa dijadikan 1 deh ini
    if (request.getFullName() != null) {
      user.setFullName(request.getFullName());
    }
    if (request.getAge() != null) {
      user.setAge(request.getAge());
    }
    if (request.getHeight() != null) {
      user.setHeight(request.getHeight());
    }
    if (request.getWeight() != null) {
      user.setWeight(request.getWeight());
    }
    if (request.getActivityLevel() != null) {
      user.setActivityLevel(request.getActivityLevel());
    }
    if (request.getLocation() != null) {
      user.setLocation(request.getLocation());
    }
    if (request.getGender() != null) {
      user.setGender(request.getGender().equals(User.Gender.MALE) ? User.Gender.MALE : User.Gender.FEMALE);
    }
    if (request.getMealplanId() != null) {
      user.setMealplanId(request.getMealplanId());
    }
    return Mono.just(user);
  }

  private Mono<User> setUserCalorieRecom(User user) {
    if(user.getMealplanId() != null) {
      return mealplanRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(user.getMealplanId()))
          .switchIfEmpty(Mono.error(new NotFoundException("MEALPLAN")))
          .map(mealplan -> {
            if (mealplan.getName().equals("weightloss")) {
              int calorieRecom = countTDEE(user);
              user.setCalorieRecom( calorieRecom <= 500 ? 0 : calorieRecom - 500);
            } else {
              user.setCalorieRecom(countTDEE(user) + 500);
            }
            return user;
          });
    }else{
      return Mono.just(user);
    }
  }

  private Mono<User> getCurrentUser() {
    return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(x -> x.getPrincipal().toString())
        .flatMap(userRepository::findFirstByEmail);
  }



  private Mono<EditProfileUserResponse> toEditProfileUserResponse(User user) {
    if(user.getMealplanId() != null){
      return mealplanRepository.findById(uuidHelper.fromString(user.getMealplanId()))
          .map(mealplan -> EditProfileUserResponse.builder()
              .activityLevel(user.getActivityLevel())
              .age(user.getAge())
              .calorieRecom(user.getCalorieRecom())
              .email(user.getEmail())
              .fullName(user.getFullName())
              .gender(user.getGender())
              .weight(user.getWeight())
              .height(user.getHeight())
              .id(user.getId())
              .location(user.getLocation())
              .mealplan(mealplan)
              .build());
    }else{
      return Mono.just(new EditProfileUserResponse(user));
    }
  }

  private int countTDEE(User user) {
    if (user.getMealplanId() != null && user.getAge() != 0 && user.getHeight() != 0 && user.getWeight() != 0
        && user.getActivityLevel() != null && user.getGender() != null) {
      int bmr = 0;
      int tdee = 0;
      if (user.getGender().equals(User.Gender.MALE)) {
        bmr = (int) (66 + (13.8 * user.getWeight()) + (5 * user.getHeight()) - (6.8 * user.getAge()));
      } else {
        bmr = (int) (655 + (9.6 * user.getWeight()) + (1.8 * user.getAge()) - (4.7 * user.getAge()));
      }
      switch (user.getActivityLevel()) {
        case SEDENTARY: {
          tdee = (int) (bmr * 1.2);
          break;
        }
        case LIGHT:
          tdee = (int) (bmr * 1.375);
          break;
        case ACTIVE:
          tdee = (int) (bmr * 1.55);
          break;
        case VERY:
          tdee = (int) (bmr * 1.725);
          break;
        case EXTREME:
          tdee = (int) (bmr * 1.95);
          break;
        default:
          throw new IllegalStateException("Unexpected value: " + user.getActivityLevel());
      }
      return tdee;
    } else {
      return 0;
    }
  }
}
