package com.blibli.nutria.nutria.command.user;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.user.VerifyEmailRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;

public interface VerifyEmailCommand extends Command<VerifyEmailRequest, BaseCommandResponse<Void>> {


}
