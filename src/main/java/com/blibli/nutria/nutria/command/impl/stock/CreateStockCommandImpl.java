package com.blibli.nutria.nutria.command.impl.stock;

import com.blibli.nutria.nutria.command.stock.CreateStockCommand;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.request.stock.CreateStockRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.repository.StockRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class CreateStockCommandImpl implements CreateStockCommand {

  private StockRepository stockRepository;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<StockResponse>> execute(Mono<CreateStockRequest> request) {
    return request.map(this::createStock)
        .flatMap(stockRepository::save)
        .map(this::toCreateStockResponse)
        .map(stockResponse ->  new BaseCommandResponse<>(HttpStatus.OK,stockResponse));
  }

  private Stock createStock(CreateStockRequest stockRequest) {
    return Stock.builder()
        .id(uuidHelper.randomUUID())
        .name(stockRequest.getName())
        .quantity(stockRequest.getQuantity())
        .description(stockRequest.getDescription())
        .imageUrl(null)
        .softDelete(false)
        .build();
  }

  private StockResponse toCreateStockResponse(Stock stock){
    return StockResponse.builder()
        .id(stock.getId())
        .description(stock.getDescription())
        .imageUrl(stock.getImageUrl())
        .name(stock.getName())
        .quantity(stock.getQuantity())
        .build();
  }
}
