package com.blibli.nutria.nutria.command.impl.user;

import com.blibli.nutria.nutria.command.user.ChangePasswordUserCommand;
import com.blibli.nutria.nutria.exception.BadCredentialsException;
import com.blibli.nutria.nutria.model.command.request.user.ChangePasswordRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class ChangePasswordUserCommandImpl implements ChangePasswordUserCommand {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  @Override
  public Mono<BaseCommandResponse<Void>> execute(Mono<ChangePasswordRequest> request) {
    Mono<User> userMono = ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getPrincipal)
        .flatMap(email -> userRepository.findFirstByEmail(email.toString()))
        .zipWith(request)
        .flatMap(tuple -> passwordMatches(tuple.getT1(), tuple.getT2()))
        .flatMap(userRepository::save);
    // disini perlu ditambahi on Error buat Bad Credentials ga se?

    return userMono.map(user -> new BaseCommandResponse<>(HttpStatus.OK,null));
  }

  private Mono<User> passwordMatches(User user, ChangePasswordRequest t2) {
    if (passwordEncoder.matches(t2.getOldPassword(),user.getPassword())) {
      User updatedUser = new User();
      BeanUtils.copyProperties(user, updatedUser);
      updatedUser.setPassword(passwordEncoder.encode(t2.getNewPassword()));
      return Mono.just(updatedUser);
    }
    return Mono.error(new BadCredentialsException());
  }
}
