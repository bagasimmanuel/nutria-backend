package com.blibli.nutria.nutria.command.stock;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import org.springframework.http.codec.multipart.FilePart;
import reactor.util.function.Tuple2;

public interface UploadImageStockCommand extends Command<Tuple2<String, FilePart>, BaseCommandResponse<StockResponse>> {
}
