package com.blibli.nutria.nutria.command;

import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import reactor.core.publisher.Mono;

public interface CommandInvoker {

  <REQUEST, RESPONSE extends BaseCommandResponse<?>> Mono<RESPONSE> invokeCommand(Class<? extends Command<REQUEST, RESPONSE>> commandClass,
      Mono<REQUEST> request);

}
