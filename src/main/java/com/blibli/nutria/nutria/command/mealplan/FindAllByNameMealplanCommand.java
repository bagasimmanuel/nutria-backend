package com.blibli.nutria.nutria.command.mealplan;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.GetMealplanResponse;
import org.springframework.stereotype.Component;
import reactor.util.function.Tuple2;

@Component
public interface FindAllByNameMealplanCommand extends Command<Tuple2<String,Integer>, BasePaginatedCommandResponse<GetMealplanResponse>> {
}
