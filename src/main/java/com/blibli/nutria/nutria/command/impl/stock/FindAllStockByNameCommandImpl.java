package com.blibli.nutria.nutria.command.impl.stock;

import com.blibli.nutria.nutria.command.stock.FindAllStockByNameCommand;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class FindAllStockByNameCommandImpl implements FindAllStockByNameCommand {

  private final StockRepository stockRepository;

  @Value("${itemsPerPage:#{10}}")
  private int ITEMS_PER_PAGE;

  @Override
  public Mono<BasePaginatedCommandResponse<StockResponse>> execute(Mono<Tuple2<String, Integer>> request) {

    Mono<Tuple2<String,Integer>> cached = request.cache();

    return request.flatMapMany(tuple -> {
          Pageable pageable = PageRequest.of(tuple.getT2(), ITEMS_PER_PAGE,Sort.by("name").ascending());
          return stockRepository.findAllByNameLikeAndSoftDeleteFalse(tuple.getT1(), pageable); // Get All Mealplan by Name
        }).collectList()
        .zipWith(cached) // Zip with request to get Total Items
        .flatMap(tuple2 -> {
          Mono<Long> totalItems = stockRepository.countAllByNameLikeAndSoftDeleteFalse(tuple2.getT2().getT1());
          return totalItems.map(longTotalItem -> Tuples.of(tuple2.getT1(),longTotalItem,tuple2.getT2().getT2()) // return tuplesOfThree List<Mealplan>,TotalItem,CurrentPage
          );
        }).map(tuple3 -> new BasePaginatedCommandResponse<>(
            HttpStatus.OK,
            toStockResponse(tuple3.getT1()),
            BasePaginatedCommandResponse.Pagination.builder().currentPage(tuple3.getT3()).totalItems(tuple3.getT2()).itemsPerPage(ITEMS_PER_PAGE).build()));
  }

  private List<StockResponse> toStockResponse(List<Stock> stocks) {
    return stocks.stream().map(stock ->
            StockResponse.builder()
                .id(stock.getId())
                .name(stock.getName())
                .description(stock.getDescription())
                .quantity(stock.getQuantity())
                .imageUrl(stock.getImageUrl())
                .build())
        .collect(Collectors.toList());
  }


}
