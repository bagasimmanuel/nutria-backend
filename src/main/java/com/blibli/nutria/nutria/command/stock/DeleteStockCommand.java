package com.blibli.nutria.nutria.command.stock;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;

public interface DeleteStockCommand extends Command<String, BaseCommandResponse<Void>> {
}
