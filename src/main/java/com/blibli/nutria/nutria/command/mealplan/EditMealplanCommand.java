package com.blibli.nutria.nutria.command.mealplan;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.mealplan.EditMealplanRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.EditMealplanResponse;
import reactor.util.function.Tuple2;

public interface EditMealplanCommand extends Command<Tuple2<EditMealplanRequest,String>, BaseCommandResponse<EditMealplanResponse>> {
}
