package com.blibli.nutria.nutria.command.impl.subscription;

import com.blibli.nutria.nutria.command.subscription.FindByIdSubscriptionCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.exception.UnauthorizedException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class FindByIdSubscriptionCommandImpl implements FindByIdSubscriptionCommand {

  private SubscriptionRepository subscriptionRepository;
  private MenuRepository menuRepository;
  private UserRepository userRepository;
  private UUIDHelper uuidHelper;
  private ConstructorHelper constructorHelper;

  @Override
  public Mono<BaseCommandResponse<SubscriptionResponse>> execute(Mono<String> request) {

    return request.flatMap(subscriptionId -> subscriptionRepository.findFirstById(uuidHelper.fromString(subscriptionId)))
        .switchIfEmpty(Mono.error(new NotFoundException("SUBSCRIPTION")))
        .flatMap(this::validateUser)
        .flatMap(constructorHelper::constructSubscriptionResponse)
        .map(subscriptionResponse -> new BaseCommandResponse<>(HttpStatus.OK, subscriptionResponse));
  }

  private Mono<Subscription> validateUser(Subscription subscription){
    Mono<User> userMono = ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(x -> x.getPrincipal().toString())
        .flatMap(userRepository::findFirstByEmail);

    return userMono.flatMap(user -> {
      if(user.getRoles().contains("ADMIN") || user.getId().toString().equals(subscription.getUserId())){
        return Mono.just(subscription);
      }else{
        return Mono.error(new UnauthorizedException());
      }
    });
  }

}
