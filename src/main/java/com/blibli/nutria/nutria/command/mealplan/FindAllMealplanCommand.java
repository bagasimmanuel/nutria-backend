package com.blibli.nutria.nutria.command.mealplan;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.GetMealplanResponse;
import org.springframework.stereotype.Component;

@Component
public interface FindAllMealplanCommand extends Command<Integer, BasePaginatedCommandResponse<GetMealplanResponse>> {
}
