package com.blibli.nutria.nutria.command.impl.menu;

import com.blibli.nutria.nutria.command.menu.FindByIdMenuCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class FindByIdMenuCommandImpl implements FindByIdMenuCommand {

  private UUIDHelper uuidHelper;
  private ConstructorHelper constructorHelper;
  private MenuRepository menuRepository;
  private StockRepository stockRepository;
  private MealplanRepository mealplanRepository;


  @Override
  public Mono<BaseCommandResponse<MenuAdminResponse>> execute(Mono<String> request) {
    return request.flatMap(menuId -> menuRepository.findFirstById(uuidHelper.fromString(menuId)))
        .switchIfEmpty(Mono.error(new NotFoundException("MENU")))
        .flatMap(constructorHelper::constructMenuAdminResponse)
        .map(menuAdminResponse -> new BaseCommandResponse<>(HttpStatus.OK,menuAdminResponse));
  }

}
