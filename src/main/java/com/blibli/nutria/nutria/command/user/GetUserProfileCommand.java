package com.blibli.nutria.nutria.command.user;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.UserProfileResponse;

public interface GetUserProfileCommand extends Command<Void, BaseCommandResponse<UserProfileResponse>> {
}
