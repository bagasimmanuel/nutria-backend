package com.blibli.nutria.nutria.command.subscription.item;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionItemRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import reactor.util.function.Tuple2;

public interface EditSubscriptionItemCommand extends Command<Tuple2<EditSubscriptionItemRequest,String>, BaseCommandResponse<SubscriptionResponse>> {
}
