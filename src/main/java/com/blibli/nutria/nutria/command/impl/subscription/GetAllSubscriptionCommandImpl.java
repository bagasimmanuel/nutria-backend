package com.blibli.nutria.nutria.command.impl.subscription;

import com.blibli.nutria.nutria.command.subscription.GetAllSubcriptionCommand;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionCriteria;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Component
@RequiredArgsConstructor
public class GetAllSubscriptionCommandImpl implements GetAllSubcriptionCommand {

  private final UUIDHelper uuidHelper;
  private final ConstructorHelper constructorHelper;
  private final UserRepository userRepository;
  private final SubscriptionRepository subscriptionRepository;
  private final MenuRepository menuRepository;

  @Value("${itemsPerPage:#{10}}")
  private int ITEMS_PER_PAGE;

  @Override
  public Mono<BasePaginatedCommandResponse<SubscriptionResponse>> execute(Mono<Tuple2<SubscriptionCriteria, Integer>> request) {
    return request.flatMap(this::updateSubscriptionCriteria)
        .flatMap(this::handler);
  }

  public Mono<Tuple2<SubscriptionCriteria, Integer>> updateSubscriptionCriteria(Tuple2<SubscriptionCriteria, Integer> tuple2) {

    return ReactiveSecurityContextHolder.getContext()
        .map(SecurityContext::getAuthentication)
        .map(x -> x.getPrincipal().toString())
        .flatMap(userRepository::findFirstByEmail)
        .map(user -> {
          if (!user.getRoles().contains("ADMIN")) {
            tuple2.getT1().setUserId(user.getId().toString());
          }
          return tuple2;
        });
  }

  private Mono<BasePaginatedCommandResponse<SubscriptionResponse>> handler(Tuple2<SubscriptionCriteria, Integer> request) {
    System.out.println(request.getT1());
    Pageable pageRequest = PageRequest.of(request.getT2(), ITEMS_PER_PAGE);
    return subscriptionRepository.findAllWithParam(request.getT1(), pageRequest)
        .flatMap(constructorHelper::constructSubscriptionResponse).collectList()
        .zipWith(subscriptionRepository.countAllWithParam(request.getT1()))
        .map(tuple2 -> new BasePaginatedCommandResponse<>(HttpStatus.OK, tuple2.getT1(), BasePaginatedCommandResponse.Pagination.builder()
            .itemsPerPage(ITEMS_PER_PAGE)
            .totalItems(tuple2.getT2())
            .currentPage(request.getT2()).build()));
  }



}
