package com.blibli.nutria.nutria.command.banner;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;

public interface DeleteBannerImageCommand extends Command<String, BaseCommandResponse<Void>> {
}
