package com.blibli.nutria.nutria.command.menu;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import reactor.util.function.Tuple2;

public interface FindAllByNameMenuCommand extends Command<Tuple2<String,Integer>, BasePaginatedCommandResponse<MenuAdminResponse>> {
}
