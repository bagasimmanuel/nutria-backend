package com.blibli.nutria.nutria.command.subscription;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionPaymentStatusRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import reactor.util.function.Tuple2;

public interface EditSubscriptionPaymentStatusCommand extends Command<Tuple2<String, EditSubscriptionPaymentStatusRequest>, BaseCommandResponse<SubscriptionResponse>> {
}
