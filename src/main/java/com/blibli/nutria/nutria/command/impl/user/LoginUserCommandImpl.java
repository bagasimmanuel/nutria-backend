package com.blibli.nutria.nutria.command.impl.user;

import com.blibli.nutria.nutria.command.user.LoginUserCommand;
import com.blibli.nutria.nutria.exception.BadCredentialsException;
import com.blibli.nutria.nutria.exception.UnverifiedEmailException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.helper.mail.EmailHelper;
import com.blibli.nutria.nutria.model.command.request.user.LoginUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.LoginUserResponse;
import com.blibli.nutria.nutria.model.entity.RefreshToken;
import com.blibli.nutria.nutria.model.entity.User;
import com.blibli.nutria.nutria.repository.RefreshTokenRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.nio.file.Files;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class LoginUserCommandImpl implements LoginUserCommand {

  private ReactiveAuthenticationManager reactiveAuthenticationManager;
  private RefreshTokenRepository refreshTokenRepository;
  private UserRepository userRepository;
  private UUIDHelper uuidHelper;
  private EmailHelper emailHelper;
  private PasswordEncoder passwordEncoder;
  private ReactiveRedisTemplate<UUID, String> redisTemplate;

  private ReactiveRedisTemplate<String, String> otpRedisTemplate;

  @Override
  public Mono<BaseCommandResponse<LoginUserResponse>> execute(Mono<LoginUserRequest> request) {

    return request
        .flatMap(this::checkIfVerified)
        // Check dlu email verification, mending juga pakai OTP
        .flatMap(loginUserRequest -> reactiveAuthenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUserRequest.getEmail(),
            loginUserRequest.getPassword())))
        .flatMap(this::handler)
        .map(loginUserResponse -> new BaseCommandResponse<>(HttpStatus.OK, loginUserResponse));
  }

  private Mono<LoginUserRequest> checkIfVerified(LoginUserRequest loginUserRequest) {
    return userRepository.findFirstByEmail(loginUserRequest.getEmail())
        .switchIfEmpty(Mono.error(new BadCredentialsException()))
        .flatMap(user -> {
          if (passwordEncoder.matches(loginUserRequest.getPassword(), user.getPassword())) {
            return Mono.just(user);
          } else {
            return Mono.error(new BadCredentialsException());
          }
        }).flatMap(user -> {
          if (user.isVerified()) {
            return Mono.just(loginUserRequest);
          } else {
            return processUnverifiedUser(user);
          }
        });


  }

  private Mono<LoginUserRequest> processUnverifiedUser(User user) {
    return otpRedisTemplate.opsForValue().get(user.getEmail())
        .switchIfEmpty(resendVerificationEmail(user))
        .then(Mono.error(new UnverifiedEmailException()));
  }

  private Mono<String> resendVerificationEmail(User user) {

//    String otp = new DecimalFormat("000000").format(new SecureRandom().nextInt(999999));
    String otp = "111111";
    return otpRedisTemplate.opsForValue().set(user.getEmail(), otp, Duration.ofSeconds(300))
        .map(bool -> {
          Mono.fromCallable(() -> {
            HashMap<String, String> replacements = new HashMap<>();
            replacements.put("fullName", user.getFullName());
            replacements.put("OTP", otp);

            String template = String.join("\n", Files.readAllLines(
                new ClassPathResource("mail-template/email-verification.txt").getFile().toPath()));

            for (Map.Entry<String, String> entry : replacements.entrySet()) {
              template = template.replaceAll("\\{" + entry.getKey() + "\\}", entry.getValue());
            }
            emailHelper.sendEmail(user.getEmail(), "Nutria - Email Verification", template);
            return "true";
          }).subscribe();
          return "true";
        }).then(Mono.error(new UnverifiedEmailException()));
  }


  private Mono<LoginUserResponse> handler(Authentication authentication) {
    UUID accessToken = uuidHelper.randomUUID();
    RefreshToken refreshToken = RefreshToken.builder()
        .id(uuidHelper.randomUUID())
        .ttl(new Date())
        .refreshToken(uuidHelper.randomUUID())
        .accessToken(accessToken)
        .email(((UserDetails) authentication.getPrincipal()).getUsername())
        .build();

    return refreshTokenRepository.save(refreshToken)
        .zipWith(saveAccessToken(authentication, accessToken))
        .map(tuple -> constructLoginResponse(tuple, authentication));

  }

  private Mono<UUID> saveAccessToken(Authentication authentication, UUID accessToken) {

    return redisTemplate.opsForValue()
        .set(accessToken, ((UserDetails) authentication.getPrincipal()).getUsername(), Duration.ofHours(1))
        .thenReturn(accessToken);
  }

  private LoginUserResponse constructLoginResponse(Tuple2<RefreshToken, UUID> tuple2, Authentication authentication) {
    return LoginUserResponse.builder()
        .accessToken(tuple2.getT2())
        .refreshToken(tuple2.getT1().getRefreshToken())
        .roles(authentication.getAuthorities().stream().map(Object::toString).collect(Collectors.toList())).build();
  }

}
