package com.blibli.nutria.nutria.command.user;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.user.RegisterUserRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.RegisterUserResponse;

public interface RegisterUserCommand extends Command<RegisterUserRequest, BaseCommandResponse<RegisterUserResponse>> {
}
