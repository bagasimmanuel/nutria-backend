package com.blibli.nutria.nutria.command.impl.banner;

import com.blibli.nutria.nutria.command.banner.DeleteBannerImageCommand;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.repository.BannerRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class DeleteBannerImageCommandImpl implements DeleteBannerImageCommand {

  private BannerRepository bannerRepository;
  private UUIDHelper uuidHelper;

  @Override
  public Mono<BaseCommandResponse<Void>> execute(Mono<String> request) {
    return request.flatMap(this::handler);
  }

  private Mono<BaseCommandResponse<Void>> handler(String bannerId) {
    return bannerRepository.findFirstById(uuidHelper.fromString(bannerId))
        .map(banner -> {
          banner.setSoftDelete(true);
          return banner;
        }).map(bannerRepository::save)
        .thenReturn(new BaseCommandResponse<>(HttpStatus.OK, null));
  }
}
