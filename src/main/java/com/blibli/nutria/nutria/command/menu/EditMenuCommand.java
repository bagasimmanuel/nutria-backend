package com.blibli.nutria.nutria.command.menu;

import com.blibli.nutria.nutria.command.Command;
import com.blibli.nutria.nutria.model.command.request.menu.EditMenuRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import reactor.util.function.Tuple2;

public interface EditMenuCommand extends Command<Tuple2<EditMenuRequest,String>, BaseCommandResponse<MenuAdminResponse>> {
}
