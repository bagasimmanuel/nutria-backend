package com.blibli.nutria.nutria.command.impl.stock;

import com.blibli.nutria.nutria.command.stock.FindAllStockCommand;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class FindAllStockCommandImpl implements FindAllStockCommand {

  private final StockRepository stockRepository;

  @Value("${itemsPerPage:#{10}}")
  private int ITEMS_PER_PAGE;

  @Override
  public Mono<BasePaginatedCommandResponse<StockResponse>> execute(Mono<Integer> request) {

    Mono<Integer> pageNum = request.cache();
    return request.flatMapMany(page -> stockRepository.findAllByIdNotNullAndSoftDeleteFalse(PageRequest.of(page, ITEMS_PER_PAGE,Sort.by("name").ascending())))
        .collectList()
        .zipWith(stockRepository.countAllByIdNotNullAndSoftDeleteFalse())
        .zipWith(pageNum)
        .map(tupleOfTuples -> Tuples.of(tupleOfTuples.getT1().getT1(), tupleOfTuples.getT1().getT2(), tupleOfTuples.getT2()))
        .map(tuple -> new BasePaginatedCommandResponse<>(HttpStatus.OK,
            toStockResponse(tuple.getT1()),
            BasePaginatedCommandResponse.Pagination.builder()
                .itemsPerPage(ITEMS_PER_PAGE)
                .totalItems(tuple.getT2())
                .currentPage(tuple.getT3()).build()));

  }

  private List<StockResponse> toStockResponse(List<Stock> stocks) {
    return stocks.stream().map(stock ->
        StockResponse.builder()
            .id(stock.getId())
            .name(stock.getName())
            .description(stock.getDescription())
            .quantity(stock.getQuantity())
            .imageUrl(stock.getImageUrl())
            .build())
        .collect(Collectors.toList());
  }
}
