package com.blibli.nutria.nutria.command.impl.stock;

import com.blibli.nutria.nutria.command.stock.FindByIdStockCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.entity.Stock;
import com.blibli.nutria.nutria.repository.StockRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
@Component
@AllArgsConstructor
public class FindByIdStockCommandImpl implements FindByIdStockCommand {

  private StockRepository stockRepository;
  private UUIDHelper uuidHelper;
  @Override
  public Mono<BaseCommandResponse<StockResponse>> execute(Mono<String> request) {
    return request.flatMap(stringId -> stockRepository.findFirstByIdAndSoftDeleteFalse(uuidHelper.fromString(stringId)))
        .switchIfEmpty(Mono.error(new NotFoundException("STOCK")))
        .map(stock -> new BaseCommandResponse<>(HttpStatus.OK,toStockResponse(stock)));
  }

  private StockResponse toStockResponse(Stock stock){
    return StockResponse.builder()
        .id(stock.getId())
        .quantity(stock.getQuantity())
        .name(stock.getName())
        .imageUrl(stock.getImageUrl())
        .description(stock.getDescription())
        .build();
  }
}
