package com.blibli.nutria.nutria.command.impl.menu;

import com.blibli.nutria.nutria.command.menu.DeleteMenuCommand;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.helper.StorageHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class DeleteMenuCommandImpl implements DeleteMenuCommand {

  private UUIDHelper uuidHelper;
  private StorageHelper storageHelper;
  private MenuRepository menuRepository;

  @Override
  public Mono<BaseCommandResponse<Void>> execute(Mono<String> request) {
    return request.flatMap(this::handler);
  }

  private Mono<BaseCommandResponse<Void>> handler(String menuId) {
    return menuRepository.findFirstById(uuidHelper.fromString(menuId))
        .switchIfEmpty(Mono.error(new NotFoundException("MENU")))
        .map(menu -> {
          menu.setSoftDelete(true);
          return menu;
        })
        .flatMap(menuRepository::save)
//        .then(storageHelper.deleteFile(StorageHelper.STORAGE_TYPE.MENU_IMAGE, uuidHelper.fromString(menuId))
            .thenReturn(new BaseCommandResponse<>(HttpStatus.OK, null));
  }
}
