package com.blibli.nutria.nutria.command.impl.menu;

import com.blibli.nutria.nutria.command.menu.FindAllMenuCommand;
import com.blibli.nutria.nutria.helper.ConstructorHelper;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class FindAllMenuCommandImpl implements FindAllMenuCommand {

  private final MenuRepository menuRepository;
  private final MealplanRepository mealplanRepository;
  private final StockRepository stockRepository;
  private final UUIDHelper uuidHelper;
  private final ConstructorHelper constructorHelper;

  @Value("${itemsPerPage:#{10}}")
  private int ITEMS_PER_PAGE;

  @Override
  public Mono<BasePaginatedCommandResponse<MenuAdminResponse>> execute(Mono<Integer> request) {
    return request.flatMap(this::handle);
  }

  private Mono<BasePaginatedCommandResponse<MenuAdminResponse>> handle(Integer page) {
    Pageable pageable = PageRequest.of(page,ITEMS_PER_PAGE, Sort.by("name").ascending());
    return menuRepository.findAllByIdIsNotNullAndSoftDeleteFalse(pageable)
        .flatMapSequential(constructorHelper::constructMenuAdminResponse)
        .collectList()
        .zipWith(menuRepository.countAllByIdNotNullAndSoftDeleteFalse())
        .map(tuple2 -> new BasePaginatedCommandResponse<>(HttpStatus.OK,
            tuple2.getT1(),
            BasePaginatedCommandResponse.Pagination.builder()
                .currentPage(page)
                .itemsPerPage(ITEMS_PER_PAGE)
                .totalItems(tuple2.getT2())
                .build()));// Ini harus dijadikan Flux<MenuAdminRepsonse>, zipWithPage,Count
  }

}
