package com.blibli.nutria.nutria.exception;

public class BadCredentialsException extends BaseException {
  public BadCredentialsException() {
    super("BAD_CREDENTIALS");
  }
}
