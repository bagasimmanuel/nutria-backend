package com.blibli.nutria.nutria.exception;

public class InvalidDateException extends BaseException{

  public InvalidDateException() {
    super("Invalid Date");
  }
}
