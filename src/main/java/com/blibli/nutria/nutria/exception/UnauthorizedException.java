package com.blibli.nutria.nutria.exception;

public class UnauthorizedException extends BaseException{

  public UnauthorizedException() {
    super("UNAUTHORIZED");
  }

}
