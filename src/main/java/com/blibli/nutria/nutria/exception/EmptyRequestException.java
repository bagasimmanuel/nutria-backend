package com.blibli.nutria.nutria.exception;

public class EmptyRequestException extends BaseException{
  public EmptyRequestException() {
    super("EMPTY_BODY");
  }
}
