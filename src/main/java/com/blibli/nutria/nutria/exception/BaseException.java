package com.blibli.nutria.nutria.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BaseException extends Throwable {

  private String error;

}
