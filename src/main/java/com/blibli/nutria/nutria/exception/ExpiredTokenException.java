package com.blibli.nutria.nutria.exception;

public class ExpiredTokenException extends BaseException{
  public ExpiredTokenException(String error) {
    super(error + " Token is Expired");
  }
}
