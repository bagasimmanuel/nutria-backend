package com.blibli.nutria.nutria.exception;

public class DuplicateKeyException extends BaseException {

  public DuplicateKeyException() {
    super("DUPLICATE_KEY");
  }

}
