package com.blibli.nutria.nutria.exception;

public class BadRequestException extends BaseException{
  public BadRequestException(String error) {
    super(error);
  }
}
