package com.blibli.nutria.nutria.exception;

public class UnverifiedEmailException extends BaseException {

  public UnverifiedEmailException() {
    super("EMAIL_VERIFY");
  }

}
