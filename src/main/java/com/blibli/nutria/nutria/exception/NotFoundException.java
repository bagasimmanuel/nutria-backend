package com.blibli.nutria.nutria.exception;

public class NotFoundException extends BaseException{

  public NotFoundException(String className) {
    super(className + "_NOT_FOUND");
  }
}
