package com.blibli.nutria.nutria.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.UUID;

@Configuration
public class RedisConfiguration {

  @Bean
  public ReactiveRedisTemplate<UUID,String> sessionRedisTemplate(ReactiveRedisConnectionFactory factory){
    RedisSerializationContext<UUID, String>
        context = RedisSerializationContext.<UUID, String>newSerializationContext(new StringRedisSerializer())
        .key(new GenericToStringSerializer<>(UUID.class)) 
        .build();
    return new ReactiveRedisTemplate<>(factory, context);
  }

  @Bean
  @Qualifier("otpRedis")
  public ReactiveRedisTemplate<String,String> otpRedisTemplate(ReactiveRedisConnectionFactory factory){
    StringRedisSerializer keySerializer = new StringRedisSerializer();
    RedisSerializationContext<String,String>
        context = RedisSerializationContext.<String, String>newSerializationContext(new StringRedisSerializer())
        .key(keySerializer)
        .build();

    return new ReactiveRedisTemplate<>(factory,context);
  }


  @Bean
  @Qualifier("springRedisTest")
  public ReactiveRedisTemplate<String,String> sessionRedisTemplateTest(ReactiveRedisConnectionFactory factory){
    StringRedisSerializer keySerializer = new StringRedisSerializer();
    RedisSerializationContext<String, String>
        context = RedisSerializationContext.<String, String>newSerializationContext(new StringRedisSerializer())
        .key(keySerializer)
        .build();
    return new ReactiveRedisTemplate<>(factory, context);
  }

  @Bean
  @Qualifier("otpRedisTest")
  public ReactiveRedisTemplate<String,String> otpRedisTemplateTest(ReactiveRedisConnectionFactory factory){
    StringRedisSerializer keySerializer = new StringRedisSerializer();
    RedisSerializationContext<String,String>
        context = RedisSerializationContext.<String, String>newSerializationContext(new StringRedisSerializer())
        .key(keySerializer)
        .build();

    return new ReactiveRedisTemplate<>(factory,context);
  }

}
