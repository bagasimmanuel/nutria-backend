package com.blibli.nutria.nutria.configuration;

import com.blibli.nutria.nutria.exception.UnauthorizedException;
import com.blibli.nutria.nutria.filter.ExceptionFilter;
import com.blibli.nutria.nutria.security.ReactiveUserDetailsServiceImpl;
import com.blibli.nutria.nutria.security.SessionAuthenticationManager;
import com.blibli.nutria.nutria.security.SessionSecurityContextRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Mono;

@Configuration
@EnableWebFluxSecurity
@AllArgsConstructor
public class SecurityConfiguration {

  private ReactiveUserDetailsServiceImpl reactiveUserDetailsService;
  private SessionAuthenticationManager sessionAuthenticationManager;
  private SessionSecurityContextRepository sessionSecurityContextRepository;
  private ExceptionFilter exceptionFilter;

  @Bean
  public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
    return http
        .csrf().disable()
        .cors().disable()
        .formLogin().disable()
        .authenticationManager(sessionAuthenticationManager)
        .securityContextRepository(sessionSecurityContextRepository)
        .authorizeExchange()
        .and()
        .authorizeExchange()
        .pathMatchers("/files/**").permitAll()
        .pathMatchers("/api/admin/**").hasRole("ADMIN")
        .pathMatchers("/api/users/register").permitAll()
        .pathMatchers("/api/users/login").permitAll()
        .pathMatchers("/api/users/refreshToken").permitAll()
        .pathMatchers("/api/users/verify").permitAll()
        .pathMatchers("/api/mealplans/**").permitAll()
        .pathMatchers("/api/menus/**").permitAll()
        .pathMatchers("/api/subscriptions/**").hasRole("USER")
        .pathMatchers("/api/users/**").hasRole("USER")
        .pathMatchers("/api/banners/**").permitAll()
        .and()
        .exceptionHandling()
        .authenticationEntryPoint((serverWebExchange, e) -> Mono.error(new UnauthorizedException()))
        .accessDeniedHandler((serverWebExchange, e) -> Mono.error(new UnauthorizedException()))
        .and()
        .addFilterBefore(exceptionFilter,SecurityWebFiltersOrder.LOGOUT)
        .build();
  }


  @Bean
  @Primary
  public ReactiveAuthenticationManager reactiveAuthenticationManager() {
    UserDetailsRepositoryReactiveAuthenticationManager authenticationManager =
        new UserDetailsRepositoryReactiveAuthenticationManager(reactiveUserDetailsService);
    authenticationManager.setPasswordEncoder(passwordEncoder());
    return authenticationManager;
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder(10);
  }
}
