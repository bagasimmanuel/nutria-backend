package com.blibli.nutria.nutria.configuration;

import com.blibli.nutria.nutria.controller.AdminController;
import com.blibli.nutria.nutria.controller.BannerController;
import com.blibli.nutria.nutria.controller.MealplanController;
import com.blibli.nutria.nutria.controller.MenuController;
import com.blibli.nutria.nutria.controller.SubscriptionController;
import com.blibli.nutria.nutria.controller.UserController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import java.nio.file.Paths;

@Configuration
public class RouterConfig {

  @Bean
  public RouterFunction<ServerResponse> adminControllerRouter(AdminController adminController) {
    return RouterFunctions.route().path("/api/admin",
            apiBuilder -> apiBuilder
                //Mealplan
                .POST("/mealplans", adminController::createMealplan)
                .PUT("/mealplans/{mealplanId}", adminController::editMealplan)
                .DELETE("/mealplans/{mealplanId}", adminController::deleteMealplan)
                .POST("/mealplans/{mealplanId}/image", adminController::uploadImageMealplan)
                //Stock
                .POST("/stocks", adminController::createStock)
                .PUT("/stocks/{stockId}", adminController::editStock)
                .DELETE("/stocks/{stockId}", adminController::deleteStock)
                .GET("/stocks", adminController::findAllWithParamStock)
                .GET("/stocks/{stockId}", adminController::findByIdStock)
                .POST("/stocks/{stockId}/image", adminController::uploadImageStock)
                 .GET("/stocks/check-stocks/{timestamp}",adminController::checkStock)
                //Menu
                .POST("/menus", adminController::createMenu)
                .PUT("/menus/{menuId}", adminController::editMenu)
                .DELETE("/menus/{menuId}", adminController::deleteMenu)
                .GET("/menus", adminController::findAllWithParamMenu)
                .GET("/menus/{menuId}", adminController::findByIdMenu)
                .POST("/menus/{menuId}/image", adminController::uploadImageMenu)

                // Subscritpion
                .GET("/subscriptions/{timestamp}/active", adminController::findAllActiveOrderSubscription)

                // BANNER
                .POST("/banners", adminController::uploadImageBanner)
                .DELETE("/banners/{bannerId}", adminController::deleteImageBanner)

                // REPORT
                .GET("/reports/{timestamp}",adminController::generateReport)

                .build())
        .build();
  }

  @Bean
  public RouterFunction<ServerResponse> menuControllerRouter(MenuController menuController) {
    return RouterFunctions.route().path("/api/menus",
        apiBuilder -> apiBuilder
            .GET("", menuController::findAllWithParam)
            .GET("/{menuId}", menuController::findById)
            .build()
    ).build();
  }

  @Bean
  public RouterFunction<ServerResponse> userControllerRouter(UserController userController) {
    return RouterFunctions.route().path("/api/users",
        apiBuilder -> apiBuilder
            .POST("/register", userController::register)
            .POST("/login", userController::login)
            .POST("/change-password", userController::changePassword)
            .POST("/logout", userController::logout)
            .PUT("/profile", userController::editProfile)
            .GET("/user", userController::getUserProfile)
            .POST("/refreshToken",userController::getNewAccessToken)
            .POST("/verify",userController::verifyToken)
            .build()
    ).build();
  }

  @Bean
  public RouterFunction<ServerResponse> mealplanRouter(MealplanController mealPlanController) {
    return RouterFunctions.route().path("/api/mealplans",
            apiBuilder -> apiBuilder
                .GET("", mealPlanController::findAllWithParam)
                .GET("/{mealplanId}", mealPlanController::findById)
                .build())
        .build();
  }

  @Bean
  public RouterFunction<ServerResponse> subscriptionRouter(SubscriptionController subscriptionController) {
    return RouterFunctions.route().path("/api/subscriptions",
            apiBuilder -> apiBuilder
                .POST("/subscribe", subscriptionController::createSubscription) //3
                .GET("/{subscriptionId}", subscriptionController::findById) // 5
                .PUT("/{subscriptionId}", subscriptionController::editPaymentStatus) // 6
                .PUT("/{subscriptionId}/item", subscriptionController::editSubscriptionItem) // 2
                .PUT("/{subscriptionId}/deliveryStatus", subscriptionController::editDeliveryStatus)
                .POST("/{subscriptionId}/renewal", subscriptionController::renewSubscription) // 1
                .GET("", subscriptionController::findAllWithParam) // 4
                .build())
        .build();
  }

  @Bean
  public RouterFunction<ServerResponse> bannerRouter(BannerController bannerController) {
    return RouterFunctions.route().path("/api/banners",
            apiBuilder -> apiBuilder
                .GET("", bannerController::findAllBanners)
                .build())
        .build();
  }

  @Bean
  public RouterFunction<ServerResponse> imgRouter(){
    return RouterFunctions.resources("/files/**",new FileSystemResource(Paths.get("storage").toAbsolutePath().normalize() + "/"));
  }

}
