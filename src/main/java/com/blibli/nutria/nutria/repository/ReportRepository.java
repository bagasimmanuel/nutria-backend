package com.blibli.nutria.nutria.repository;

import com.blibli.nutria.nutria.model.entity.Report;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface ReportRepository extends ReactiveMongoRepository<Report, UUID> {

  Mono<Report> findFirstByTimestamp(Long timestamp);

}
