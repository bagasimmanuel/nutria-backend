package com.blibli.nutria.nutria.repository.subscription;

import com.blibli.nutria.nutria.model.entity.Subscription;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface SubscriptionCustomRepository {

  Flux<Subscription> findAllWithParam(SubscriptionCriteria subscriptionCriteria, Pageable pageable);
  Mono<Long> countAllWithParam(SubscriptionCriteria subscriptionCriteria);

}
