package com.blibli.nutria.nutria.repository;

import com.blibli.nutria.nutria.model.entity.VerifyToken;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface VerifyTokenRepository extends ReactiveMongoRepository<VerifyToken, UUID> {

  Mono<VerifyToken> findFirstByVerifyTokenEquals(String token);

  Mono<Void> deleteByEmailEquals(String email);

}
