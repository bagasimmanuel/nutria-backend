package com.blibli.nutria.nutria.repository.subscription;

import com.blibli.nutria.nutria.model.entity.Subscription;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SubscriptionCriteria {

  private long timestamp;
  private String userId;
  private Subscription.PaymentStatus paymentStatus;

}
