package com.blibli.nutria.nutria.repository.menu;

import com.blibli.nutria.nutria.model.entity.Menu;
import org.reactivestreams.Publisher;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface MenuRepository extends ReactiveMongoRepository<Menu, UUID>, MenuCustomRepository {


  Mono<Menu> findFirstById(UUID uuid);

  Mono<Menu> findFirstByIdAndSoftDeleteFalse(UUID uuid);

  Mono<Void> deleteById(UUID uuid);

  Flux<Menu> findAllByIdIsNotNull(Pageable pageable);
  Mono<Long> countAllByIdNotNull();

  Flux<Menu> findAllByIdIsNotNullAndSoftDeleteFalse(Pageable pageable);
  Mono<Long> countAllByIdNotNullAndSoftDeleteFalse();

  Flux<Menu> findAllByNameLikeAndSoftDeleteFalse(String name, Pageable pageable);
  Mono<Long> countAllByNameLikeAndSoftDeleteFalse(String name);

  @Query(value = "{ '_id' : ?0 }",delete = true)
  Mono<Void> deleteById(Publisher<UUID> id);
}
