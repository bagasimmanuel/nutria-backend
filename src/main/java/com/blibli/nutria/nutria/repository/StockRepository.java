package com.blibli.nutria.nutria.repository;

import com.blibli.nutria.nutria.model.entity.Stock;
import org.reactivestreams.Publisher;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface StockRepository extends ReactiveMongoRepository<Stock, UUID> {

  Flux<Stock> findAllByIdNotNullAndSoftDeleteFalse(Pageable pageable);
  Mono<Long> countAllByIdNotNullAndSoftDeleteFalse();

  Flux<Stock> findAllByNameLikeAndSoftDeleteFalse(String name,Pageable pageable);
  Mono<Long> countAllByNameLikeAndSoftDeleteFalse(String name);

  Mono<Stock> findFirstByIdAndSoftDeleteFalse(UUID uuid);

  @Query(value = "{ '_id' : ?0 }",delete = true)
  Mono<Void> deleteById(Publisher<UUID> id);
}
