package com.blibli.nutria.nutria.repository.menu;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MenuCriteria {

  private String name;
  private String mealplanId;
  private Integer calorie;

}
