package com.blibli.nutria.nutria.repository.subscription;

import com.blibli.nutria.nutria.model.entity.Subscription;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface SubscriptionRepository extends ReactiveMongoRepository<Subscription, UUID>, SubscriptionCustomRepository {

  Mono<Subscription> findFirstById(UUID id);

  Mono<Subscription> findFirstByUserIdAndTimestampAndPaymentStatus(String userId, long timestmap, Subscription.PaymentStatus paymentStatus);

  @Query(value = "{ $and: [ { '_id' : ?0 }, { 'paymentStatus' : ?1 },{ 'subscriptionList.timestamp' : ?2 }]}")
  Mono<Subscription> findFirstByIdAndPaymentStatusAndSubscriptionItemTimestamp(UUID id, Subscription.PaymentStatus paymentStatus,long timestamp);

  @Query(value = "{ $and: [ { 'paymentStatus' : ?0 }, { 'subscriptionList.timestamp' : ?1 }]}")
  Flux<Subscription> findAllByPaymentStatusAndSubscriptionItemTimestamp(Subscription.PaymentStatus paymentStatus, long timestamp,Pageable pageable);
  @Query(value = "{ $and: [ { 'paymentStatus' : ?0 }, { 'subscriptionList.timestamp' : ?1 }]}",count = true)
  Mono<Long> countAllByPaymentStatusAndSubscriptionItemTimestamp(Subscription.PaymentStatus paymentStatus,long timestamp);

  Flux<Subscription> findAllByPaymentStatusAndTimestamp(Subscription.PaymentStatus paymentStatus,long timestamp);

}
