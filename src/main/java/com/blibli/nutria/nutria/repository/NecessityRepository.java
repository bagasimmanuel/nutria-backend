package com.blibli.nutria.nutria.repository;

import com.blibli.nutria.nutria.model.entity.Necessity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface NecessityRepository extends ReactiveMongoRepository<Necessity, UUID> {


  Mono<Necessity> findFirstByTimestamp(long timestamp);

}
