package com.blibli.nutria.nutria.repository.subscription;

import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.entity.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class SubscriptionCustomRepositoryImpl implements SubscriptionCustomRepository {

  @Autowired
  private UUIDHelper uuidHelper;
  @Autowired
  ReactiveMongoTemplate reactiveMongoTemplate;

  private static final String PAYMENT_STATUS = "paymentStatus";
  private static final String USER_ID = "userId";
  private static final String TIMESTAMP = "timestamp";


  @Override
  public Flux<Subscription> findAllWithParam(SubscriptionCriteria subscriptionCriteria, Pageable pageable) {
    Query query = constructQuery(subscriptionCriteria);
    query.with(pageable);
    return reactiveMongoTemplate.find(query,Subscription.class);
  }

  @Override
  public Mono<Long> countAllWithParam(SubscriptionCriteria subscriptionCriteria) {
    Query query = constructQuery(subscriptionCriteria);
    return reactiveMongoTemplate.count(query,Subscription.class);
  }

  private Query constructQuery(SubscriptionCriteria subscriptionCriteria) {
    Query query = new Query();
    if (subscriptionCriteria.getPaymentStatus() != null) {
      if (subscriptionCriteria.getUserId() != null) {
        if (subscriptionCriteria.getTimestamp() != 0) {
          query.addCriteria(new Criteria()
              .andOperator(
                  Criteria
                      .where(PAYMENT_STATUS)
                      .is(subscriptionCriteria.getPaymentStatus()),
                  Criteria
                      .where(USER_ID)
                      .is(subscriptionCriteria.getUserId()),
                  Criteria
                      .where(TIMESTAMP)
                      .lte(subscriptionCriteria.getTimestamp()))
          );
        } // Query with PaymentStatus,userId,Timestamp
        else {

          query.addCriteria(new Criteria()
              .andOperator(
                  Criteria
                      .where(PAYMENT_STATUS)
                      .is(subscriptionCriteria.getPaymentStatus().toString()),
                  Criteria
                      .where(USER_ID)
                      .is(subscriptionCriteria.getUserId())));

        } // Query with PaymentStatus,UserId
      } else {
        if (subscriptionCriteria.getTimestamp() != 0) {
          query.addCriteria(new Criteria()
              .andOperator(
                  Criteria
                      .where(PAYMENT_STATUS)
                      .is(subscriptionCriteria.getPaymentStatus()),
                  Criteria
                      .where(TIMESTAMP)
                      .lte(subscriptionCriteria.getTimestamp())));

        }  // Query with PaymentStatus, Timestamp
        else {
          query.addCriteria(
              Criteria
                  .where(PAYMENT_STATUS)
                  .is(subscriptionCriteria.getPaymentStatus()));

        }   //Query with paymentStatus Only
      }
    } else if (subscriptionCriteria.getUserId() != null) {
      if (subscriptionCriteria.getTimestamp() != 0) {
        query.addCriteria(new Criteria()
            .andOperator(
                Criteria
                    .where(USER_ID)
                    .is(subscriptionCriteria.getUserId()),
                Criteria
                    .where(TIMESTAMP)
                    .lte(subscriptionCriteria.getTimestamp())));
      }  //Query with userId and Timestamp
      else {
        query.addCriteria(
            Criteria
                .where(USER_ID)
                .is(subscriptionCriteria.getUserId()));

      } // Query with userId only
    } else if (subscriptionCriteria.getTimestamp() != 0) {
      query.addCriteria(
          Criteria
              .where(TIMESTAMP)
              .lte(subscriptionCriteria.getTimestamp()));
    }   // Query with Timestamp only
    else {
      query.addCriteria(
          Criteria
              .where("_id")
              .exists(true));
    }    // Query without Param
    return query;
  }

}
