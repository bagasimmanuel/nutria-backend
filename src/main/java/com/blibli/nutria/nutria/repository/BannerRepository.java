package com.blibli.nutria.nutria.repository;

import com.blibli.nutria.nutria.model.entity.Banner;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface BannerRepository extends ReactiveMongoRepository<Banner, UUID> {


  Flux<Banner> findAllByIdIsNotNull(Pageable pageable);
  Mono<Long> countAllByIdNotNull();

  Flux<Banner> findAllByIdIsNotNullAndSoftDeleteFalse(Pageable pageable);
  Mono<Long> countAllByIdIsNotNullAndSoftDeleteFalse();

  Mono<Banner> findFirstById(UUID uuid);

}
