package com.blibli.nutria.nutria.repository.menu;

import com.blibli.nutria.nutria.model.entity.Menu;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MenuCustomRepository {

  Flux<Menu> findAllWithParam(MenuCriteria menuCriteria,Pageable pageable);
  Mono<Long> countAllWithParam(MenuCriteria menuCriteria);

}
