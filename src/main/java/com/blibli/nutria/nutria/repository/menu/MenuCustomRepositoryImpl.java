package com.blibli.nutria.nutria.repository.menu;

import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.entity.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Repository
public class MenuCustomRepositoryImpl implements MenuCustomRepository {

  @Autowired
  ReactiveMongoTemplate reactiveMongoTemplate;

  @Autowired
  UUIDHelper uuidHelper;

  private static final String CALORIE = "calorie";
  private static final String MEALPLAN_ID = "mealplanId";
  private static final String NAME = "name";
  private static final String SOFT_DELETE = "softDelete";


  @Override
  public Flux<Menu> findAllWithParam(MenuCriteria menuCriteria, Pageable pageable) {
    Query query = constructQuery(menuCriteria);
    query.with(Sort.by(Sort.Direction.ASC,"name"));
    query.with(pageable);
    return reactiveMongoTemplate.find(query, Menu.class);
  }

  @Override
  public Mono<Long> countAllWithParam(MenuCriteria menuCriteria) {
    Query query = constructQuery(menuCriteria);
    return reactiveMongoTemplate.count(query, Menu.class);
  }

  // Query Constructor
  private Query constructQuery(MenuCriteria menuCriteria) {
    Query query = new Query();
    query.addCriteria(Criteria.where(SOFT_DELETE).is(false));
    if (menuCriteria.getName() != null) {
      if (menuCriteria.getMealplanId() != null) {
        if (menuCriteria.getCalorie() != 0) {
          query.addCriteria(new Criteria()
              .andOperator(
                  Criteria
                      .where(NAME)
                      .regex(menuCriteria.getName()),
                  Criteria
                      .where(MEALPLAN_ID)
                      .is(menuCriteria.getMealplanId()),
                  Criteria
                      .where(CALORIE)
                      .lte(menuCriteria.getCalorie()))
          );// Query with Name,Mealplan,Calorie
        } // Query with Name,Mealplan,Calorie
        else {
          query.addCriteria(new Criteria()
              .andOperator(
                  Criteria
                      .where(NAME)
                      .regex(menuCriteria.getName()),
                  Criteria
                      .where(MEALPLAN_ID)
                      .is(menuCriteria.getMealplanId())));

        } // Query with Name,Mealplan
      } else {
        if (menuCriteria.getCalorie() != 0) {
          query.addCriteria(new Criteria()
              .andOperator(
                  Criteria
                      .where(NAME)
                      .regex(menuCriteria.getName()),
                  Criteria
                      .where(CALORIE)
                      .lte(menuCriteria.getCalorie())));

        }  // Query with Name, Calorie
        else {
          query.addCriteria(
              Criteria
                  .where(NAME)
                  .regex(menuCriteria.getName()));

        }   //Query with Name Only
      }
    } else if (menuCriteria.getMealplanId() != null) {
      if (menuCriteria.getCalorie() != 0) {
        query.addCriteria(new Criteria()
            .andOperator(
                Criteria
                    .where(MEALPLAN_ID)
                    .is(menuCriteria.getMealplanId()),
                Criteria
                    .where(CALORIE)
                    .lte(menuCriteria.getCalorie())));
      }  //Query with Mealplan and calorie
      else {
        query.addCriteria(
            Criteria
                .where(MEALPLAN_ID)
                .is(menuCriteria.getMealplanId()));

      } // Query with Mealplan only
    } else if (menuCriteria.getCalorie() != 0) {
      query.addCriteria(
          Criteria
              .where(CALORIE)
              .lte(menuCriteria.getCalorie()));
    }   // Query with Calorie only
    else {
      query.addCriteria(
          Criteria
              .where("_id")
              .exists(true));
    }    // Query without Param
    return query;
  }

}
