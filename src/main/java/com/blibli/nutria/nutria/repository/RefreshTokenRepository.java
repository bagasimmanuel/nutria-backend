package com.blibli.nutria.nutria.repository;

import com.blibli.nutria.nutria.model.entity.RefreshToken;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface RefreshTokenRepository extends ReactiveMongoRepository<RefreshToken,UUID> {

  Mono<RefreshToken> findFirstByRefreshToken(UUID token);

}
