package com.blibli.nutria.nutria.repository;

import com.blibli.nutria.nutria.model.entity.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, UUID> {

  Mono<User> findFirstByEmail(String s);

}
