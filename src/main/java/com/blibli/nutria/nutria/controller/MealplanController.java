package com.blibli.nutria.nutria.controller;

import com.blibli.nutria.nutria.command.CommandInvoker;
import com.blibli.nutria.nutria.command.mealplan.FindAllByNameMealplanCommand;
import com.blibli.nutria.nutria.command.mealplan.FindAllMealplanCommand;
import com.blibli.nutria.nutria.command.mealplan.FindByIdMealplanCommand;
import com.blibli.nutria.nutria.model.command.request.mealplan.FindByIdMealplanRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.FindByIdMealplanResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.GetMealplanResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Optional;

@Component
@AllArgsConstructor
public class MealplanController {

  private CommandInvoker commandInvoker;

  public Mono<ServerResponse> findById(ServerRequest request) {
    Mono<FindByIdMealplanRequest> monoRequest =
        Mono.just(FindByIdMealplanRequest.builder().mealplanId(request.pathVariable("mealplanId")).build());
    Mono<BaseCommandResponse<FindByIdMealplanResponse>> responseMono =
        commandInvoker.invokeCommand(FindByIdMealplanCommand.class, monoRequest);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> findAllWithParam(ServerRequest request) {
    Optional<String> nameQuery = request.queryParam("name");
    int page = Integer.parseInt(request.queryParam("page").orElse("0"));
    Mono<BasePaginatedCommandResponse<GetMealplanResponse>> responseMono;
    if (nameQuery.isPresent()) { // Apakah harus dibuat seperti ini untuk setiap queryParam?
      responseMono = commandInvoker.invokeCommand(FindAllByNameMealplanCommand.class, Mono.zip(Mono.just(nameQuery.get()),Mono.just(page)));
    } else {
      responseMono = commandInvoker.invokeCommand(FindAllMealplanCommand.class, Mono.just(page));
    }
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }


}
