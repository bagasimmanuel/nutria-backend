package com.blibli.nutria.nutria.controller;

import com.blibli.nutria.nutria.command.CommandInvoker;
import com.blibli.nutria.nutria.command.banner.DeleteBannerImageCommand;
import com.blibli.nutria.nutria.command.banner.UploadBannerImageCommand;
import com.blibli.nutria.nutria.command.mealplan.CreateMealplanCommand;
import com.blibli.nutria.nutria.command.mealplan.DeleteMealplanCommand;
import com.blibli.nutria.nutria.command.mealplan.EditMealplanCommand;
import com.blibli.nutria.nutria.command.mealplan.UploadImageMealplanCommand;
import com.blibli.nutria.nutria.command.menu.CreateMenuCommand;
import com.blibli.nutria.nutria.command.menu.DeleteMenuCommand;
import com.blibli.nutria.nutria.command.menu.EditMenuCommand;
import com.blibli.nutria.nutria.command.menu.FindAllByNameMenuCommand;
import com.blibli.nutria.nutria.command.menu.FindAllMenuCommand;
import com.blibli.nutria.nutria.command.menu.FindByIdMenuCommand;
import com.blibli.nutria.nutria.command.menu.UploadImageMenuCommand;
import com.blibli.nutria.nutria.command.report.GenerateReportCommand;
import com.blibli.nutria.nutria.command.stock.CheckStockCommand;
import com.blibli.nutria.nutria.command.stock.CreateStockCommand;
import com.blibli.nutria.nutria.command.stock.DeleteStockCommand;
import com.blibli.nutria.nutria.command.stock.EditStockCommand;
import com.blibli.nutria.nutria.command.stock.FindAllStockByNameCommand;
import com.blibli.nutria.nutria.command.stock.FindAllStockCommand;
import com.blibli.nutria.nutria.command.stock.FindByIdStockCommand;
import com.blibli.nutria.nutria.command.stock.UploadImageStockCommand;
import com.blibli.nutria.nutria.command.subscription.item.GetAllActiveSubscriptionCommand;
import com.blibli.nutria.nutria.model.command.request.mealplan.CreateMealplanRequest;
import com.blibli.nutria.nutria.model.command.request.mealplan.EditMealplanRequest;
import com.blibli.nutria.nutria.model.command.request.menu.CreateMenuRequest;
import com.blibli.nutria.nutria.model.command.request.menu.EditMenuRequest;
import com.blibli.nutria.nutria.model.command.request.stock.CreateStockRequest;
import com.blibli.nutria.nutria.model.command.request.stock.EditStockRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.banner.BannerResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.CreateMealplanResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.EditMealplanResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.GetMealplanResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import com.blibli.nutria.nutria.model.command.response.report.ReportResponse;
import com.blibli.nutria.nutria.model.command.response.stock.CheckStockResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.ActiveOrderResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Optional;

@Component
@AllArgsConstructor
public class AdminController {

  private CommandInvoker commandInvoker;

  //Mealplan

  private static final String MEALPLAN_ID = "mealplanId";
  private static final String STOCK_ID = "stockId";
  private static final String MENU_ID = "menuId";
  private static final String SUBSCRIPTION_ID = "subscriptionId";

  public Mono<ServerResponse> createMealplan(ServerRequest request) {
    Mono<CreateMealplanRequest> monoRequest = request.bodyToMono(CreateMealplanRequest.class);
    Mono<BaseCommandResponse<CreateMealplanResponse>> responseMono = commandInvoker.invokeCommand(CreateMealplanCommand.class, monoRequest);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> editMealplan(ServerRequest request) {
    Mono<EditMealplanRequest> requestMono = request.bodyToMono(EditMealplanRequest.class);
    Mono<String> idMono = Mono.just(request.pathVariable(MEALPLAN_ID));
    Mono<BaseCommandResponse<EditMealplanResponse>> responseMono =
        this.commandInvoker.invokeCommand(EditMealplanCommand.class, Mono.zip(requestMono, idMono));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> deleteMealplan(ServerRequest request) {
    Mono<String> idMono = Mono.just(request.pathVariable(MEALPLAN_ID));
    Mono<BaseCommandResponse<Void>> responseMono = this.commandInvoker.invokeCommand(DeleteMealplanCommand.class, idMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> uploadImageMealplan(ServerRequest request) {
    Mono<String> mealplanId = Mono.just(request.pathVariable(MEALPLAN_ID));
    Mono<FilePart> filePartMono = request.body(BodyExtractors.toMultipartData()).map(p -> (FilePart) p.toSingleValueMap().get("file"));
    Mono<BaseCommandResponse<GetMealplanResponse>> responseMono =
        this.commandInvoker.invokeCommand(UploadImageMealplanCommand.class, mealplanId.zipWith(filePartMono));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic()).log();
  }

  // STOCK
  // STOCK
  // STOCK

  public Mono<ServerResponse> createStock(ServerRequest request) {
    Mono<CreateStockRequest> requestMono = request.bodyToMono(CreateStockRequest.class);
    Mono<BaseCommandResponse<StockResponse>> responseMono = this.commandInvoker.invokeCommand(CreateStockCommand.class, requestMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> editStock(ServerRequest request) {
    Mono<String> stockId = Mono.just(request.pathVariable(STOCK_ID));
    Mono<EditStockRequest> requestMono = request.bodyToMono(EditStockRequest.class);
    Mono<BaseCommandResponse<StockResponse>> responseMono =
        this.commandInvoker.invokeCommand(EditStockCommand.class, requestMono.zipWith(stockId));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> deleteStock(ServerRequest request) {
    Mono<String> stockId = Mono.just(request.pathVariable(STOCK_ID));
    Mono<BaseCommandResponse<Void>> responseMono = this.commandInvoker.invokeCommand(DeleteStockCommand.class, stockId);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> findByIdStock(ServerRequest request) {
    Mono<String> stockId = Mono.just(request.pathVariable(STOCK_ID));
    Mono<BaseCommandResponse<StockResponse>> responseMono = this.commandInvoker.invokeCommand(FindByIdStockCommand.class, stockId);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> findAllWithParamStock(ServerRequest request) {
    Optional<String> nameQuery = request.queryParam("name");
    int page = Integer.parseInt(request.queryParam("page").orElse("0"));
    Mono<BasePaginatedCommandResponse<StockResponse>> responseMono;
    if (nameQuery.isPresent()) { // Apakah harus dibuat seperti ini untuk setiap queryParam?
      responseMono = commandInvoker.invokeCommand(FindAllStockByNameCommand.class, Mono.zip(Mono.just(nameQuery.get()), Mono.just(page)));
    } else {
      responseMono = commandInvoker.invokeCommand(FindAllStockCommand.class, Mono.just(page));
    }
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> uploadImageStock(ServerRequest request) {
    Mono<String> stockId = Mono.just(request.pathVariable(STOCK_ID));
    Mono<FilePart> filePartMono = request.body(BodyExtractors.toMultipartData()).map(p -> (FilePart) p.toSingleValueMap().get("file"));
    Mono<BaseCommandResponse<StockResponse>> responseMono =
        this.commandInvoker.invokeCommand(UploadImageStockCommand.class, stockId.zipWith(filePartMono));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> checkStock(ServerRequest request){
    Mono<Long> timestampMono = Mono.just(Long.valueOf(request.pathVariable("timestamp")));
    Mono<BaseCommandResponse<CheckStockResponse>> responseMono = this.commandInvoker.invokeCommand(CheckStockCommand.class,timestampMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  // MENU
  // MENU
  // MENU

  public Mono<ServerResponse> createMenu(ServerRequest request) {
    Mono<CreateMenuRequest> requestMono = request.bodyToMono(CreateMenuRequest.class);
    Mono<BaseCommandResponse<MenuAdminResponse>> responseMono = this.commandInvoker.invokeCommand(CreateMenuCommand.class, requestMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> editMenu(ServerRequest request) {
    Mono<EditMenuRequest> requestMono = request.bodyToMono(EditMenuRequest.class);
    Mono<String> menuId = Mono.just(request.pathVariable(MENU_ID));
    Mono<BaseCommandResponse<MenuAdminResponse>> responseMono =
        commandInvoker.invokeCommand(EditMenuCommand.class, requestMono.zipWith(menuId));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> deleteMenu(ServerRequest request) {
    Mono<String> menuId = Mono.just(request.pathVariable(MENU_ID));
    Mono<BaseCommandResponse<Void>> responseMono = commandInvoker.invokeCommand(DeleteMenuCommand.class, menuId);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> findAllWithParamMenu(ServerRequest request) {
    Optional<String> nameQuery = request.queryParam("name");
    int page = Integer.parseInt(request.queryParam("page").orElse("0"));
    Mono<BasePaginatedCommandResponse<MenuAdminResponse>> responseMono;
    if (nameQuery.isPresent()) {
      responseMono =
          this.commandInvoker.invokeCommand(FindAllByNameMenuCommand.class, Mono.zip(Mono.just(nameQuery.get()), Mono.just(page)));
    } else {
      responseMono = this.commandInvoker.invokeCommand(FindAllMenuCommand.class, Mono.just(page));
    }
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> findByIdMenu(ServerRequest request) {

    Mono<String> menuId = Mono.just(request.pathVariable(MENU_ID));
    Mono<BaseCommandResponse<MenuAdminResponse>> responseMono = this.commandInvoker.invokeCommand(FindByIdMenuCommand.class, menuId);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());

  }

  public Mono<ServerResponse> uploadImageMenu(ServerRequest request) {
    Mono<String> stockId = Mono.just(request.pathVariable(MENU_ID));
    Mono<FilePart> filePartMono = request.body(BodyExtractors.toMultipartData()).map(p -> (FilePart) p.toSingleValueMap().get("file"));
    Mono<BaseCommandResponse<MenuAdminResponse>> responseMono =
        this.commandInvoker.invokeCommand(UploadImageMenuCommand.class, stockId.zipWith(filePartMono));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  // Subscription

  public Mono<ServerResponse> findAllActiveOrderSubscription(ServerRequest request){
    int page = Integer.parseInt(request.queryParam("page").orElse("0"));
    Mono<Long> timestampMono = Mono.just(Long.valueOf(request.pathVariable("timestamp")));
    Mono<BasePaginatedCommandResponse<ActiveOrderResponse>> responseMono =
        this.commandInvoker.invokeCommand(GetAllActiveSubscriptionCommand.class, timestampMono.zipWith(Mono.just(page)));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  // BANNER
  // BANNER
  // BANNER

  public Mono<ServerResponse> uploadImageBanner(ServerRequest request){
    Mono<FilePart> filePartMono = request.body(BodyExtractors.toMultipartData()).map(p -> (FilePart) p.toSingleValueMap().get("file"));
    Mono<BaseCommandResponse<BannerResponse>> responseMono = this.commandInvoker.invokeCommand(UploadBannerImageCommand.class,filePartMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> deleteImageBanner(ServerRequest request){
    Mono<String> stringMono = Mono.just(request.pathVariable("bannerId"));
    Mono<BaseCommandResponse<Void>> responseMono = this.commandInvoker.invokeCommand(DeleteBannerImageCommand.class,stringMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  // REPORT
  // REPORT
  // REPORT
  public Mono<ServerResponse> generateReport(ServerRequest request){
    Mono<Long> timestampMono = Mono.just(Long.valueOf(request.pathVariable("timestamp")));
    Mono<BaseCommandResponse<ReportResponse>> responseMono = this.commandInvoker.invokeCommand(GenerateReportCommand.class,timestampMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

}
