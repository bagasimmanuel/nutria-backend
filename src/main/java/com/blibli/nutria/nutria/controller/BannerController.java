package com.blibli.nutria.nutria.controller;

import com.blibli.nutria.nutria.command.CommandInvoker;
import com.blibli.nutria.nutria.command.banner.FindAllBannerCommand;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.banner.BannerResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Component
@AllArgsConstructor
public class BannerController {

  private CommandInvoker commandInvoker;

  public Mono<ServerResponse> findAllBanners(ServerRequest request){
    Mono<Integer> page = Mono.just(Integer.parseInt(request.queryParam("page").orElse("0")));
    Mono<BasePaginatedCommandResponse<BannerResponse>> responseMono = this.commandInvoker.invokeCommand(FindAllBannerCommand.class,page);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

}
