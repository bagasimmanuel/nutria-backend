package com.blibli.nutria.nutria.controller;

import com.blibli.nutria.nutria.command.CommandInvoker;
import com.blibli.nutria.nutria.command.user.ChangePasswordUserCommand;
import com.blibli.nutria.nutria.command.user.EditProfileUserCommand;
import com.blibli.nutria.nutria.command.user.GetNewTokenCommand;
import com.blibli.nutria.nutria.command.user.GetUserProfileCommand;
import com.blibli.nutria.nutria.command.user.LoginUserCommand;
import com.blibli.nutria.nutria.command.user.LogoutUserCommand;
import com.blibli.nutria.nutria.command.user.RegisterUserCommand;
import com.blibli.nutria.nutria.command.user.VerifyEmailCommand;
import com.blibli.nutria.nutria.model.command.request.user.ChangePasswordRequest;
import com.blibli.nutria.nutria.model.command.request.user.EditProfileUserRequest;
import com.blibli.nutria.nutria.model.command.request.user.LoginUserRequest;
import com.blibli.nutria.nutria.model.command.request.user.NewAccessTokenRequest;
import com.blibli.nutria.nutria.model.command.request.user.RegisterUserRequest;
import com.blibli.nutria.nutria.model.command.request.user.VerifyEmailRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.user.EditProfileUserResponse;
import com.blibli.nutria.nutria.model.command.response.user.LoginUserResponse;
import com.blibli.nutria.nutria.model.command.response.user.NewAccessTokenResponse;
import com.blibli.nutria.nutria.model.command.response.user.RegisterUserResponse;
import com.blibli.nutria.nutria.model.command.response.user.UserProfileResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Component
@AllArgsConstructor
public class UserController {

  private CommandInvoker commandInvoker;

  public Mono<ServerResponse> register(ServerRequest serverRequest) {
    Mono<RegisterUserRequest> requestMono = serverRequest.bodyToMono(RegisterUserRequest.class);
    Mono<BaseCommandResponse<RegisterUserResponse>> responseMono =
        this.commandInvoker.invokeCommand(RegisterUserCommand.class, requestMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> login(ServerRequest serverRequest) {
    Mono<LoginUserRequest> requestMono = serverRequest.bodyToMono(LoginUserRequest.class);
    Mono<BaseCommandResponse<LoginUserResponse>> responseMono = this.commandInvoker.invokeCommand(LoginUserCommand.class, requestMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> changePassword(ServerRequest serverRequest) {
    Mono<ChangePasswordRequest> requestMono = serverRequest.bodyToMono(ChangePasswordRequest.class);
    Mono<BaseCommandResponse<Void>> responseMono = this.commandInvoker.invokeCommand(ChangePasswordUserCommand.class, requestMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> logout(ServerRequest serverRequest) {
    Mono<String> accessToken = Mono.justOrEmpty(serverRequest.headers().firstHeader(HttpHeaders.AUTHORIZATION));
    Mono<BaseCommandResponse<Void>> responseMono = this.commandInvoker.invokeCommand(LogoutUserCommand.class, accessToken);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> editProfile(ServerRequest serverRequest) {
    Mono<EditProfileUserRequest> requestMono = serverRequest.bodyToMono(EditProfileUserRequest.class);
    Mono<BaseCommandResponse<EditProfileUserResponse>> responseMono =
        this.commandInvoker.invokeCommand(EditProfileUserCommand.class, requestMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> getUserProfile(ServerRequest serverRequest) {
    Mono<Void> requestMono = Mono.empty();
    Mono<BaseCommandResponse<UserProfileResponse>> responseMono =
        this.commandInvoker.invokeCommand(GetUserProfileCommand.class, requestMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> getNewAccessToken(ServerRequest serverRequest){
    Mono<NewAccessTokenRequest> tokenMono = serverRequest.bodyToMono(NewAccessTokenRequest.class);
    Mono<BaseCommandResponse<NewAccessTokenResponse>> responseMono = this.commandInvoker.invokeCommand(GetNewTokenCommand.class,tokenMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> verifyToken(ServerRequest serverRequest){
    Mono<VerifyEmailRequest> requestMono = serverRequest.bodyToMono(VerifyEmailRequest.class);
    Mono<BaseCommandResponse<Void>> responseMono = this.commandInvoker.invokeCommand(VerifyEmailCommand.class,requestMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

}
