package com.blibli.nutria.nutria.controller;

import com.blibli.nutria.nutria.command.CommandInvoker;
import com.blibli.nutria.nutria.command.subscription.CreateSubscriptionCommand;
import com.blibli.nutria.nutria.command.subscription.EditSubscriptionPaymentStatusCommand;
import com.blibli.nutria.nutria.command.subscription.FindByIdSubscriptionCommand;
import com.blibli.nutria.nutria.command.subscription.GetAllSubcriptionCommand;
import com.blibli.nutria.nutria.command.subscription.RenewSubscriptionCommand;
import com.blibli.nutria.nutria.command.subscription.item.EditSubscriptionDeliveryStatusCommand;
import com.blibli.nutria.nutria.command.subscription.item.EditSubscriptionItemCommand;
import com.blibli.nutria.nutria.model.command.request.subscription.CreateSubscriptionRequest;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionDeliveryStatusRequest;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionItemRequest;
import com.blibli.nutria.nutria.model.command.request.subscription.EditSubscriptionPaymentStatusRequest;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.ActiveOrderResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.repository.subscription.SubscriptionCriteria;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;


@Component
@AllArgsConstructor
public class SubscriptionController {

  private CommandInvoker commandInvoker;

  public Mono<ServerResponse> createSubscription(ServerRequest request) {
    Mono<CreateSubscriptionRequest> subscriptionMono = request.bodyToMono(CreateSubscriptionRequest.class);
    Mono<BaseCommandResponse<SubscriptionResponse>> responseMono =
        this.commandInvoker.invokeCommand(CreateSubscriptionCommand.class, subscriptionMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> renewSubscription(ServerRequest request) {
    Mono<String> stringMono = Mono.just(request.pathVariable("subscriptionId"));
    Mono<BaseCommandResponse<SubscriptionResponse>> responseMono =
        this.commandInvoker.invokeCommand(RenewSubscriptionCommand.class, stringMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> findAllWithParam(ServerRequest request) {
    Subscription.PaymentStatus  paymentStatus = Subscription.PaymentStatus.valueOfOrNull(request.queryParam("paymentStatus").orElse("NULL"));
    long timestampQuery = Integer.parseInt(request.queryParam("timestamp").orElse("0"));
    int page = Integer.parseInt(request.queryParam("page").orElse("0"));
    SubscriptionCriteria subscriptionCriteria = SubscriptionCriteria.builder()
        .paymentStatus(paymentStatus)
        .timestamp(timestampQuery)
        .build();
    Mono<BasePaginatedCommandResponse<SubscriptionResponse>> responseMono =
        this.commandInvoker.invokeCommand(GetAllSubcriptionCommand.class, Mono.zip(Mono.just(subscriptionCriteria), Mono.just(page)));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> findById(ServerRequest request){
    Mono<String> stringMono = Mono.just(request.pathVariable("subscriptionId"));
    Mono<BaseCommandResponse<SubscriptionResponse>> responseMono =
        this.commandInvoker.invokeCommand(FindByIdSubscriptionCommand.class, stringMono);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> editPaymentStatus(ServerRequest request){
    Mono<String> stringMono = Mono.just(request.pathVariable("subscriptionId"));
    Mono<EditSubscriptionPaymentStatusRequest> paymentStatusMono = request.bodyToMono(EditSubscriptionPaymentStatusRequest.class);
    Mono<BaseCommandResponse<SubscriptionResponse>> responseMono =
        this.commandInvoker.invokeCommand(EditSubscriptionPaymentStatusCommand.class, stringMono.zipWith(paymentStatusMono));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> editDeliveryStatus(ServerRequest request){
    Mono<String> stringMono = Mono.just(request.pathVariable("subscriptionId"));
    Mono<EditSubscriptionDeliveryStatusRequest> paymentStatusMono = request.bodyToMono(EditSubscriptionDeliveryStatusRequest.class);
    Mono<BaseCommandResponse<ActiveOrderResponse>> responseMono =
        this.commandInvoker.invokeCommand(EditSubscriptionDeliveryStatusCommand.class, stringMono.zipWith(paymentStatusMono));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> editSubscriptionItem(ServerRequest request){
    Mono<String> stringMono = Mono.just(request.pathVariable("subscriptionId"));
    Mono<EditSubscriptionItemRequest> itemMono = request.bodyToMono(EditSubscriptionItemRequest.class);
    Mono<BaseCommandResponse<SubscriptionResponse>> responseMono = this.commandInvoker.invokeCommand(EditSubscriptionItemCommand.class,itemMono.zipWith(stringMono));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

}
