package com.blibli.nutria.nutria.controller;

import com.blibli.nutria.nutria.command.CommandInvoker;
import com.blibli.nutria.nutria.command.menu.user.UserFindAllWithParam;
import com.blibli.nutria.nutria.command.menu.user.UserFindByIdMenuCommand;
import com.blibli.nutria.nutria.helper.UUIDHelper;
import com.blibli.nutria.nutria.model.command.response.BaseCommandResponse;
import com.blibli.nutria.nutria.model.command.response.BasePaginatedCommandResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuResponse;
import com.blibli.nutria.nutria.repository.menu.MenuCriteria;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Optional;

@Component
@AllArgsConstructor
public class MenuController {

  private CommandInvoker commandInvoker;
  private UUIDHelper uuidHelper;

  public Mono<ServerResponse> findAllWithParam(ServerRequest request) {
    Optional<String> nameQuery = request.queryParam("name");
    Optional<String> mealplanIdQuery = request.queryParam("mealplanId");
    int calorieQuery = Integer.parseInt(request.queryParam("calorie").orElse("0"));
    int page = Integer.parseInt(request.queryParam("page").orElse("0"));
    MenuCriteria menuCriteria = MenuCriteria.builder()
        .mealplanId(mealplanIdQuery.orElse(null))
        .name(nameQuery.orElse(null))
        .calorie(calorieQuery)
        .build();
    System.out.println(menuCriteria);
    Mono<BasePaginatedCommandResponse<MenuResponse>> responseMono = this.commandInvoker.invokeCommand(UserFindAllWithParam.class,Mono.zip(Mono.just(menuCriteria),Mono.just(page)));
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());
  }

  public Mono<ServerResponse> findById(ServerRequest request) {
    Mono<String> menuId = Mono.just(request.pathVariable("menuId"));
    Mono<BaseCommandResponse<MenuResponse>> responseMono = this.commandInvoker.invokeCommand(UserFindByIdMenuCommand.class, menuId);
    return responseMono.flatMap(response -> ServerResponse.ok().bodyValue(response)).subscribeOn(Schedulers.boundedElastic());

  }
}
