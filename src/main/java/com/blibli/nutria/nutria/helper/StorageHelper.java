package com.blibli.nutria.nutria.helper;

import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Component
public class StorageHelper {


  public Mono<String> storeFile(FilePart filePart,STORAGE_TYPE storageType, UUID uuid) {
    Path completePath = Paths.get(Paths.get("storage").toAbsolutePath().normalize().toString(),storageType.getName(),uuid.toString());
//    System.out.println(String.format("/files/%s/%s",storageType.getName(),uuid.toString()));
    return Mono.fromCallable(() -> {
      Files.createDirectories(completePath.getParent());
      return true;
    }).then(filePart.transferTo(new File(completePath.toString()))).thenReturn(String.format("/files/%s/%s",storageType.getName(),uuid.toString()));
  }

  public Mono<Boolean> deleteFile(STORAGE_TYPE storageType,UUID uuid){
    Path fileToDeletePath = Paths.get(Paths.get("storage").toAbsolutePath().normalize().toString(), storageType.getName(),uuid.toString());
    File f = new File(String.valueOf(fileToDeletePath));
    if(f.exists()){
      return Mono.fromCallable(() -> {
        Files.delete(fileToDeletePath);
        return true;
      });
    }else{
      return Mono.just(true);
    }
  }


  public enum STORAGE_TYPE{
    MEALPLAN_IMAGE("mealplan"),
    BANNER_IMAGE("banner"),
    MENU_IMAGE("menu"),
    STOCK_IMAGE("stock");

    private String name;

    STORAGE_TYPE(String name) {
      this.name = name;
    }

    public String getName(){ return this.name; }
  }
}
