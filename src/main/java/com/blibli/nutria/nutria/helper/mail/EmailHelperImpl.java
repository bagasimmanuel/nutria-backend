package com.blibli.nutria.nutria.helper.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Date;

@Component

public class EmailHelperImpl implements EmailHelper{

  private final String senderAddress = "nutriaapplication@gmail.com";

  private final String senderName = "nutria";

  @Autowired
  private JavaMailSender emailSender;


  @Override
  public void sendEmail(String to, String subject,String body) {
    try {
      MimeMessage msg = emailSender.createMimeMessage();
      MimeMessageHelper helper = new MimeMessageHelper(msg,true,"UTF-8");
      msg.setFrom(new InternetAddress(senderAddress, senderName));
      msg.setReplyTo(InternetAddress.parse(senderAddress, false));
      msg.setSubject("Nutria Email Verification");
      msg.setText(body);
      msg.setSentDate(new Date());
      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
      emailSender.send(msg);
      System.out.println("message Sent");
    } catch (MailException | MessagingException | UnsupportedEncodingException exception) {
      System.out.println("error");
    }
  }
}
