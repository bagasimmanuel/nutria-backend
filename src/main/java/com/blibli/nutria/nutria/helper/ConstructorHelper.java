package com.blibli.nutria.nutria.helper;

import com.blibli.nutria.nutria.exception.BadRequestException;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.model.command.response.IngredientResponse;
import com.blibli.nutria.nutria.model.command.response.SubscriptionItemResponse;
import com.blibli.nutria.nutria.model.command.response.mealplan.MealplanResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuAdminResponse;
import com.blibli.nutria.nutria.model.command.response.menu.MenuResponse;
import com.blibli.nutria.nutria.model.command.response.report.ReportResponse;
import com.blibli.nutria.nutria.model.command.response.stock.StockResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.ActiveOrderResponse;
import com.blibli.nutria.nutria.model.command.response.subscription.SubscriptionResponse;
import com.blibli.nutria.nutria.model.command.response.user.UserResponse;
import com.blibli.nutria.nutria.model.dto.Ingredient;
import com.blibli.nutria.nutria.model.dto.SubscriptionItem;
import com.blibli.nutria.nutria.model.entity.Menu;
import com.blibli.nutria.nutria.model.entity.Report;
import com.blibli.nutria.nutria.model.entity.Subscription;
import com.blibli.nutria.nutria.repository.MealplanRepository;
import com.blibli.nutria.nutria.repository.StockRepository;
import com.blibli.nutria.nutria.repository.UserRepository;
import com.blibli.nutria.nutria.repository.menu.MenuRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class ConstructorHelper {

  private MenuRepository menuRepository;
  private MealplanRepository mealplanRepository;
  private StockRepository stockRepository;
  private UserRepository userRepository;
  private UUIDHelper uuidHelper;

  public Mono<SubscriptionResponse> constructSubscriptionResponse(Subscription subscription) {
    Mono<Subscription> subscriptionMono = Mono.just(subscription);
    return subscriptionMono.zipWhen(this::getSubscriptionItemResponse)
        .zipWhen(tuple2 -> constructUserResponse(subscription.getUserId()))
        .map(tuple3 -> Tuples.of(tuple3.getT1().getT1(), tuple3.getT1().getT2(), tuple3.getT2()))
        .map(SubscriptionResponse::new);

  }

  private Mono<UserResponse> constructUserResponse(String userId) {
    return userRepository.findById(uuidHelper.fromString(userId))
        .switchIfEmpty(Mono.error(new NotFoundException("USER")))
        .zipWhen(user -> constructMealplanResponse(user.getMealplanId()))
        .map(UserResponse::new);
  }

  private Mono<MealplanResponse> constructMealplanResponse(String mealplanId) {
    return mealplanRepository.findById(uuidHelper.fromString(mealplanId))
        .switchIfEmpty(Mono.error(new NotFoundException("MEALPLAN")))
        .map(MealplanResponse::new);
  }

  public Mono<List<SubscriptionItemResponse>> getSubscriptionItemResponse(Subscription subscription) {
    return Flux.fromIterable(subscription.getSubscriptionList())
        .flatMapSequential(this::constructSubscriptionItemResponse)
        .collectList();
  }

  private Mono<SubscriptionItemResponse> constructSubscriptionItemResponse(SubscriptionItem subscriptionItem) {
    return menuRepository.findFirstById(uuidHelper.fromString(subscriptionItem.getMenuId()))
        .switchIfEmpty(Mono.error(new NotFoundException("MENU")))
        .flatMap(this::constructMenuResponse)
        .map(menuResponse ->
            SubscriptionItemResponse.builder()
                .deliveryTime(subscriptionItem.getDeliveryTime())
                .deliveryLoc(subscriptionItem.getDeliveryLoc())
                .deliveryStatus(subscriptionItem.getDeliveryStatus())
                .timestamp(subscriptionItem.getTimestamp())
                .menu(menuResponse)
                .build());
  }

  public Mono<ReportResponse> constructReportResponse(Report report) {

    Mono<List<ReportResponse.ReportItemResponse>> reportItemListResponseMono = Flux.fromIterable(report.getItems())
        .flatMapSequential(this::constructReportItemResponse)
        .collectList();

    return reportItemListResponseMono.map(reportItemListResponse ->
        ReportResponse.builder()
            .id(report.getId())
            .items(reportItemListResponse)
            .revenue(report.getRevenue())
            .subscriptionCount(report.getSubscriptionCount())
            .timestamp(report.getTimestamp())
            .build());
  }

  private Mono<ReportResponse.ReportItemResponse> constructReportItemResponse(Report.ReportItem reportItem) {
    return menuRepository.findFirstById(uuidHelper.fromString(reportItem.getMenuId()))
        .switchIfEmpty(Mono.error(new NotFoundException("MENU")))
        .flatMap(this::constructMenuResponse)
        .map(menuResponse -> ReportResponse.ReportItemResponse.builder()
            .purchaseCount(reportItem.getPurchaseCount())
            .menu(menuResponse).build());
  }


  public Mono<MenuResponse> constructMenuResponse(Menu menu) {
    return constructMealplanResponse(menu.getMealplanId())
        .map(mealplanResponse -> new MenuResponse(menu, mealplanResponse));
  }

  public Mono<MenuAdminResponse> constructMenuAdminResponse(Menu menu) {

    Mono<List<IngredientResponse>> ingredientsResponseFlux = Flux.fromIterable(menu.getRecipes())
        .flatMap(this::constructIngredientResponse)
        .collectList();

    return constructMealplanResponse(menu.getMealplanId())
        .zipWith(ingredientsResponseFlux)
        .map(tuple2 -> new MenuAdminResponse(menu, tuple2.getT1(), tuple2.getT2()));

  }

  public Mono<IngredientResponse> constructIngredientResponse(Ingredient ingredient) {
    return stockRepository.findById(uuidHelper.fromString(ingredient.getStockId()))
        .switchIfEmpty(Mono.error(new NotFoundException("STOCK")))
        .map(StockResponse::new)
        .map(stockResponse -> IngredientResponse.builder()
            .stock(stockResponse)
            .quantity(ingredient.getQuantity())
            .build());
  }

  public Mono<ActiveOrderResponse> constructActiveOrderResponse(Subscription updatedSubscription, long timestamp) {

    List<SubscriptionItem> subscriptionItem = updatedSubscription.getSubscriptionList()
        .stream()
        .filter(subscriptionItem1 -> subscriptionItem1.getTimestamp() == timestamp)
        .collect(
            Collectors.toList());

    Mono<UserResponse> userResponseMono = userRepository.findById(uuidHelper.fromString(updatedSubscription.getUserId()))
        .switchIfEmpty(Mono.error(new NotFoundException("USER")))
        .zipWhen(user -> constructMealplanResponse(user.getMealplanId()))
        .map(UserResponse::new);

    return menuRepository.findFirstById(uuidHelper.fromString(subscriptionItem.get(0).getMenuId()))
        .switchIfEmpty(Mono.error(new NotFoundException("MENU")))
        .flatMap(this::constructMenuAdminResponse)
        .zipWith(userResponseMono)
        .map(tuple2 ->
            ActiveOrderResponse.builder()
                .subscriptionId(updatedSubscription.getId().toString())
                .deliveryLoc(subscriptionItem.get(0).getDeliveryLoc())
                .deliveryStatus(subscriptionItem.get(0).getDeliveryStatus())
                .deliveryTime(subscriptionItem.get(0).getDeliveryTime())
                .timestamp(subscriptionItem.get(0).getTimestamp())
                .user(tuple2.getT2())
                .menu(tuple2.getT1())
                .build());

  }


}
