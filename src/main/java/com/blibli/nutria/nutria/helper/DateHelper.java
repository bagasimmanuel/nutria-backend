package com.blibli.nutria.nutria.helper;

import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.TimeZone;

@Component
public class DateHelper {

  private final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("Asia/Ho_Chi_Minh")));

  public long getNextDayOfTheWeek(DayOfWeek dayOfWeek){
//    Long nextMondayLocalDate = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY)).atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli(); GMT + 0
    return LocalDate.now().with(TemporalAdjusters.next(dayOfWeek)).atStartOfDay().atZone(ZoneId.of("Asia/Ho_Chi_Minh")).toInstant().toEpochMilli() / 1000; // GMT +7

  }

  public long getPreviousOrSameDayOfTheWeek(DayOfWeek dayOfWeek){
    return LocalDate.now().with(TemporalAdjusters.previousOrSame(dayOfWeek)).atStartOfDay().atZone(ZoneId.of("Asia/Ho_Chi_Minh")).toInstant().toEpochMilli() / 1000;
  }

  public boolean isMonday(long timestamp){
    calendar.setTimeInMillis(timestamp * 1000);
//    System.out.println(calendar.getTime());
//    System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
    return calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
  }

  public boolean fridaySaturdayOrSunday(long timestamp){
    calendar.setTimeInMillis(timestamp * 1000);
    return calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
  }

  public long getDayEpoch(long timestamp,int day){
    return timestamp + (day * 86400);
  }

  public long getTodayInEpoch(){
    return LocalDate.now().atStartOfDay().atZone(ZoneId.of("Asia/Ho_Chi_Minh")).toInstant().toEpochMilli() / 1000;
  }

}
