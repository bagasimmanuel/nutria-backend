package com.blibli.nutria.nutria.helper;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UUIDHelper {

  public UUID randomUUID() { return UUID.randomUUID(); }

  public UUID fromString(String s) { return UUID.fromString(s); }

}
