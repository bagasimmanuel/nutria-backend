package com.blibli.nutria.nutria.filter;

import com.blibli.nutria.nutria.exception.BadCredentialsException;
import com.blibli.nutria.nutria.exception.BadRequestException;
import com.blibli.nutria.nutria.exception.BaseException;
import com.blibli.nutria.nutria.exception.DuplicateKeyException;
import com.blibli.nutria.nutria.exception.EmptyRequestException;
import com.blibli.nutria.nutria.exception.ExpiredTokenException;
import com.blibli.nutria.nutria.exception.InvalidDateException;
import com.blibli.nutria.nutria.exception.NotFoundException;
import com.blibli.nutria.nutria.exception.UnauthorizedException;
import com.blibli.nutria.nutria.exception.UnverifiedEmailException;
import com.blibli.nutria.nutria.model.command.response.BaseErrorResponse;
import org.springframework.core.ResolvableType;
import org.springframework.core.codec.Hints;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.security.authentication.DisabledException;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Component
public class ExceptionFilter implements WebFilter {

  private final Jackson2JsonEncoder jackson2JsonEncoder;

  public ExceptionFilter() {
    this.jackson2JsonEncoder = new Jackson2JsonEncoder();
  }

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    return chain.filter(exchange)
        .onErrorResume(EmptyRequestException.class,e -> createErrorResponse(exchange,e,HttpStatus.BAD_REQUEST))
        .onErrorResume(BadRequestException.class,e -> createErrorResponse(exchange,e,HttpStatus.BAD_REQUEST))
        .onErrorResume(DuplicateKeyException.class,e -> createErrorResponse(exchange,e, HttpStatus.BAD_REQUEST))
        .onErrorResume(NotFoundException.class,e -> createErrorResponse(exchange,e,HttpStatus.NOT_FOUND))
        .onErrorResume(UnauthorizedException.class,e -> createErrorResponse(exchange,e,HttpStatus.UNAUTHORIZED))
        .onErrorResume(DisabledException.class,e -> createErrorResponse(exchange,new UnverifiedEmailException(),HttpStatus.BAD_REQUEST))
        .onErrorResume(UnverifiedEmailException.class,e -> createErrorResponse(exchange,e, HttpStatus.BAD_REQUEST))
        .onErrorResume(BadCredentialsException.class,e -> createErrorResponse(exchange,new BadCredentialsException(),HttpStatus.BAD_REQUEST))
        .onErrorResume(InvalidDateException.class,e -> createErrorResponse(exchange,new InvalidDateException(),HttpStatus.BAD_REQUEST))
        .onErrorResume(ExpiredTokenException.class,e -> createErrorResponse(exchange,e,HttpStatus.BAD_REQUEST));
  }

  private <T extends BaseException> Mono<Void> createErrorResponse(ServerWebExchange exchange,T exception,HttpStatus status){
    exchange.getResponse().getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    exchange.getResponse().setStatusCode(status);

    BaseErrorResponse<T> errorResponse = new BaseErrorResponse<>(status,exception);
    return exchange.getResponse().writeWith(jackson2JsonEncoder.encode(
        Mono.just(errorResponse),
        exchange.getResponse().bufferFactory(),
        ResolvableType.forInstance(errorResponse),
        MediaType.APPLICATION_JSON,
        Hints.from(Hints.LOG_PREFIX_HINT, exchange.getLogPrefix())));
  }


}
