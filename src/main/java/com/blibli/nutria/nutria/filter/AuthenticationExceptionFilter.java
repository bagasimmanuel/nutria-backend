package com.blibli.nutria.nutria.filter;

import com.blibli.nutria.nutria.exception.UnauthorizedException;
import com.blibli.nutria.nutria.model.command.response.BaseErrorResponse;
import org.springframework.core.ResolvableType;
import org.springframework.core.codec.Hints;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Component
public class AuthenticationExceptionFilter implements WebFilter {
  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    return chain.filter(exchange)
        .onErrorResume(UnauthorizedException.class,e -> {
          exchange.getResponse().getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
          exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
          BaseErrorResponse<UnauthorizedException> response = new BaseErrorResponse<>(HttpStatus.UNAUTHORIZED, e);
          return exchange.getResponse().writeWith(new Jackson2JsonEncoder().encode(
              Mono.just(response),
              exchange.getResponse().bufferFactory(),
              ResolvableType.forInstance(response),
              MediaType.APPLICATION_JSON,
              Hints.from(Hints.LOG_PREFIX_HINT, exchange.getLogPrefix())));
        });
  }




}
